-- GMR Object Avoidance by Snoogens

local known_sizes = {
    -- Example (ID as Key, Radius as Value):
    -- [177025] = 5,
    [190130] = 4, -- Brazier in Onslaught Camp, Dragonblight
    [190131] = 4, -- Brazier in Onslaught Camp, Dragonblight
    [190132] = 4, -- Brazier in Onslaught Camp, Dragonblight
    [190133] = 4, -- Brazier in Onslaught Camp, Dragonblight
    [190134] = 4, -- Brazier in Onslaught Camp, Dragonblight

    [192135] = 3.5,
    [191820] = 3.5,
}

local exceptions = {
    233282,
    251353,
    251352,
    303148,
    335621,
    336432,
    233282,
    59271,
    59262,
    191083,
    180379,
    180145,
    191605,
    335620,
    227741,
    227740,
    186812,
    181621,

    181645, -- Gryphon Egg
    153239, -- Wildkin Feather

    -- Mineral Veins
    324, 2047, 2040, 2653, 1735, 1610, 1667, 1734, 1731, 2055, 3763, 1732, 2054, 3764, 1733, 73940, 73941, 19903, 150081,
    181108, 191133, 103711, 181249, 150082, 176643, 181068, 105569, 189980, 175404, 189981, 189979, 181569, 181570,
    195036, 123309, 123848, 177388, 123310, 185877, 150079, 176645, 181069, 181557, 180215, 150080, 181109, 181555,
    165658, 103713, 181248, 189978, 185557, 181556,
    -- Herbs
    1621, 3729, 1622, 3730, 1619, 3726, 2042, 2866, 2046, 1628, 2043, 1624, 2041, 1620, 3727, 1618, 3724, 1617, 3725,
    2045, 1623, 2044, 191019, 181278, 142141, 176642, 176589, 142143, 183046, 181166, 176584, 176639, 180168, 181271,
    183045, 181270, 183044, 191303, 181276, 190176, 190173, 190174, 190175, 142144, 189973, 176583, 176638, 180167,
    142145, 176637, 176588, 190172, 190171, 181281, 176586, 176640, 180166, 181279, 185881, 181280, 176587, 176641,
    142140, 180165, 181275, 183043, 142142, 176636, 180164, 190170, 181277, 190169,
}

local mineralveins = { 324, 2047, 2040, 2653, 1735, 1610, 1667, 1734, 1731, 2055, 3763, 1732, 2054, 3764, 1733, 73940,
    73941, 19903, 150081, 181108, 191133, 103711, 181249, 150082, 176643, 181068, 105569, 189980, 175404, 189981, 189979,
    181569, 181570, 195036, 123309, 123848, 177388, 123310, 185877, 150079, 176645, 181069, 181557, 180215, 150080,
    181109, 181555, 165658, 103713, 181248, 189978, 185557, 181556, }
local herbs = { 1621, 3729, 1622, 3730, 1619, 3726, 2042, 2866, 2046, 1628, 2043, 1624, 2041, 1620, 3727, 1618, 3724,
    1617, 3725, 2045, 1623, 2044, 191019, 181278, 142141, 176642, 176589, 142143, 183046, 181166, 176584, 176639, 180168,
    181271, 183045, 181270, 183044, 191303, 181276, 190176, 190173, 190174, 190175, 142144, 189973, 176583, 176638,
    180167, 142145, 176637, 176588, 190172, 190171, 181281, 176586, 176640, 180166, 181279, 185881, 181280, 176587,
    176641, 142140, 180165, 181275, 183043, 142142, 176636, 180164, 190170, 181277, 190169, }


local function FastDistance(x1, y1, z1, x2, y2, z2) return math.sqrt(((x1 - x2) ^ 2) + ((y1 - y2) ^ 2) + ((z1 - z2) ^ 2)) end

local function GetPositionFromPosition(x, y, z, dist, angle) return math.cos(angle) * dist + x,
        math.sin(angle) * dist + y, z
end

local function PointToLineDist(x1, y1, x2, y2, x3, y3)
    -- x3,y3 is the point
    px = x2 - x1
    py = y2 - y1
    norm = px * px + py * py
    u = ((x3 - x1) * px + (y3 - y1) * py) / (norm)
    if u > 1 then
        u = 1
    elseif u < 0 then
        u = 0
    end
    x = x1 + u * px
    y = y1 + u * py
    dx = x - x3
    dy = y - y3
    dist = math.sqrt(dx * dx + dy * dy)
    return dist
end

C_Timer.NewTicker(0.1, function()
    if GMR and GMR.IsExecuting() == true then
        local path = GMR.Variables.MeshPath
        if path and #path > 1 then

            local pp = GMR.GetPlayerPosition()
            local extra_buffer = GMR.GetObstacleDistance()
            --for i = 1, #path do
            local i = GMR.Variables.MeshPathIndex
            local point = path[i]

            for j = 1, #GMR.Tables.MeshAreaBlacklist do
                local mab = GMR.Tables.MeshAreaBlacklist[j]
                if point and point[1] and mab and mab[1] then
                    local dist = PointToLineDist(point[1], point[2], pp.x, pp.y, mab[1], mab[2])
                    -- If the points is within mab[4] just remove it
                    if FastDistance(point[1], point[2], point[3], mab[1], mab[2], mab[3]) < mab[4] then
                        table.remove(GMR.Variables.MeshPath, i)

                    else
                        if dist <= mab[4]
                        then
                            -- Delete the point from the path
                            table.remove(path, i)

                            -- Get the angle of attack, if its positive, we put points on the left side of the profile, if its negative, we put points on the right side of the profile
                            local ab = GMR.GetAnglesBetweenPositions(mab[1], mab[2], 0, pp.x, pp.y, 0)
                            ab = ab * 180 / 3.1415

                            local r = mab[4] + extra_buffer
                            if ab > 0 and not GMR.GetDelay("MeshPathEditing") then

                                for k = tostring(math.floor(ab)), tostring(math.floor(ab + 140)) do
                                    -- Do it every 15 degrees
                                    if k % 15 == 0 then
                                        local angle = k / 57.2958
                                        local ptx, pty = mab[1] + r * math.cos(angle), mab[2] + r * math.sin(angle)

                                        table.insert(path, i, { ptx, pty, point[3] })
                                        i = i + 1

                                    end
                                end
                                GMR.SetDelay("MeshPathEditing", 2)
                            end
                        end
                    end
                end
            end
        end
    end
end)
GMR.Draw = function()
    if GMR.Variables.MeshPath then
        for i = 1, #GMR.Variables.MeshPath do
            local x, y, z = GMR.GetMeshPoints(GMR.Variables.MeshPath[i])
            if x then
                GMR.LibDraw.Circle(x, y, z, 2)
            end
        end
    end
end
local f = CreateFrame("frame")
f:SetScript("OnUpdate", function()
    GMR.Draw()
end)
--[[ local f = CreateFrame("frame")
f:SetScript("OnUpdate", function()

    for i = 1, #GMR.Variables.MeshPath do
        --GMR.Print(i)
        local object = GMR.Variables.MeshPath[i]
        GMR.LibDraw.Circle(object[1], object[2], object[3], 2)
    end
end) ]]
