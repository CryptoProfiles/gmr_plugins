-- Goof of the day:
-- If you want to learn to code in Lua, just start at step 1

local players = {}
local playerName = UnitName("player")

local function BuffPlayers()
    if IsInGroup() then 
        for i = 1, GetNumGroupMembers() do
            local unit = IsInRaid() and "raid" .. i or "party" .. i
            if UnitIsDeadOrGhost(unit) or not UnitIsFriend("player", unit) then
                repeat until true
            else
                table.insert(players, unit)
            end
        end
    else
        table.insert(players, "player")
    end
 -- I initially was planning on just buffing your target, and then got the players table to work. i'll keep this part here... maybe it's useful?
    if UnitExists("target") and UnitIsPlayer("target") and UnitIsFriend("player", "target") and UnitInRange("target") and not GMR.UnitAffectingCombat("playerName") and not IsMounted() and not GMR.IsDrinking("playerName") then
        local hasKings = GMR.HasBuff("target", 20217, true, "target") or GMR.HasBuff("target", 25898, true, "target")
        --print("target", hasKings)
        if not hasKings then
            local success, err = GMR.Cast(20217, "target")
            if not success then
                if err then
                    --print("Error casting buff: " .. err)
                    if string.find(err, "Out of range") then
                        --print("Target is out of range")
                    end
                else
                    --print("Error casting buff")
                end
            end
        end
    else
        for _, unit in ipairs(players) do
            local hasKings = GMR.HasBuff(unit, 20217, true, unit) or GMR.HasBuff(unit, 25898, true, unit)
            --print(unit, hasKings)
            if not hasKings and UnitInRange(unit) and not GMR.UnitAffectingCombat("playerName") and not IsMounted() and not GMR.IsDrinking("playerName") then
                local success, err = GMR.Cast(20217, unit)
                if not success then
                    if err then
                        --print("Error casting buff: " .. err)
                        if string.find(err, "Out of range") then
                            --print("Player is out of range")
                        end
                    else
                        --print("Error casting buff. ") --Maybe you don't have enough mana? Or the target isn't in LoS... or perhaps you don't know the spell? Most likely HTTP 418
                    end
                end
            end
        end
    end

    wipe(players)
end
local timer = C_Timer.NewTicker(1.5, BuffPlayers)
