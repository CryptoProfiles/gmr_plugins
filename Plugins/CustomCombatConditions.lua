function GMR.CustomCombatConditions() 
	local class = GMR.GetClass("player")
	if class == "PALADIN" then 
		local divine_storm = GetSpellInfo(53385)
		if GMR.IsCastable(divine_storm)
		and GMR.GetDistance("player", "target", "<", 6.5) then 
			GMR.Cast(divine_storm)
		end 
	elseif class == "DRUID" then 
		-- Do some druid rotation stuff
	end
end 