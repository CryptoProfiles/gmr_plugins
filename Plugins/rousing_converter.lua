C_Timer.NewTicker(0.1, function()

	-- ITEMS
	local rousingFrost = GetItemInfo(190328)
	local rousingFire = GetItemInfo(190320)
	local rousingEarth = GetItemInfo(190315)
	local rousingAir = GetItemInfo(190326)
	local rousingOrder = GetItemInfo(190322)
	local rousingDecay = GetItemInfo(190330)
	local rousingIre = GetItemInfo(190451)
	if GetItemCount(rousingFrost) >= 10 then
		GMR.Use(rousingFrost)
	end
	if GetItemCount(rousingFire) >= 10 then
		GMR.Use(rousingFire)
	end
	if GetItemCount(rousingEarth) >= 10 then
		GMR.Use(rousingEarth)
	end
	if GetItemCount(rousingAir) >= 10 then
		GMR.Use(rousingAir)
	end
	if GetItemCount(rousingOrder) >= 10 then
		GMR.Use(rousingOrder)
	end
	if GetItemCount(rousingDecay) >= 10 then
		GMR.Use(rousingDecay)
	end
	if GetItemCount(rousingIre) >= 10 then
		GMR.Use(rousingIre)
	end

	if not GMR.GetDelay("CustomQuest") then
		if GossipFrame:IsShown() then
			SelectGossipOption(2); GMR.SetDelay("CustomQuest", 3)
		end
	end
end)