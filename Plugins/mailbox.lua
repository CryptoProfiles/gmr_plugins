local SharedMailboxList = {
    "Goldclover",
}

local EXPORT_FILE_NAME = "_mailboxer_plugin"
local PROCESSED_EXPORT_FILE_NAME = "_mailboxer_plugin_processed"

local function magiclines(s)
    if s:sub(-1) ~= "\n" then
        s = s .. "\n"
    end
    return s:gmatch("(.-)\n")
end

do
    GMR.ExportSettings(EXPORT_FILE_NAME)
    local str = GMR.ReadFile(GMR.Variables.Directory .. "SavedVariables/Exports/" .. EXPORT_FILE_NAME .. ".lua")

    local newFileContent = ""
    local newItemList = {}
    local oldItemsIndex = {}
    for line in magiclines(str) do
        if string.find(line, "MailingList") and line ~= "_G.GMR_SavedVariablesPerCharacter[\"MailingList\"] = {}" then
            local equalSymbolPos = string.find(line, "=")
            local itemName = string.sub(string.sub(line, equalSymbolPos + 3), 1, -2)
            table.insert(newItemList, itemName)
            oldItemsIndex[itemName] = true
        else
            newFileContent = newFileContent .. line .. "\n"
        end
    end

    for _, v in ipairs(SharedMailboxList) do
        if not oldItemsIndex[v] then
            table.insert(newItemList, v)
        end
    end

    local test = ""
    for i, v in ipairs(newItemList) do
        str = "_G.GMR_SavedVariablesPerCharacter[\"MailingList\"][" .. tostring(i) .. "] = \"" .. v .. "\"\n"
        newFileContent = newFileContent .. str
        test = test .. str
    end

    GMR.WriteFile(GMR.Variables.Directory .. "SavedVariables/Exports/" .. PROCESSED_EXPORT_FILE_NAME .. ".lua", newFileContent)
    GMR.ImportSettings(PROCESSED_EXPORT_FILE_NAME)
end
