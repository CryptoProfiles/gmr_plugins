--- version : 0.1
--- Author : trick9000
--- Community : GMR

--- Changelog:
--- .1
--- Initial beta release
C_Timer.NewTicker(6, function()
	if GMR.IsExecuting() and GMR.HasDebuff("player", "Intense Cold") then 
		GMR.Jump()
		GMR.Print("Intense Cold Jump!")
	end
end)