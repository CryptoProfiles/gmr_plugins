DistanceBetweenWaypoints = 50;
WaypointsRadius = 150;
LogWaypoints=0;
LastWaypoint=nil;
	
local function getDistance(wayA, wayB)
	return math.sqrt(math.pow(wayB.x - wayA.x, 2) + math.pow(wayB.y - wayA.y, 2) + math.pow(wayB.z - wayA.z, 2))
end

function RakkanWayPoints(distance, radius)

	if(distance~=nil and isNumber(distance)) then
		DistanceBetweenWaypoints = distance;
	end

	if(radius~=nil and isNumber(radius)) then
	WaypointsRadius = radius;
	end

	if(LogWaypoints==0) then
	LogWaypoints=1;
	GMR.Print("Rakkan Profile Creator: Adding Automatic Waypoints every "..DistanceBetweenWaypoints.." yards.");
	elseif(LogWaypoints==1) then
	LogWaypoints=2;
	GMR.Print("Rakkan Profile Raw Editor: Adding Automatic Waypoints every "..DistanceBetweenWaypoints.." yards. Radius: "..WaypointsRadius);
	else
	LogWaypoints=0;
	GMR.Print("Rakkan Automatic Waypoints Disabled.");
	end
end

C_Timer.NewTicker(0.5, function()
	if(LogWaypoints>0) then
	local pos = GMR.GetPlayerPosition();
	local saveWaypoint = false;
	if(LastWaypoint~=nil) then
		local dis = getDistance(pos,LastWaypoint);
		saveWaypoint = (dis >= DistanceBetweenWaypoints);
	else
		saveWaypoint = true;
	end

	if(saveWaypoint) then
		LastWaypoint = pos;
		if(LogWaypoints==1) then
		DefineManualCenter_Button:Click()
		elseif(LogWaypoints==2) then
		local way = "\nGMR.DefineProfileCenter("..tostring(pos.x)..","..tostring(pos.y)..","..tostring(pos.z)..","..tostring(WaypointsRadius)..")";
		ProfileProfileRawEditor_EditBox:SetText(ProfileProfileRawEditor_EditBox:GetText()..way);
		end
		GMR.Print("Waypoint Added: "..tostring(pos.x)..","..tostring(pos.y)..","..tostring(pos.z));
	end
	else
	LastWaypoint=nil;
	end	
end)