-- Author: Mons
-- Date: 13-11-2022
-- Instaloot.lua
--
-- Description: Sets the GMR.Timer.Looting to GetTime() to let GMR instaloot. Enjoy! :monscool:

local looting_delay = 0.1
local skinning_delay = 0.1
if not GMR.Frames.MonsInstaLooter then
    GMR.Print("Frame created - Instaloot")
    GMR.Frames.MonsInstaLooter = CreateFrame("frame")

    GMR.Frames.MonsInstaLooter:SetScript("OnUpdate", function(self)
        -- looting
        if GMR.Timer.Looting and (GetTime() - GMR.Timer.Looting) > looting_delay then
            GMR.Timer.Looting = GetTime()
        end
        -- skinning
        if GMR.Timer.Skinning and (GetTime() - GMR.Timer.Skinning) > skinning_delay then
            GMR.Timer.Skinning = GetTime()
        end

    end)
end
