--- version : 0.1
--- Author : trick9000
--- Community : GMR

--- Changelog:
--- .1
--- Initial beta release
C_Timer.NewTicker(1, function()
	if GMR.IsRecovering() then 
		if GMR.GetHealth("player") < 95 and GMR.IsDrinking("player") and not GMR.IsEating("Player") then
			GMR.Eat()
			GMR.Print("Eating to top up")
		elseif GMR.GetMana("player") < 95 and GMR.IsEating("player") and not GMR.IsDrinking("Player") then
			GMR.Drink()
			GMR.Print("Drinking to top up")
		end
	end
end)