-- Edit below to match the folder name & profile name of the profiles you want to use.
-- You can change the number of profiles to be used by adding/removing lines in profiles = {}

local profiles = {
    [1] = {"09. Mining [Alliance]", "1-65 Darkshore"},
    [2] = {"07. Herbalism [Alliance]", "1-50 Elwynn Forest"},
    [3] = {"07. Herbalism [Alliance]", "1-75 Dun Morogh"},
    [4] = {"07. Herbalism [Alliance]", "50-75 Westfall"}
}


local randomProfile = math.random(1, #profiles)
local macroexecuted = 0
local profile_folder = profiles[randomProfile][1]
local profile_name = profiles[randomProfile][2]

if macroexecuted == 0 then
  myTimer = C_Timer.NewTicker(0.5, function()

          if GMR.IsFullyLoaded() and macroexecuted == 0 then
            GMR.LoadProfile(profile_folder, profile_name)
            macroexecuted=1	    
          end
  end)

else 
  myTimer:Cancel()
end