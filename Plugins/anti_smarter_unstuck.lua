local UNSTUCKS = {
	--[[
    {
        {x, y, z, 5},
        {x1, y1, z1}
    },
    {
        {x, y, z, 5},
        {x1, y1, z1}
    }
	]]--
}

local DOING_UNSTUCK = false
local UNSTUCK_INDEX = nil
local UNSTUCK_STEP = 1

local GMR_DEBUG_BLOCK_UNSTUCK = {
    --"ANTI_DOING_THE_THING"
}

local GMR_DEBUG = nil
hooksecurefunc(GMR, 'Log', function(value)
    GMR_DEBUG = value
end)

local function DontDoUnstucks()
    for i = 1,#GMR_DEBUG_BLOCK_UNSTUCK do
        if GMR_DEBUG == GMR_DEBUG_BLOCK_UNSTUCK[i] then
            return true
        end
    end
    return false
end

local function DoUnstucks()
    if not DOING_UNSTUCK then
        for i = 1,#UNSTUCKS do
            if GMR.IsPlayerPosition(UNSTUCKS[i][1][1], UNSTUCKS[i][1][2], UNSTUCKS[i][1][3], UNSTUCKS[i][1][4]) and GMR.IsFacingXYZ(UNSTUCKS[i][#UNSTUCKS[i]][1], UNSTUCKS[i][#UNSTUCKS[i]][2], UNSTUCKS[i][#UNSTUCKS[i]][3]) then
                DOING_UNSTUCK = true
                UNSTUCK_INDEX = i
                GMR.SetDelay("Execute", 0.5)
                GMR.StopMoving()
                break
            end
        end
    else
        GMR.SetDelay("Execute", 0.5)
        if UNSTUCK_STEP <= #UNSTUCKS[UNSTUCK_INDEX] then
            if not GMR.IsPlayerPosition(UNSTUCKS[UNSTUCK_INDEX][UNSTUCK_STEP][1], UNSTUCKS[UNSTUCK_INDEX][UNSTUCK_STEP][2], UNSTUCKS[UNSTUCK_INDEX][UNSTUCK_STEP][3], 1.5) then
                GMR.MoveTo(UNSTUCKS[UNSTUCK_INDEX][UNSTUCK_STEP][1], UNSTUCKS[UNSTUCK_INDEX][UNSTUCK_STEP][2], UNSTUCKS[UNSTUCK_INDEX][UNSTUCK_STEP][3])
            else
                UNSTUCK_STEP = UNSTUCK_STEP + 1
            end
        else
            DOING_UNSTUCK = false
            UNSTUCK_STEP = 1
        end
    end
end

if not GMR.Frames.AntiUnstuck then
    GMR.Frames.AntiUnstuck = CreateFrame("frame")
    GMR.Frames.AntiUnstuck:SetScript("OnUpdate", function(self)
        if not DontDoUnstucks() then
            DoUnstucks()
        end
    end)
end