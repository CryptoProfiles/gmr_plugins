C_Timer.NewTicker(0.5, function()
		
	if (not GMR.IsExecuting() or not GMR.IsAlive()) then 
		return 
	end
	
	
	-- ONLY EDIT HERE --
	local gates = {
		{	-- Gate 1
			gatePosition = { 1, 2, 3 },			-- gate x, y, z 									- > /dump GMR.GetObject("object_name")	-	replace "object_name" with the real object name. for example: GMR.GetObject("Iron Gate")
			gateId = 4, 						-- gate id 											- > /dump GMR.GetObject("object_name")	-	replace "object_name" with the real object name. for example: GMR.GetObject("Iron Gate")
			interactPosition = { 1, 2, 3 },		-- x, y, z of where you want to interact with it 	- > /dump GMR.ObjectPosition("player")
			tracepoint = { 1, 2, 3 }				-- x, y, z of a position behind the closed gate 	- > /dump GMR.ObjectPosition("player") 
		},
		{	-- Gate 2
			gatePosition = { 1, 2, 3 },
			gateId = 4,
			interactPosition = { 1, 2, 3 },
			tracepoint = { 1, 2, 3 }
		},
		-- You can add more gates by copying the above syntax
	}
	-----------------------------------------------------------------------------------------------------
	
	
	for i = 1,#gates do 
		local gateX, gateY, gateZ = GMR.GetMeshPoints(gates[i].gatePosition)
		local gateId = gates[i].gateId
		local interactX, interactY, interactZ = GMR.GetMeshPoints(gates[i].interactPosition)
		local x2, y2, z2 = GMR.GetMeshPoints(gates[i].tracepoint)
		if GMR.IsPlayerPosition(interactX, interactY, interactZ, 15) then
			local hit = GMR.TraceLine(interactX, interactY, interactZ+2, x2, y2, z2+2, 0x100111)
			if hit then
				GMR.SetDelay("Execute", 1)
				if not GMR.IsPlayerPosition(interactX, interactY, interactZ, 1.5) then 
					GMR.Mesh(interactX, interactY, interactZ)
				elseif GMR.IsMoving() then 
					GMR.StopMoving() 
				else
					local gate = GMR.GetObjectWithXYZ(gateX, gateY, gateZ, gateId)
					if gate then 
						GMR.InteractObject(gate)
					end 
				end
			end 
		end
	end
end)

