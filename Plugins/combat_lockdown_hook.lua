--- @class CombatLockdownHook
--- @field oFunc fun():boolean the original function
CombatLockdownHook = CombatLockdownHook or {
    oFunc = nil
}

--- Overrides InCombatLockdown() to always return false
function CombatLockdownHook:OverrideCombatLockdown()
    if InCombatLockdown then
        self.oFunc = InCombatLockdown
        InCombatLockdown = function()
            return false
        end
    else
        RunNextFrame(self:OverrideCombatLockdown())
    end
end

--- Restores the original InCombatLockdown function
function CombatLockdownHook:Restore()
    if self.oFunc then
        InCombatLockdown = self.oFunc
    end
end

CombatLockdownHook:OverrideCombatLockdown()