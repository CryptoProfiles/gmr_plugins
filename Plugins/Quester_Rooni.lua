local function DefineVendors(location)
    if (location == "Sunstrider Isle") then 
        GMR.DefineSellVendor(10407.733398438, -6351.8081054688, 36.927261352539, 15289)
        GMR.DefineRepairVendor(10407.733398438, -6351.8081054688, 36.927261352539, 15289)
        GMR.DefineGoodsVendor(10374.122070312, -6391.7490234375, 38.531349182129, 15287)
		GMR.DefineAmmoVendor(10374.122070312, -6391.7490234375, 38.531349182129, 15287)	
    elseif (location == "Falconwing Square" or location == "The Dead Scar") then
        GMR.DefineSellVendor (9482.34375, -6805.5122070312, 16.493392944336, 18926)
		GMR.DefineRepairVendor(9482.34375, -6805.5122070312, 16.493392944336, 18926)
		GMR.DefineAmmoVendor(9491.6416015625, -6856.794921875, 17.429178237915, 16259)
    elseif (location == "Fairbreeze Village" or location == "Eversong Woods") then
        GMR.DefineSellVendor(8731.693359375, -6656.4975585938, 70.585578918457, 16444)
		GMR.DefineRepairVendor(8983.7421875, -7443.4829101562, 87.078285217285, 16263)
		GMR.DefineGoodsVendor(8700.3837890625, -6638.3559570312, 72.745613098145, 15397)
		GMR.DefineAmmoVendor(8983.7421875, -7443.4829101562, 87.077713012695, 16263)
	end
end

GMR.DefineQuester("Rooni", function()

	GMR.DefineQuest({
		QuestName = "Reclaiming Sunstrider Isle",
		QuestID = 8325,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 10357.569335938, y = -6369.5571289062, z = 36.107814788818, id = 15278 },
		TurnIn = { x = 10357.569335938, y = -6369.5571289062, z = 36.107814788818, id = 15278 },
		Profile = function()	
			GMR.DefineProfileCenter(10307.4140625, -6326.1435546875, 25.710319519043, 150)
			GMR.DefineProfileCenter(10280.224609375, -6355.3315429688, 24.179471969604, 150)
			GMR.DefineProfileCenter(10248.622070312, -6335.2802734375, 30.840591430664, 150)
			GMR.DefineQuestEnemyId(15274)
			DefineVendors("Sunstrider Isle")
		end
	})
	GMR.DefineQuest({
		QuestName = "Unfortunate Measures",
		QuestID = 8326,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 10357.569335938, y = -6369.5571289062, z = 36.107814788818, id = 15278 },
		TurnIn = { x = 10357.569335938, y = -6369.5571289062, z = 36.107814788818, id = 15278 },
		Profile = function()	
			GMR.DefineQuestEnemyId(15366)
			GMR.DefineProfileCenter(10418.323242188, -6415.3349609375, 42.85986328125, 150)
			GMR.DefineProfileCenter(10469.055664062, -6462.6674804688, 22.717561721802, 150)
			GMR.DefineProfileCenter(10424.014648438, -6469.5395507812, 33.300388336182, 150)
			GMR.DefineProfileCenter(10369.087890625, -6468.0629882812, 36.797660827637, 150)
			DefineVendors("Sunstrider Isle")
		end
	})
	GMR.DefineQuest({
	    QuestName = "Windows to the Source",
		QuestID = 8344,
		QuestType = "Grinding",
		Race = "BloodElf",
		Class = "WARLOCK",
		PickUp = { x = 10337.5703125, y = -6405.0258789062, z = 38.531345367432, id = 15283 }, -- do not forget commas
		TurnIn = { x = 10337.5703125, y = -6405.0258789062, z = 38.531345367432, id = 15283 },
		Profile = function()
		GMR.DefineQuestEnemyId(15273)
		GMR.DefineQuestEnemyId(15298)
		GMR.DefineProfileCenter(10210.368164062, -6115.1997070312, 17.187274932861, 100)
		GMR.DefineProfileCenter(10208.391601562, -6107.591796875, 16.286113739014, 100)
		GMR.DefineProfileCenter(10205.990234375, -6095.697265625, 15.029792785645, 100)
		GMR.DefineProfileCenter(10201.822265625, -6089.5146484375, 17.273376464844, 100)
		GMR.DefineProfileCenter(10198.31640625, -6084.2573242188, 20.629573822021, 100)
		GMR.DefineProfileCenter(10196.671875, -6082.078125, 21.707054138184, 100)
		GMR.DefineProfileCenter(10193.330078125, -6076.5395507812, 23.788082122803, 150)
		GMR.DefineProfileCenter(10190.984375, -6072.744140625, 25.224704742432, 100)
		GMR.DefineProfileCenter(10187.791015625, -6068.3725585938, 25.720516204834, 150)
		GMR.DefineProfileCenter(10182.833007812, -6057.6171875, 25.53985786438, 150)
		GMR.DefineProfileCenter(10174.982421875, -6047.5078125, 25.538677215576, 150)
		GMR.DefineProfileCenter(10222.81640625, -5987.4013671875, 38.945457458496, 150)
		GMR.DefineProfileCenter(10241.3828125, -5957.951171875, 41.946960449219, 100)
		GMR.DefineProfileCenter(10244.426757812, -5942.3115234375, 45.149909973145, 100)
		GMR.DefineProfileCenter(10240.16015625, -5928.8696289062, 47.565746307373, 100)
		GMR.DefineProfileCenter(10231.19140625, -5918.5356445312, 49.934421539307, 100)
		GMR.DefineProfileCenter(10215.919921875, -5911.9809570312, 52.729019165039, 100)
		GMR.DefineProfileCenter(10197.484375, -5915.12890625, 55.994689941406, 100)
		GMR.DefineProfileCenter(10183.717773438, -5922.8950195312, 58.756240844726, 100)
		GMR.DefineProfileCenter(10175.77734375, -5935.0415039062, 61.561813354492, 100)
		GMR.DefineProfileCenter(10165.990234375, -5969.8168945312, 63.569454193115, 100)
		GMR.DefineProfileCenter(10168.045898438, -6020.5874023438, 63.57071685791, 150)
		GMR.DefineProfileCenter(10183.374023438, -5997.7333984375, 68.756980895996, 100)
		GMR.DefineProfileCenter(10184.206054688, -5989.423828125, 72.787101745605, 100)
		GMR.DefineProfileCenter(10183.446289062, -5982.6337890625, 75.934120178223, 50)
		GMR.DefineProfileCenter(10180.655273438, -5976.1577148438, 79.055274963379, 50)
		GMR.DefineProfileCenter(10176.540039062, -5970.1733398438, 82.483909606934, 100)
		GMR.DefineProfileCenter(10170.048828125, -5965.5600585938, 85.783355712891, 100)
		GMR.DefineProfileCenter(10162.486328125, -5963.2607421875, 89.318618774414, 100)
		GMR.DefineProfileCenter(10154.922851562, -5962.4189453125, 92.747367858887, 100)
		GMR.DefineProfileCenter(10147.663085938, -5964.3784179688, 96.032028198242, 50)
		GMR.DefineProfileCenter(10140.590820312, -5967.5659179688, 99.626113891602, 50)
		GMR.DefineProfileCenter(10134.84765625, -5972.513671875, 102.94095611572, 50)
		GMR.DefineProfileCenter(10129.748046875, -5978.5078125, 105.77222442627, 50)
		GMR.DefineProfileCenter(10125.549804688, -5984.455078125, 108.16416168213, 50)
		GMR.DefineProfileCenter(10122.87109375, -5991.591796875, 108.74279785156, 50)
		GMR.DefineProfileCenter(10133.866210938, -5998.6547851562, 110.15525817871, 50)
		GMR.DefineProfileCenter(10151.376953125, -6017.017578125, 110.15524291992, 150)
		GMR.DefineProfileCenter(10163.96484375, -5999.9848632812, 110.15524291992, 150)
		GMR.CreateTableEntry("Unstuck")
		GMR.DefineUnstuck(10193.944335938, -6089.4580078125, 9.2986736297607, 8)
		GMR.DefineUnstuck(10195.260742188, -6111.5141601562, 16.335889816284)
		GMR.DefineUnstuck(10208.150390625, -6107.6870117188, 16.279636383057)
		GMR.CreateTableEntry("Unstuck")
		GMR.DefineUnstuck(10194.618164062, -6072.7993164062, 24.8440990448, 5)
		GMR.DefineUnstuck(10179.482421875, -6054.013671875, 25.538703918457)
		DefineVendors("Sunstrider Isle")
		end
	})
	GMR.DefineQuest({
		QuestName = "Solanian's Belongings",
		QuestID = 8330,
		QuestType = "Custom",
		Race = "BloodElf",
		PickUp = { x = 10406.016601562, y = -6396.2377929688, z = 52.095478057861, id = 15295 },
		TurnIn = { x = 10406.016601562, y = -6396.2377929688, z = 52.095478057861, id = 15295 },
		Profile = function()
			DefineVendors("Sunstrider Isle")
		end,
		Lua = function()
			if not GMR.Questing.IsObjectiveCompleted(8330, 1) then -- if the first task of the quest is not completed then do
				if GMR.IsPlayerPosition(10403.977539062, -6397.1767578125, 52.09188079834, 5) then 
					GMR.SetDelay("Executing", 2)
					GMR.MoveTo(10396.392578125, -6379.4794921875, 37.698509216309)
					return
				end
				local x, y, z, id = 10093.028320312, -6218.3227539062, 16.667625427246, 180510
				if not GMR.IsPlayerPosition(x, y, z, 4) then 
					GMR.MeshTo(x, y, z)
				elseif GMR.IsMoving() then 
					GMR.StopMoving()
				else 
					local object = GMR.GetObjectWithInfo({ id = id, rawType = 8, isInteractable = true })
					if object then 
						GMR.Interact(object)
					end  -- and the data therefor we receive by this api
				end 
			elseif not GMR.Questing.IsObjectiveCompleted(8330, 2) then -- else if first is complete but second one is not then do 
			local x, y, z, id = 10294.891601562, -6030.453125, 28.442716598511, 180511
				if not GMR.IsPlayerPosition(x, y, z, 4) then 
					GMR.MeshTo(x, y, z)
				elseif GMR.IsMoving() then 
					GMR.StopMoving()
				else 
					local object = GMR.GetObjectWithInfo({ id = id, rawType = 8, isInteractable = true })
					if object then 
						GMR.Interact(object)
					end 
				end 
			elseif not GMR.Questing.IsObjectiveCompleted(8330, 3) then 
				local x, y, z, id = 10223.86328125, -6344.1728515625, 30.840763092041, 180512
				if not GMR.IsPlayerPosition(x, y, z, 4) then 
					GMR.MeshTo(x, y, z)
				elseif GMR.IsMoving() then 
					GMR.StopMoving()
				else 
					local object = GMR.GetObjectWithInfo({ id = id, rawType = 8, isInteractable = true })
					if object then 
						GMR.Interact(object)
					end 
				end 
			end
		end
	}) 
	GMR.DefineQuest({
		QuestName = "A Fistful of Slivers",
		QuestID = 8336,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 10413.703125, y = -6372.462890625, z = 35.644340515137, id = 15296 }, -- do not forget commas
		TurnIn = { x = 10413.703125, y = -6372.462890625, z = 35.644340515137, id = 15296 },
		Profile = function()
			GMR.DefineQuestEnemyId(15274)
			GMR.DefineProfileCenter(10387.583007812, -6308.939453125, 32.78893661499, 150)
			GMR.DefineProfileCenter(10376.05859375, -6250.6044921875, 27.071140289307, 150)
			GMR.DefineProfileCenter(10314.84375, -6257.48046875, 28.601665496826, 150)
			GMR.DefineProfileCenter(10297.508789062, -6310.2817382812, 25.695320129395, 150)
			DefineVendors("Sunstrider Isle")
		end
	})
	GMR.DefineQuest({
		QuestName = "Thirst Unending",
		QuestID = 8346,
		QuestType = "Custom",
		Race = "BloodElf",
		PickUp = { x = 10419.655273438, y = -6318.6176757812, z = 35.573333740234, id = 15297 },
		TurnIn = { x = 10419.655273438, y = -6318.6176757812, z = 35.573333740234, id = 15297 },
		Profile = function()
			GMR.DefineProfileCenter(10265.258789062, -6340.1264648438, 26.694179534912, 150)
			DefineVendors("Sunstrider Isle")
		end,
		Lua = function()
			local mana_tap = GetSpellInfo(28734)
			local object = GMR.GetObjectWithInfo({ id = 15274, isAlive = true, canAttack = true, distance })
			if object then 
				GMR.SetQuestingState(nil) 
				if GMR.IsCastable(mana_tap, object) then 
					GMR.Cast(mana_tap, object)
				else
					GMR.Questing.KillEnemy(object)
				end 
			else 
				GMR.SetQuestingState("Idle")
			end
				
		end
	})
	GMR.DefineQuest({
		QuestName = "Report to Lanthan Perilon",
		QuestID = 8327,
		QuestType = "Custom",
		Race = "BloodElf",
		PickUp = { x = 10357.569335938, y = -6369.5571289062, z = 36.108196258545, id = 15278 }, 
		TurnIn = { x = 10302.25, y = -6229.302734375, z = 26.622295379639, id = 15281 },
		Profile = function()	
			DefineVendors("Sunstrider Isle")
		end
	})
	GMR.DefineQuest({
	    QuestName = "Aggression",
		QuestID = 8334,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 10302.25, y = -6229.302734375, z = 26.622930526733, id = 15281 }, -- do not forget commas
		TurnIn = { x = 10302.25, y = -6229.302734375, z = 26.622930526733, id = 15281 },
		Profile = function()
		GMR.DefineQuestEnemyId(15271)
		GMR.DefineQuestEnemyId(15294)
		GMR.DefineProfileCenter(10254.947265625, -6152.3974609375, 19.19803237915, 150)
		GMR.DefineProfileCenter(10238.124023438, -6151.9018554688, 19.824527740479, 150)
		GMR.DefineProfileCenter(10213.041015625, -6153.126953125, 18.205686569214, 150)
		GMR.DefineProfileCenter(10181.431640625, -6166.31640625, 21.770263671875, 150)
		DefineVendors("Sunstrider Isle")
		end
	})
	GMR.DefineQuest({
	    QuestName = "Felendren the Banished",
		QuestID = 8335,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 10302.25, y = -6229.302734375, z = 26.622930526733, id = 15281 }, -- do not forget commas
		TurnIn = { x = 10302.25, y = -6229.302734375, z = 26.622930526733, id = 15281 },
		Profile = function()
		GMR.DefineQuestEnemyId(15273)
		GMR.DefineQuestEnemyId(15298)
		GMR.DefineQuestEnemyId(15367)
		GMR.DefineProfileCenter(10210.368164062, -6115.1997070312, 17.187274932861, 100)
		GMR.DefineProfileCenter(10208.391601562, -6107.591796875, 16.286113739014, 100)
		GMR.DefineProfileCenter(10205.990234375, -6095.697265625, 15.029792785645, 100)
		GMR.DefineProfileCenter(10201.822265625, -6089.5146484375, 17.273376464844, 100)
		GMR.DefineProfileCenter(10198.31640625, -6084.2573242188, 20.629573822021, 100)
		GMR.DefineProfileCenter(10196.671875, -6082.078125, 21.707054138184, 100)
		GMR.DefineProfileCenter(10193.330078125, -6076.5395507812, 23.788082122803, 150)
		GMR.DefineProfileCenter(10190.984375, -6072.744140625, 25.224704742432, 100)
		GMR.DefineProfileCenter(10187.791015625, -6068.3725585938, 25.720516204834, 150)
		GMR.DefineProfileCenter(10182.833007812, -6057.6171875, 25.53985786438, 150)
		GMR.DefineProfileCenter(10174.982421875, -6047.5078125, 25.538677215576, 150)
		GMR.DefineProfileCenter(10222.81640625, -5987.4013671875, 38.945457458496, 150)
		GMR.DefineProfileCenter(10241.3828125, -5957.951171875, 41.946960449219, 100)
		GMR.DefineProfileCenter(10244.426757812, -5942.3115234375, 45.149909973145, 100)
		GMR.DefineProfileCenter(10240.16015625, -5928.8696289062, 47.565746307373, 100)
		GMR.DefineProfileCenter(10231.19140625, -5918.5356445312, 49.934421539307, 100)
		GMR.DefineProfileCenter(10215.919921875, -5911.9809570312, 52.729019165039, 100)
		GMR.DefineProfileCenter(10197.484375, -5915.12890625, 55.994689941406, 100)
		GMR.DefineProfileCenter(10183.717773438, -5922.8950195312, 58.756240844726, 100)
		GMR.DefineProfileCenter(10175.77734375, -5935.0415039062, 61.561813354492, 100)
		GMR.DefineProfileCenter(10165.990234375, -5969.8168945312, 63.569454193115, 100)
		GMR.DefineProfileCenter(10168.045898438, -6020.5874023438, 63.57071685791, 150)
		GMR.DefineProfileCenter(10183.374023438, -5997.7333984375, 68.756980895996, 100)
		GMR.DefineProfileCenter(10184.206054688, -5989.423828125, 72.787101745605, 100)
		GMR.DefineProfileCenter(10183.446289062, -5982.6337890625, 75.934120178223, 50)
		GMR.DefineProfileCenter(10180.655273438, -5976.1577148438, 79.055274963379, 50)
		GMR.DefineProfileCenter(10176.540039062, -5970.1733398438, 82.483909606934, 100)
		GMR.DefineProfileCenter(10170.048828125, -5965.5600585938, 85.783355712891, 100)
		GMR.DefineProfileCenter(10162.486328125, -5963.2607421875, 89.318618774414, 100)
		GMR.DefineProfileCenter(10154.922851562, -5962.4189453125, 92.747367858887, 100)
		GMR.DefineProfileCenter(10147.663085938, -5964.3784179688, 96.032028198242, 50)
		GMR.DefineProfileCenter(10140.590820312, -5967.5659179688, 99.626113891602, 50)
		GMR.DefineProfileCenter(10134.84765625, -5972.513671875, 102.94095611572, 50)
		GMR.DefineProfileCenter(10129.748046875, -5978.5078125, 105.77222442627, 50)
		GMR.DefineProfileCenter(10125.549804688, -5984.455078125, 108.16416168213, 50)
		GMR.DefineProfileCenter(10122.87109375, -5991.591796875, 108.74279785156, 50)
		GMR.DefineProfileCenter(10133.866210938, -5998.6547851562, 110.15525817871, 50)
		GMR.DefineProfileCenter(10151.376953125, -6017.017578125, 110.15524291992, 150)
		GMR.DefineProfileCenter(10163.96484375, -5999.9848632812, 110.15524291992, 150)
		GMR.CreateTableEntry("Unstuck")
		GMR.DefineUnstuck(10193.944335938, -6089.4580078125, 9.2986736297607, 8)
		GMR.DefineUnstuck(10195.260742188, -6111.5141601562, 16.335889816284)
		GMR.DefineUnstuck(10208.150390625, -6107.6870117188, 16.279636383057)
		GMR.CreateTableEntry("Unstuck")
		GMR.DefineUnstuck(10194.618164062, -6072.7993164062, 24.8440990448, 5)
		GMR.DefineUnstuck(10179.482421875, -6054.013671875, 25.538703918457)
		DefineVendors("Sunstrider Isle")
		end
	})
	GMR.DefineQuest({
	    QuestName = "Aiding the Outrunners",
		QuestID = 8347,
		QuestType = "Custom",
		Race = "BloodElf",
		PickUp = { x = 10302.25, y = -6229.302734375, z = 26.622930526733, id = 15281 }, -- do not forget commas
		TurnIn = { x = 9984.0439453125, y = -6478.0190429688, z = 0.99795734882355, id = 15301 },
		Profile = function()
		GMR.DefineProfileCenter(10254.947265625, -6152.3974609375, 19.19803237915, 150)
		GMR.DefineProfileCenter(10254.290039062, -6151.6245117188, 19.198274612427, 150)
		GMR.DefineProfileCenter(10213.041015625, -6153.126953125, 18.205686569214, 150)
		GMR.DefineProfileCenter(10181.431640625, -6166.31640625, 21.770263671875, 150)
		DefineVendors("Sunstrider Isle")
		end
	})
	GMR.DefineQuest({
	    QuestName = "Slain by the Wretched",
		QuestID = 9704,
		QuestType = "Custom",
		Race = "BloodElf",
		PickUp = { x = 9984.0439453125, y = -6478.0190429688, z = 0.99795734882355, id = 15301 }, -- do not forget commas
		TurnIn = { x = 9870.9970703125, y = -6557.1909179688, z = 10.300135612488, id = 17849 },
		Profile = function()
		
		DefineVendors("Falconwing Square")
		end
	})	
	GMR.DefineQuest({
	    QuestName = "Package Recovery",
		QuestID = 9705,
		QuestType = "Custom",
		Race = "BloodElf",
		PickUp = { x = 9870.9970703125, y = -6557.1909179688, z = 10.300135612488, id = 17849 }, -- do not forget commas
		TurnIn = { x = 9984.0439453125, y = -6478.0190429688, z = 0.99765008687973, id = 15301 },
		Profile = function()
		
		DefineVendors("Falconwing Square")
		end
	})
	GMR.DefineQuest({
	    QuestName = "Completing the Delivery",
		QuestID = 8350,
		QuestType = "Custom",
		Race = "BloodElf",
		PickUp = { x = 9984.0439453125, y = -6478.0190429688, z = 0.99765008687973, id = 15301 }, -- do not forget commas
		TurnIn = { x = 9476.9130859375, y = -6859.2104492188, z = 17.341638565063, id = 15433 },
		Profile = function()
		
		DefineVendors("Falconwing Square")
		end
	})	
	GMR.DefineQuest({
	    QuestName = "Major Malfunction",
		QuestID = 8472,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 9521.166015625, y = -6814.8510742188, z = 16.494009017944, id = 15418 }, -- do not forget commas
		TurnIn = { x = 9521.166015625, y = -6814.8510742188, z = 16.494009017944, id = 15418 },
		Profile = function()
		GMR.DefineQuestEnemyId(15638)
		GMR.DefineProfileCenter(9652.70703125, -6804.1977539062, 11.599552154541, 100)
		GMR.DefineProfileCenter(9704.876953125, -6813.1645507812, 15.575776100159, 100)
		GMR.DefineProfileCenter(9755.267578125, -6810.271484375, 15.062891960144, 150)
		GMR.DefineProfileCenter(9782.744140625, -6764.802734375, 6.0113806724548, 150)
		GMR.DefineProfileCenter(9824.513671875, -6801.9604492188, 15.317755699158, 150)
		GMR.DefineProfileCenter(9877.9072265625, -6768.0424804688, 12.3663854599, 150)
		
		DefineVendors("Falconwing Square")
		end
	})	
	GMR.DefineQuest({
	    QuestName = "Delivery to the North Sanctum",
		QuestID = 8895,
		QuestType = "Custom",
		Race = "BloodElf",
		PickUp = { x = 9521.166015625, y = -6814.8510742188, z = 16.495344161987, id = 15418 }, -- do not forget commas
		TurnIn = { x = 9297.12890625, y = -6685.4340820312, z = 22.447435379028, id = 15405 },
		Profile = function()
		DefineVendors("Falconwing Square")
		end
	})
	GMR.DefineQuest({
	    QuestName = "Malfunction at the West Sanctum",
		QuestID = 9119,
		QuestType = "Custom",
		Race = "BloodElf",
		PickUp = { x = 9297.12890625, y = -6685.4340820312, z = 22.447608947754, id = 15405 }, -- do not forget commas
		TurnIn = { x = 9155.77734375, y = -6294.83984375, z = 21.047233581543, id = 15401 },
		Profile = function()
		DefineVendors("Falconwing Square")
		end
	})
	GMR.DefineQuest({
	    QuestName = "Arcane Instability",
		QuestID = 8486,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 9155.77734375, y = -6294.83984375, z = 21.047233581543, id = 15401 }, -- do not forget commas
		TurnIn = { x = 9155.77734375, y = -6294.83984375, z = 21.047233581543, id = 15401 },
		Profile = function()
		GMR.DefineQuestEnemyId(15647)
		GMR.DefineQuestEnemyId(15648)
		GMR.DefineProfileCenter(9172.6923828125, -6226.35546875, 17.935890197754, 100)
		GMR.DefineProfileCenter(9153.443359375, -6215.2065429688, 17.658760070801, 100)
		GMR.DefineProfileCenter(9111.3818359375, -6192.5444335938, 20.464563369751, 100)
		GMR.DefineProfileCenter(9077.6015625, -6192.9990234375, 19.113985061646, 100)
		GMR.DefineProfileCenter(9064.9248046875, -6239.8984375, 17.078615188599, 100)
		GMR.DefineProfileCenter(9106.8271484375, -6262.96875, 17.905704498291, 100)
		DefineVendors("Falconwing Square")
		end	
	})
	GMR.DefineQuest({
	    QuestName = "Roadside Ambush",
		QuestID = 9035,
		QuestType = "Custom",
		Race = "BloodElf",
		PickUp = { x = 9188.7626953125, y = -6712.9086914062, z = 25.9137840271, id = 15941 }, -- do not forget commas
		TurnIn = { x = 9037.787109375, y = -6697.6186523438, z = 12.562435150146, id = 15945 },
		Profile = function()

		DefineVendors("Eversong Woods")
		end	
	})
	GMR.DefineQuest({
	    QuestName = "The Dead Scar",
		QuestID = 8475,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 9374.8388671875, y = -6966.5756835938, z = 18.078981399536, id = 15416 }, -- do not forget commas
		TurnIn = { x = 9374.8388671875, y = -6966.5756835938, z = 18.078981399536, id = 15416 },
		Profile = function()
		GMR.DefineQuestEnemyId(15654)
		GMR.DefineProfileCenter(9281.7060546875, -6949.1337890625, 5.498610496521, 150)
		GMR.DefineProfileCenter(9266.3447265625, -6973.2451171875, 4.3676228523254, 100)
		DefineVendors("Falconwing Square")
		end	
	})
	GMR.DefineQuest({
	    QuestName = "Pelt Collection",
		QuestID = 8491,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 8755.6123046875, y = -6690.0532226562, z = 69.604080200195, id = 15417 }, -- do not forget commas
		TurnIn = { x = 8755.6123046875, y = -6690.0532226562, z = 69.604080200195, id = 15417 },
		Profile = function()
		GMR.DefineQuestEnemyId(15651)
		GMR.DefineProfileCenter(8778.865234375, -6777.5639648438, 58.73157119751, 150)
		GMR.DefineProfileCenter(8849.72265625, -6774.8149414062, 36.332714080811, 150)
		GMR.DefineProfileCenter(8851.4482421875, -6702.3505859375, 41.941394805908, 150)
		GMR.DefineProfileCenter(8888.3779296875, -6620.580078125, 33.465408325195, 150)
		DefineVendors("Eversong Woods")
		end	
	})
	GMR.DefineQuest({
		QuestName = "Situation at Sunsail Anchorage",
		QuestID = 8892,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 8716.26953125, y = -6622.22265625, z = 80.681999206543, id = 15939 }, -- do not forget commas
		TurnIn = { x = 8716.26953125, y = -6622.22265625, z = 80.681999206543, id = 15939 },
		Profile = function()
		GMR.DefineQuestEnemyId(15645)
		GMR.DefineQuestEnemyId(16162)
		GMR.DefineProfileCenter(8811.29296875, -6195.8188476562, 6.86736536026, 100)
		GMR.DefineProfileCenter(8772.73828125, -6191.779296875, 7.8955607414246, 100)
		GMR.DefineProfileCenter(8724.8212890625, -6151.8139648438, 14.204508781433, 100)
		GMR.DefineProfileCenter(8695.0751953125, -6091.421875, 17.715879440308, 100)
		GMR.DefineProfileCenter(8733.7734375, -6014.3061523438, 7.5739593505859, 100)
		GMR.DefineProfileCenter(8761.4990234375, -5971.634765625, 5.3311429023743, 100)
		DefineVendors("Eversong Woods")
		end	
	})
	GMR.DefineQuest({
	    QuestName = "Defending Fairbreeze Village",
		QuestID = 9252,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 8684.6298828125, y = -6798.7094726562, z = 94.016128540039, id = 15942 }, -- do not forget commas
		TurnIn = { x = 8684.6298828125, y = -6798.7094726562, z = 94.016128540039, id = 15942 },
		Profile = function()
		GMR.DefineQuestEnemyId(15658)
		GMR.DefineQuestEnemyId(15657)
		GMR.DefineProfileCenter(8575.7861328125, -6973.2236328125, 76.345626831055, 150)
		GMR.DefineProfileCenter(8479.583984375, -6973.396484375, 92.51781463623, 150)
		GMR.DefineProfileCenter(8424.609375, -6994.4150390625, 93.336196899414, 150)
		DefineVendors("Eversong Woods")
		end	
	})
	GMR.DefineQuest({
	    QuestName = "Farstrider Retreat",
		QuestID = 9359,
		QuestType = "TalkTo",
		Race = "BloodElf",
		PickUp = { x = 8716.26953125, y = -6622.22265625, z = 80.681999206543, id = 15939 }, -- do not forget commas
		TurnIn = { x = 8980.837890625, y = -7458.359375, z = 86.689262390137, id = 15399 },
		Profile = function()
		DefineVendors("Eversong Woods")
		end	
	})
	GMR.DefineQuest({
	    QuestName = "Grind to Level 10",
		QuestType = "GrindTo",
		Level = 10,
		Race = "BloodElf",
		Profile = function()
		GMR.DefineProfileCenter(9015.0029296875, -7592.3432617188, 123.29552459717, 150)
		GMR.DefineProfileCenter(8982.5126953125, -7643.0546875, 135.60087585449, 150)
		GMR.DefineProfileCenter(9016.0302734375, -7680.6225585938, 143.41110229492, 150)
		GMR.DefineProfileCenter(9061.005859375, -7699.6020507812, 137.51458740234, 150)
		GMR.DefineProfileCenter(9098.62890625, -7611.923828125, 114.37740325928, 150)
		GMR.DefineProfileCenter(9114.5322265625, -7544.8676757812, 95.045112609863, 150)
		DefineVendors("Eversong Woods")
		GMR.SkipTurnIn(true)
		end	
	})
	GMR.DefineQuest({
	QuestName = "Farstrider Retreat: PickUp 2 Quests",
	QuestType = "MassPickUp",
	Race = "BloodElf", -- either this or you can also put -> Faction = "Alliance" -- if its for all races
	MassPickUp = { 
		{ questId = 8476, x = 8980.837890625, y = -7458.359375, z = 86.689262390137, id = 15399},
		{ questId = 8477, x = 8986.42578125, y = -7419.0659179688, z = 89.404273986816, id = 15400}
	},
	Profile = function()
		DefineVendors("Eversong Woods")
	end	
	})
	GMR.DefineQuest({
	    QuestName = "Amani Encroachment",
		QuestID = 8476,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 8980.837890625, y = -7458.359375, z = 86.689262390137, id = 15399 }, -- do not forget commas
		TurnIn = { x = 8980.837890625, y = -7458.359375, z = 86.689262390137, id = 15399 },
		Profile = function()
		GMR.DefineQuestEnemyId(15643)
		GMR.DefineQuestEnemyId(15641)
		GMR.DefineProfileCenter(8654.2431640625, -7883.580078125, 155.96890258789, 150)
		GMR.DefineProfileCenter(8631.7412109375, -7873.6713867188, 155.93214416504, 150)
		GMR.DefineProfileCenter(8565.7041015625, -7924.9018554688, 154.91151428223, 150)
		DefineVendors("Eversong Woods")
		GMR.SkipTurnIn(true)
		end	
	})
	GMR.DefineQuest({
	    QuestName = "The Spearcrafter's Hammer",
		QuestID = 8477,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 8986.42578125, y = -7419.0659179688, z = 89.404273986816, id = 15400}, -- do not forget commas
		TurnIn = { x = 8986.42578125, y = -7419.0659179688, z = 89.404273986816, id = 15400},
		Profile = function()
		GMR.DefineQuestEnemyId(15408)
		GMR.DefineProfileCenter(8662.3603515625, -7933.0380859375, 152.69790649414, 150)
		DefineVendors("Eversong Woods")
		GMR.SkipTurnIn(true)
		end	
	})
	GMR.DefineQuest({
	QuestName = "Farstrider Retreat TurnIn 2 Quests",
	QuestType = "MassTurnIn",
	Race = "BloodElf", -- either this or you can also put -> Faction = "Alliance" -- if its for all races
	MassTurnIn = { 
		{ questId = 8476, x = 8980.837890625, y = -7458.359375, z = 86.689262390137, id = 15399},
		{ questId = 8477, x = 8986.42578125, y = -7419.0659179688, z = 89.405616760254, id = 15400}
	},
	Profile = function()
		DefineVendors("Eversong Woods")
		GMR.SkipTurnIn(false) -- here we reset the turn in skip
	end	
	})
	GMR.DefineQuest({
	    QuestName = "Grind to Level 12",
		QuestType = "GrindTo",
		Level = 12,
		Race = "BloodElf",
		Profile = function()
		GMR.DefineProfileCenter(8815.4765625, -7768.0971679688, 161.0322265625, 150)
		GMR.DefineProfileCenter(8810.7939453125, -7809.1572265625, 170.14976501465, 150)
		GMR.DefineProfileCenter(8778.3681640625, -7850.2006835938, 174.26420593262, 150)
		GMR.DefineProfileCenter(8710.5869140625, -7838.5908203125, 167.62043762207, 150)
		GMR.DefineProfileCenter(8658.84375, -7841.41015625, 161.37414550781, 150)
		GMR.DefineProfileCenter(8615.2802734375, -7882.7143554688, 155.22477722168, 150)
		GMR.DefineProfileCenter(8563.0302734375, -7912.697265625, 154.91151428223, 150)
		GMR.DefineProfileCenter(8574.017578125, -7938.0092773438, 154.86192321777, 150)
		GMR.DefineProfileCenter(8512.28125, -7666.2939453125, 149.15222167969, 150)
		GMR.DefineProfileCenter(8510.890625, -7597.00390625, 151.26724243164, 150)
		GMR.DefineProfileCenter(8414.2109375, -7607.9340820312, 159.44639587402, 150)
		GMR.DefineSellVendor(8983.7421875, -7443.4829101562, 87.078155517578, 16263)
		GMR.DefineRepairVendor(8983.7421875, -7443.4829101562, 87.078155517578, 16263)
		GMR.DefineGoodsVendor(8990.8759765625, -7462.5825195312, 86.685615539551, 16443)
		GMR.DefineAmmoVendor(8983.7421875, -7443.4829101562, 87.077713012695, 16263)		
		end	
	})
	GMR.DefineQuest({
	    QuestName = "Grind to Level 14",
		QuestType = "GrindTo",
		Level = 14,
		Race = "BloodElf",
		Profile = function()
		GMR.DefineQuestEnemyId(16313)
		GMR.DefineProfileCenter(8482.0126953125, -7648.6650390625, 150.78730773926, 150)
		GMR.DefineProfileCenter(8168.4819335938, -7516.541015625, 149.92184448242, 150)
		GMR.DefineProfileCenter(8172.751953125, -7552.5268554688, 152.27032470703, 150)
		GMR.DefineProfileCenter(8165.0791015625, -7581.9555664062, 153.32743835449, 150)
		GMR.DefineProfileCenter(8133.1245117188, -7588.0258789062, 157.27377319336, 150)
		GMR.DefineProfileCenter(8076.875, -7582.0581054688, 150.3232421875, 150)
		GMR.DefineProfileCenter(8047.2084960938, -7541.36328125, 149.41883850098, 150)
		GMR.DefineProfileCenter(8079.9702148438, -7431.7983398438, 145.68838500976, 150)
		GMR.DefineProfileCenter(7999.2299804688, -7430.10546875, 145.2938079834, 150)
		GMR.DefineProfileCenter(7943.8354492188, -7384.5961914062, 142.2419128418, 150)
		GMR.DefineProfileCenter(7950.3715820312, -7309.5043945312, 139.6682434082, 150)
		GMR.DefineProfileCenter(8020.3061523438, -7366.2260742188, 141.6941986084, 150)
		GMR.DefineSellVendor(8983.7421875, -7443.4829101562, 87.078155517578, 16263)
		GMR.DefineRepairVendor(8983.7421875, -7443.4829101562, 87.078155517578, 16263)
		GMR.DefineGoodsVendor(8990.8759765625, -7462.5825195312, 86.685615539551, 16443)
		GMR.DefineAmmoVendor(8983.7421875, -7443.4829101562, 87.077713012695, 16263)		
		end	
	})
	GMR.DefineQuest({
	    QuestName = "The Fallen Courier",
		QuestID = 9147,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 8117.9946289062, y = -6901.5302734375, z = 70.345634460449, id = 16196 }, -- do not forget commas
		TurnIn = { x = 8117.9946289062, y = -6901.5302734375, z = 70.345634460449, id = 16196 },
		Profile = function()
		GMR.DefineProfileCenter(7967.4521484375, -6941.2939453125, 65.92505645752, 150)
		GMR.DefineProfileCenter(7951.7158203125, -6886.8149414062, 62.814498901367, 150)
		GMR.DefineProfileCenter(7967.59375, -6838.3979492188, 56.693634033203, 150)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)	
		end	
	})
	GMR.DefineQuest({
	    QuestName = "Delivery to Tranquillien",
		QuestID = 9148,
		QuestType = "TalkTo",
		Race = "BloodElf",
		PickUp = { x = 8119.7143554688, y = -6899.6235351562, z = 70.760627746582, id = 16183  }, -- do not forget commas
		TurnIn = { x = 7642.4477539062, y = -6816.7514648438, z = 79.037315368652, id = 16197 },
		Profile = function()
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineHearthstoneBindLocation(7553.3696289062, -6897.3525390625, 96.027099609375, 16542)
		end	
	})
	GMR.DefineQuest({
	    QuestName = "The Forsaken",
		QuestID = 9329,
		QuestType = "TalkTo",
		Race = "BloodElf",
		PickUp = { x = 7642.4477539062, y = -6816.7514648438, z = 79.037315368652, id = 16197 }, -- do not forget commas
		TurnIn = { x = 7552.796875, y = -6760.87890625, z = 91.235481262207, id = 16252 },
		Profile = function()
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineHearthstoneBindLocation(7553.3696289062, -6897.3525390625, 96.027099609375, 16542)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)	
		end	
	})
	GMR.DefineQuest({
	    QuestName = "Return to Arcanist Vandril",
		QuestID = 9758,
		QuestType = "TalkTo",
		Race = "BloodElf",
		PickUp = { x = 7552.796875, y = -6760.87890625, z = 91.235481262207, id = 16252 }, -- do not forget commas
		TurnIn = { x = 7642.4477539062, y = -6816.7514648438, z = 79.037315368652, id = 16197 },
		Profile = function()
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)	
		end	
	})
	GMR.DefineQuest({
	QuestName = "Tranquillien PickUp Two Quests",
	QuestType = "MassPickUp",
	Race = "BloodElf", -- either this or you can also put -> Faction = "Alliance" -- if its for all races
	MassPickUp = { 
		{ questId = 9149, x = 7499.6459960938, y = -6856.2446289062, z = 85.396492004395, id = 16198},
		{ questId = 9152, x = 7637.673828125, y = -6843.02734375, z = 83.460540771484, id = 16224 }
	},
	Profile = function()
		GMR.SkipTurnIn(true) -- here we reset the turn in skip
	end	
	})
	GMR.DefineQuest({
		QuestName = "Tomber's Supplies",
		QuestID = 9152,
		QuestType = "Custom",
		Race = "BloodElf",
		PickUp = { x = 7637.673828125, y = -6843.02734375, z = 83.460540771484, id = 16224 }, -- do not forget commas
		TurnIn = { x = 7637.673828125, y = -6843.02734375, z = 83.460540771484, id = 16224 },
		Profile = function()
			GMR.DefineMailboxLocation(7579.0200195312, -6860.1196289062, 93.349311828613, 181883)
			GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
			GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
			GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
			GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
			GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)	
		end,
		Lua = function()
		if not GMR.Questing.IsObjectiveCompleted(9152) then
			if GMR.IsPlayerPosition(7682.548828125, -6390.578125, 20.676553726196) then
				GMR.SetDelay("Executing", 5)
				GMR.MoveTo(7682.548828125, -6390.578125, 20.676553726196)
				return
			end
			local x, y, z, id = 7682.548828125, -6390.578125, 20.676553726196, 181133
			if not GMR.IsPlayerPosition(x, y, z, 6) then 
				GMR.MeshTo(x, y, z)
			elseif GMR.IsMoving() then 
				GMR.StopMoving()
			else 
				local object = GMR.GetObjectWithInfo({ id = id, rawType = 8, isInteractable = true })
				if object then 
					GMR.Interact(object)		
				end
			end	
		end
	GMR.SkipTurnIn(true)		
	end
	})
	GMR.DefineQuest({
	    QuestName = "The Plagued Coast",
		QuestID = 9149,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 7499.6459960938, y = -6856.2446289062, z = 85.396492004395, id = 16198 }, -- do not forget commas
		TurnIn = { x = 7499.6459960938, y = -6856.2446289062, z = 85.396492004395, id = 16198 },
		Profile = function()
		GMR.DefineQuestEnemyId(16402)
		GMR.DefineQuestEnemyId(16403)
		GMR.DefineProfileCenter(7864.8872070312, -5975.53125, 2.3475730419159, 100)
		GMR.DefineProfileCenter(7885.5483398438, -5936.4951171875, 1.0635201931, 100)
		GMR.DefineProfileCenter(7881.5498046875, -5906.2294921875, 1.7017489671707, 100)
		GMR.DefineProfileCenter(7831.6533203125, -5947.7543945312, 1.4952529668808, 150)
		GMR.DefineProfileCenter(7796.0659179688, -5959.69140625, 1.6189634799957, 150)
		GMR.DefineMailboxLocation(7579.0200195312, -6860.1196289062, 93.349311828613, 181883)
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)	
		GMR.SkipTurnIn(true)	
		end
	})
	GMR.DefineQuest({
	QuestName = "Tranquillien Turn In Two Quests",
	QuestType = "MassTurnIn",
	Race = "BloodElf", -- either this or you can also put -> Faction = "Alliance" -- if its for all races
	MassTurnIn = { 
		{ questId = 9149, x = 7499.6459960938, y = -6856.2446289062, z = 85.396492004395, id = 16198 },
		{ questId = 9152, x = 7637.673828125, y = -6843.02734375, z = 83.460540771484, id = 16224 }
	},
	Profile = function()
		GMR.DefineMailboxLocation(7579.0200195312, -6860.1196289062, 93.349311828613, 181883)
		GMR.SkipTurnIn(false) -- here we reset the turn in skip
	end	
	})
	GMR.DefineQuest({
	QuestName = "Tranquillien Pickup 2 Quests, And Travel To quest Area",
	QuestType = "MassPickUp",
	Race = "BloodElf", -- either this or you can also put -> Faction = "Alliance" -- if its for all races
	MassPickUp = { 
		{ questId = 9138, x = 7643.2846679688, y = -6806.6865234375, z = 78.961952209473, id = 16197},
		{ questId = 9315, x = 7938.3803710938, y = -7182.3369140625, z = 133.67260742188, id = 16601 }
	},
	Profile = function()
		GMR.DefineMailboxLocation(7579.0200195312, -6860.1196289062, 93.349311828613, 181883)
		GMR.SkipTurnIn(true) -- here we reset the turn in skip
	end	
	})
	GMR.DefineQuest({
	    QuestName = "Suncrown Village",
		QuestID = 9138,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 7643.2846679688, y = -6806.6865234375, z = 78.961952209473, id = 16197 }, -- do not forget commas
		TurnIn = { x = 7643.2846679688, y = -6806.6865234375, z = 78.961952209473, id = 16197 },
		Profile = function()
		GMR.DefineQuestEnemyId(16313)
		GMR.DefineQuestEnemyId(16357)
		GMR.DefineProfileCenter(7956.5454101562, -7285.125, 137.38304138184, 150)
		GMR.DefineProfileCenter(7958.7592773438, -7329.0717773438, 140.20971679688, 150)
		GMR.DefineProfileCenter(8001.3881835938, -7352.771484375, 140.47512817383, 150)
		GMR.DefineProfileCenter(8033.2939453125, -7342.4995117188, 141.09071350098, 150)
		GMR.DefineProfileCenter(8063.818359375, -7294.263671875, 142.30046081543, 150)
		GMR.DefineProfileCenter(8024.0141601562, -7242.359375, 141.52062988281, 150)
		GMR.DefineProfileCenter(7984.4760742188, -7245.2231445312, 137.91575622558, 150)
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)	
		GMR.SkipTurnIn(true)	
		end
	})
	GMR.DefineQuest({
	    QuestName = "Anok'suten",
		QuestID = 9315,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 7938.3803710938, y = -7182.3369140625, z = 133.67245483398, id = 16601 }, -- do not forget commas
		TurnIn = { x = 7643.021484375, y = -6807.8110351562, z = 78.94474029541, id = 16197 },
		Profile = function()
		GMR.DefineQuestEnemyId(16357)
		GMR.DefineProfileCenter(7956.5454101562, -7285.125, 137.38304138184, 150)
		GMR.DefineProfileCenter(7958.7592773438, -7329.0717773438, 140.20971679688, 150)
		GMR.DefineProfileCenter(8001.3881835938, -7352.771484375, 140.47512817383, 150)
		GMR.DefineProfileCenter(8033.2939453125, -7342.4995117188, 141.09071350098, 150)
		GMR.DefineProfileCenter(8063.818359375, -7294.263671875, 142.30046081543, 150)
		GMR.DefineProfileCenter(8024.0141601562, -7242.359375, 141.52062988281, 150)
		GMR.DefineProfileCenter(7984.4760742188, -7245.2231445312, 137.91575622558, 150)
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)	
		GMR.SkipTurnIn(true)	
		end
	})
	GMR.DefineQuest({
	QuestName = "Tranquillien Turn In Two Quests",
	QuestType = "MassTurnIn",
	Race = "BloodElf", -- either this or you can also put -> Faction = "Alliance" -- if its for all races
	MassTurnIn = { 
		{ questId = 9315, x = 7643.021484375, y = -6807.8110351562, z = 78.94474029541, id = 16197  },
		{ questId = 9138, x = 7643.021484375, y = -6807.8110351562, z = 78.94474029541, id = 16197 }
	},
	Profile = function()
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)	
		GMR.SkipTurnIn(false) -- here we reset the turn in skip
	end	
	})
	GMR.DefineQuest({
	QuestName = "Tranquillien Pickup 3 Quests, And Travel To quest Area",
	QuestType = "MassPickUp",
	Race = "BloodElf", -- either this or you can also put -> Faction = "Alliance" -- if its for all races
	MassPickUp = { 
		{ questId = 9139, x = 7642.5581054688, y = -6815.8061523438, z = 78.982978820801, id = 16197},
		{ questId = 9171, x = 7586.095703125, y = -6881.4150390625, z = 95.02400970459, id = 16253},
		{ questId = 9145, x = 7552.8471679688, y = -6784.1499023438, z = 89.333549499512, id = 16213 }
	},
	Profile = function()
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)	
		GMR.SkipTurnIn(true) -- here we reset the turn in skip
	end	
	})
	GMR.DefineQuest({
	    QuestName = "Culinary Crunch",
		QuestID = 9171,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 7586.095703125, y = -6881.4150390625, z = 95.02400970459, id = 16253 }, -- do not forget commas
		TurnIn = { x = 7586.095703125, y = -6881.4150390625, z = 95.02400970459, id = 16253 },
		Profile = function()
		GMR.DefineQuestEnemyId(16350)
		GMR.DefineProfileCenter(7691.3549804688, -6296.44140625, 28.561653137207, 150)
		GMR.DefineProfileCenter(7716.458984375, -6267.3676757812, 23.226989746094, 150)
		GMR.DefineProfileCenter(7669.0756835938, -6210.91796875, 22.84824180603, 150)
		GMR.DefineProfileCenter(7594.673828125, -6211.228515625, 22.200483322144, 150)
		GMR.DefineProfileCenter(7597.333984375, -6249.1943359375, 30.781408309937, 150)
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)	
		GMR.SkipTurnIn(true)	
		end
	})
	GMR.DefineQuest({
	    QuestName = "Goldenmist Village",
		QuestID = 9139,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 7642.5581054688, y = -6815.8061523438, z = 78.982978820801, id = 16197 }, -- do not forget commas
		TurnIn = { x = 7642.5581054688, y = -6815.8061523438, z = 78.982978820801, id = 16197 },
		Profile = function()
		GMR.DefineQuestEnemyId(16326)
		GMR.DefineQuestEnemyId(16325)
		GMR.DefineProfileCenter(7860.6118164062, -6117.8559570312, 16.731435775757, 150)
		GMR.DefineProfileCenter(7871.8134765625, -6077.8579101562, 17.067699432373, 150)
		GMR.DefineProfileCenter(7908.7075195312, -6062.1489257812, 15.500351905823, 150)
		GMR.DefineProfileCenter(7933.9614257812, -6091.541015625, 15.690487861633, 150)
		GMR.DefineProfileCenter(7914.8115234375, -6125.9169921875, 15.604954719543, 150)
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)	
		GMR.SkipTurnIn(true)	
		end
	})
	GMR.DefineQuest({
	    QuestName = "Help Ranger Valanna!",
		QuestID = 9145,
		QuestType = "TalkTo",
		Race = "BloodElf",
		PickUp = { x = 7552.8471679688, y = -6784.1499023438, z = 89.333549499512, id = 16213 }, -- do not forget commas
		TurnIn = { x = 7932.6162109375, y = -7573.5502929688, z = 145.41928100586, id = 16219 },
		Profile = function()
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)	
		GMR.SkipTurnIn(false)	
		end
	})
	GMR.DefineQuest({
	    QuestName = "Dealing with Zeb'Sora",
		QuestID = 9143,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 7932.6162109375, y = -7573.5502929688, z = 145.41870117188, id = 16219 }, -- do not forget commas
		TurnIn = { x = 7932.6162109375, y = -7573.5502929688, z = 145.41870117188, id = 16219 },
		Profile = function()
		GMR.DefineQuestEnemyId(16340)
		GMR.DefineQuestEnemyId(16341)
		GMR.DefineProfileCenter(7937.4892578125, -7756.9135742188, 153.1068572998, 150)
		GMR.DefineProfileCenter(7956.8198242188, -7817.5209960938, 171.53952026367, 150)
		GMR.DefineProfileCenter(8025.9653320312, -7825.1811523438, 173.57029724121, 150)
		GMR.DefineProfileCenter(8091.07421875, -7816.4169921875, 169.42774963379, 150)
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)	
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.SkipTurnIn(true)	
		end
	})
	GMR.DefineQuest({
	QuestName = "Tranquillien Turn In 3 Quests",
	QuestType = "MassTurnIn",
	Race = "BloodElf", -- either this or you can also put -> Faction = "Alliance" -- if its for all races
	MassTurnIn = { 
		{ questId = 9143, x = 7932.6162109375, y = -7573.5502929688, z = 145.41870117188, id = 16219  },
		{ questId = 9139, x = 7642.5581054688, y = -6815.8061523438, z = 78.982978820801, id = 16197  },
		{ questId = 9171, x = 7586.095703125, y = -6881.4150390625, z = 95.02400970459, id = 16253 }
	},
	Profile = function()
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)
		GMR.SkipTurnIn(false) -- here we reset the turn in skip
	end	
	})
	GMR.DefineQuest({
	QuestName = "Tranquillien Pickup In 4 Quests",
	QuestType = "MassPickUp",
	Race = "BloodElf", -- either this or you can also put -> Faction = "Alliance" -- if its for all races
	MassPickUp = { 
		{ questId = 9150, x = 7563.7055664062, y = -6802.0795898438, z = 87.035102844238, id = 16199  },
		{ questId = 9140, x = 7642.4477539062, y = -6816.7514648438, z = 79.037315368652, id = 16197  },
		{ questId = 9155, x = 7528.0688476562, y = -6801.9008789062, z = 83.366676330566, id = 16200  },
		{ questId = 9160, x = 7551.5244140625, y = -6764.3491210938, z = 91.253059387207, id = 16231 }
	},
	Profile = function()
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)
		GMR.SkipTurnIn(true) -- here we reset the turn in skip
	end	
	})
	GMR.DefineQuest({
	    QuestName = "Investigate An'daroth",
		QuestID = 9160,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 7551.5244140625, y = -6764.3491210938, z = 91.253211975098, id = 16231 }, -- do not forget commas
		TurnIn = { x = 7551.5244140625, y = -6764.3491210938, z = 91.253211975098, id = 16231 },
		Profile = function()
		GMR.DefineQuestEnemyId(16330)
		GMR.DefineProfileCenter(7918.4458007812, -6516.6000976562, 46.891674041748, 150)
		GMR.DefineProfileCenter(7929.5556640625, -6508.3862304688, 46.708248138428, 150)
		GMR.DefineProfileCenter(7944.2114257812, -6480.97265625, 48.447666168213, 150)
		GMR.DefineProfileCenter(7979.150390625, -6490.6411132812, 53.819557189941, 150)
		GMR.DefineProfileCenter(7999.0244140625, -6540.9096679688, 61.657531738281, 150)
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.SkipTurnIn(true)	
		end
	})
	GMR.DefineQuest({
	    QuestName = "Down the Dead Scar",
		QuestID = 9155,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 7528.0688476562, y = -6801.9008789062, z = 83.367118835449, id = 16200 }, -- do not forget commas
		TurnIn = { x = 7528.0688476562, y = -6801.9008789062, z = 83.367118835449, id = 16200 },
		Profile = function()
		GMR.DefineQuestEnemyId(16301)
		GMR.DefineQuestEnemyId(16309)
		GMR.DefineProfileCenter(7462.9594726562, -6606.5234375, 9.3097553253174, 150)
		GMR.DefineProfileCenter(7423.9375, -6575.1147460938, 10.860686302185, 150)
		GMR.DefineProfileCenter(7415.9951171875, -6537.85546875, 11.788257598877, 150)
		GMR.DefineProfileCenter(7310.48046875, -6568.1762695312, 10.495284080505, 150)
		GMR.DefineProfileCenter(7259.3793945312, -6541.6303710938, 11.999146461487, 150)
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.SkipTurnIn(true)	
		end
	})
	GMR.DefineQuest({
	    QuestName = "Salvaging the Past",
		QuestID = 9150,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 7563.7055664062, y = -6802.0795898438, z = 87.035545349121, id = 16199 }, -- do not forget commas
		TurnIn = { x = 7563.7055664062, y = -6802.0795898438, z = 87.035545349121, id = 16199 },
		Profile = function()
		GMR.DefineQuestEnemyId(16310)
		GMR.DefineQuestEnemyId(16304)
		GMR.DefineProfileCenter(7572.7436523438, -6439.126953125, 10.472791671753, 150)
		GMR.DefineProfileCenter(7575.16015625, -6417.8603515625, 12.483565330505, 150)
		GMR.DefineProfileCenter(7580.1938476562, -6373.8637695312, 12.790665626526, 100)
		GMR.DefineProfileCenter(7566.7993164062, -6317.3310546875, 13.842296600342, 150)
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.SkipTurnIn(true)	
		end
	})
	GMR.DefineQuest({
	    QuestName = "Windrunner Village",
		QuestID = 9140,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 7642.4477539062, y = -6816.7514648438, z = 79.037315368652, id = 16197 }, -- do not forget commas
		TurnIn = { x = 7642.4477539062, y = -6816.7514648438, z = 79.037315368652, id = 16197},
		Profile = function()
		GMR.DefineQuestEnemyId(16324)
		GMR.DefineQuestEnemyId(16323)
		GMR.DefineProfileCenter(7307.0087890625, -5930.3764648438, 14.069805145264, 150)
		GMR.DefineProfileCenter(7271.1616210938, -5896.1806640625, 14.534068107605, 150)
		GMR.DefineProfileCenter(7234.7768554688, -5929.5703125, 17.908489227295, 150)
		GMR.DefineProfileCenter(7233.0942382812, -5988.2412109375, 19.675239562988, 150)
		GMR.DefineProfileCenter(7248.3520507812, -6007.8754882812, 18.305841445923, 150)
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.SkipTurnIn(true)	
		end
	})
	GMR.DefineQuest({
	QuestName = "Tranquillien Turn In 4 Quests",
	QuestType = "MassTurnIn",
	Race = "BloodElf", -- either this or you can also put -> Faction = "Alliance" -- if its for all races
	MassTurnIn = { 
		{ questId = 9150, x = 7563.7055664062, y = -6802.0795898438, z = 87.035102844238, id = 16199  },
		{ questId = 9140, x = 7642.4477539062, y = -6816.7514648438, z = 79.037315368652, id = 16197  },
		{ questId = 9155, x = 7528.0688476562, y = -6801.9008789062, z = 83.366676330566, id = 16200  },
		{ questId = 9160, x = 7551.5244140625, y = -6764.3491210938, z = 91.253059387207, id = 16231 }
	},
	Profile = function()
		GMR.DefineRepairVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineSellVendor(7555.6752929688, -6857.7250976562, 93.579139709473, 16528)
		GMR.DefineGoodsVendor(7553.3696289062, -6897.3525390625, 96.026519775391, 16542)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.SkipTurnIn(false) -- here we reset the turn in skip
	end	
	})
	GMR.DefineQuest({
	    QuestName = "Bearers of the Plague",
		QuestID = 9158,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 7559.6943359375, y = -7675.6303710938, z = 151.27635192871, id = 16202 }, -- do not forget commas
		TurnIn = { x = 7559.6943359375, y = -7675.6303710938, z = 151.27635192871, id = 16202 },
		Profile = function()
		GMR.DefineQuestEnemyId(16348)
		GMR.DefineProfileCenter(7448.0532226562, -7697.2119140625, 130.93656921387, 150)
		GMR.DefineProfileCenter(7401.0571289062, -7699.9848632812, 122.3592300415, 150)
		GMR.DefineProfileCenter(7357.580078125, -7647.8706054688, 111.68474578857, 150)
		GMR.DefineProfileCenter(7394.3940429688, -7569.8432617188, 101.49643707275, 150)
		GMR.DefineProfileCenter(7525.8134765625, -7560.1264648438, 136.47023010254, 150)
		GMR.DefineRepairVendor(7561.8837890625, -7663.4975585938, 151.28189086914, 16274)
		GMR.DefineSellVendor(7561.8837890625, -7663.4975585938, 151.28189086914, 16274)
		GMR.DefineGoodsVendor(7555.501953125, -7668.7587890625, 151.28343200684, 17656)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		end
	})
	GMR.DefineQuest({
	QuestName = "Farstrider Enclave Pick up 2 Quests",
	QuestType = "MassPickUp",
	Race = "BloodElf", -- either this or you can also put -> Faction = "Alliance" -- if its for all races
	MassPickUp = { 
		{ questId = 9159, x = 7559.6943359375, y = -7675.6303710938, z = 151.27680969238, id = 16202  },
		{ questId = 9281, x = 7025.408203125, y = -6814.5947265625, z = 42.039497375488, id = 16480 }
	},
	Profile = function()
		GMR.DefineRepairVendor(7561.8837890625, -7663.4975585938, 151.28189086914, 16274)
		GMR.DefineSellVendor(7561.8837890625, -7663.4975585938, 151.28189086914, 16274)
		GMR.DefineGoodsVendor(7555.501953125, -7668.7587890625, 151.28343200684, 17656)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.SkipTurnIn(true) -- here we reset the turn in skip
	end	
	})
	GMR.DefineQuest({
	    QuestName = "Curbing the Plague",
		QuestID = 9159,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 7559.6943359375, y = -7675.6303710938, z = 151.27635192871, id = 16202 }, -- do not forget commas
		TurnIn = { x = 7559.6943359375, y = -7675.6303710938, z = 151.27635192871, id = 16202 },
		Profile = function()
		GMR.DefineQuestEnemyId(16354)
		GMR.DefineQuestEnemyId(16351)
		GMR.DefineProfileCenter(7365.4780273438, -6457.7875976562, 23.876058578491, 150)
		GMR.DefineProfileCenter(7387.083984375, -6416.2485351562, 32.876365661621, 150)
		GMR.DefineProfileCenter(7414.771484375, -6331.2153320312, 39.311630249023, 150)
		GMR.DefineProfileCenter(7367.7407226562, -6302.9145507812, 44.298080444336, 150)
		GMR.DefineProfileCenter(7320.2543945312, -6385.1186523438, 38.27645111084, 150)
		GMR.DefineRepairVendor(7561.8837890625, -7663.4975585938, 151.28189086914, 16274)
		GMR.DefineSellVendor(7561.8837890625, -7663.4975585938, 151.28189086914, 16274)
		GMR.DefineGoodsVendor(7555.501953125, -7668.7587890625, 151.28343200684, 17656)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.SkipTurnIn(true)
		end
	})
	GMR.DefineQuest({
	    QuestName = "Clearing the Way",
		QuestID = 9281,
		QuestType = "Grinding",
		Race = "BloodElf",
		PickUp = { x = 7025.408203125, y = -6814.5947265625, z = 42.039497375488, id = 16480 }, -- do not forget commas
		TurnIn = { x = 7025.408203125, y = -6814.5947265625, z = 42.039497375488, id = 16480 },
		Profile = function()
		GMR.DefineQuestEnemyId(16349)
		GMR.DefineQuestEnemyId(16352)
		GMR.DefineProfileCenter(6948.7314453125, -6644.6000976562, 16.95245552063, 150)
		GMR.DefineProfileCenter(6900.7192382812, -6649.6982421875, 23.065294265747, 150)
		GMR.DefineProfileCenter(6930.4189453125, -6722.8676757812, 25.095453262329, 150)
		GMR.DefineProfileCenter(7058.7329101562, -6652.49609375, 30.498725891113, 150)
		GMR.DefineRepairVendor(7561.8837890625, -7663.4975585938, 151.28189086914, 16274)
		GMR.DefineSellVendor(7561.8837890625, -7663.4975585938, 151.28189086914, 16274)
		GMR.DefineGoodsVendor(7555.501953125, -7668.7587890625, 151.28343200684, 17656)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.SkipTurnIn(true)
		end
	})
	GMR.DefineQuest({
	QuestName = "Farstrider Enclave Turn in 2 Quests",
	QuestType = "MassTurnIn",
	Race = "BloodElf", -- either this or you can also put -> Faction = "Alliance" -- if its for all races
	MassTurnIn = { 
		{ questId = 9281, x = 7025.408203125, y = -6814.5947265625, z = 42.039497375488, id = 16480  },
		{ questId = 9159, x = 7559.6943359375, y = -7675.6303710938, z = 151.27680969238, id = 16202 }
	},
	Profile = function()
		GMR.DefineRepairVendor(7561.8837890625, -7663.4975585938, 151.28189086914, 16274)
		GMR.DefineSellVendor(7561.8837890625, -7663.4975585938, 151.28189086914, 16274)
		GMR.DefineGoodsVendor(7555.501953125, -7668.7587890625, 151.28343200684, 17656)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		GMR.SkipTurnIn(false) -- here we reset the turn in skip
	end	
	})
	GMR.DefineQuest({
	    QuestName = "Grind to Level 18",
		QuestType = "GrindTo",
		Level = 18,
		Race = "BloodElf",
		Profile = function()
		GMR.DefineProfileCenter(7069.9985351562, -6902.4291992188, 46.918483734131, 150)
		GMR.DefineProfileCenter(7137.1997070312, -6949.6625976562, 48.176906585693, 150)
		GMR.DefineProfileCenter(7133.7529296875, -6999.1279296875, 47.577503204346, 150)
		GMR.DefineProfileCenter(7065.2807617188, -7075.5322265625, 44.856201171875, 150)
		GMR.DefineProfileCenter(6989.6997070312, -7037.0322265625, 53.626026153564, 150)
		GMR.DefineProfileCenter(6920.0263671875, -6998.2192382812, 45.2453956604, 150)
		GMR.DefineProfileCenter(6879.7744140625, -7049.2709960938, 48.193279266357, 150)
		GMR.DefineProfileCenter(6868.6357421875, -7105.9653320312, 44.596626281738, 150)
		GMR.DefineRepairVendor(7561.8837890625, -7663.4975585938, 151.28189086914, 16274)
		GMR.DefineSellVendor(7561.8837890625, -7663.4975585938, 151.28189086914, 16274)
		GMR.DefineGoodsVendor(7555.501953125, -7668.7587890625, 151.28343200684, 17656)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)	
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)
		end	
	})
	GMR.DefineQuest({
	    QuestName = "Grind to Level 21",
		QuestType = "GrindTo",
		Level = 21,
		Faction = "Horde",
		Profile = function()
		GMR.DefineProfileCenter(7058.7329101562, -6652.49609375, 30.498725891113, 150)
		GMR.DefineProfileCenter(6975.3930664062, -7073.1767578125, 49.708698272705, 150)
		GMR.DefineProfileCenter(6959.3559570312, -6954.03125, 48.573249816895, 150)
		GMR.DefineProfileCenter(6917.8061523438, -6942.5346679688, 44.98509979248, 150)
		GMR.DefineProfileCenter(7000.1811523438, -6884.173828125, 53.149032592773, 150)
		GMR.DefineProfileCenter(7019.0205078125, -7051.0439453125, 51.772838592529, 150)
		GMR.DefineRepairVendor(7561.8837890625, -7663.4975585938, 151.28189086914, 16274)
		GMR.DefineSellVendor(7561.8837890625, -7663.4975585938, 151.28189086914, 16274)
		GMR.DefineGoodsVendor(7555.501953125, -7668.7587890625, 151.28343200684, 17656)
		GMR.DefineAmmoVendor(7622.9677734375, -6845.6108398438, 83.889297485352, 16187)
		GMR.DefineProfileMailbox(7579.0200195312, -6860.1196289062, 93.349311828613)	
		end	
	})
	GMR.DefineQuest({
	    QuestName = "Grind to Level 24",
		QuestType = "GrindTo",
		Level = 24,
		Faction = "Horde",
		Profile = function()
		GMR.DefineQuestEnemyId(3424)
		GMR.DefineQuestEnemyId(3466)
		GMR.DefineQuestEnemyId(3239)
		GMR.DefineQuestEnemyId(3263)
		GMR.DefineQuestEnemyId(3261)
		GMR.DefineProfileCenter(-1948.5395507812, -2763.2451171875, 91.671508789062, 100)
		GMR.DefineProfileCenter(-1974.8317871094, -2719.0710449219, 92.060386657715, 100)
		GMR.DefineProfileCenter(-2035.5545654297, -2627.0617675781, 92.293395996094, 100)
		GMR.DefineProfileCenter(-2013.8781738281, -2597.0625, 95.279685974121, 100)
		GMR.DefineProfileCenter(-2005.6378173828, -2535.6000976562, 95.082183837891, 100)
		GMR.DefineProfileCenter(-2040.7546386719, -2511.26953125, 93.087890625, 100)
		GMR.DefineProfileCenter(-2113.453125, -2494.5356445312, 91.667663574219, 100)
		GMR.DefineProfileCenter(-2176.9399414062, -2526.4807128906, 91.692077636719, 100)
		GMR.DefineProfileCenter(-2234.2326660156, -2598.6450195312, 95.129791259766, 100)
		GMR.DefineProfileCenter(-2221.5759277344, -2634.7878417969, 91.824424743652, 100)
		GMR.DefineProfileCenter(-2192.5344238281, -2704.6384277344, 91.667091369629, 100)
		GMR.DefineRepairVendor(-2374.2587890625, -1948.7938232422, 96.086715698242, 10380)
		GMR.DefineSellVendor(-2374.2587890625, -1948.7938232422, 96.086715698242, 10380)
		GMR.DefineGoodsVendor(-2376.267578125, -1995.7365722656, 96.705146789551, 7714)
		GMR.DefineAmmoVendor(-351.18438720703, -2556.5119628906, 95.78768157959, 3488)
		GMR.DefineProfileMailbox(-2351.5825195312, -1944.7510986328, 95.791107177734)
		GMR.BlacklistId(3236)
		GMR.BlacklistId(3235)
		GMR.BlacklistId(3237)
		GMR.BlacklistId(5797)
		GMR.BlacklistId(5798)
		GMR.BlacklistId(5799)
		GMR.BlacklistId(5800)
		end	
	})
	GMR.DefineQuest({
	    QuestName = "Grind to Level 28",
		QuestType = "GrindTo",
		Level = 28,
		Faction = "Horde",
		Profile = function()
		GMR.DefineQuestEnemyId(3250)
		GMR.DefineQuestEnemyId(3251)
		GMR.DefineQuestEnemyId(3238)
		GMR.DefineQuestEnemyId(3249)
		GMR.DefineQuestEnemyId(4128)
		GMR.DefineProfileCenter(-3201.7023925781, -1878.6802978516, 92.938262939453, 100)
		GMR.DefineProfileCenter(-3181.1352539062, -1845.2199707031, 95.539924621582, 100)
		GMR.DefineProfileCenter(-3141.5021972656, -1816.8955078125, 101.47399139404, 100)
		GMR.DefineProfileCenter(-3097.0571289062, -1783.8038330078, 91.891311645508, 100)
		GMR.DefineProfileCenter(-3070.943359375, -1685.3428955078, 92.536903381348, 100)
		GMR.DefineProfileCenter(-3140.0249023438, -1696.9893798828, 92.327697753906, 150)
		GMR.DefineProfileCenter(-3199.5615234375, -1676.9182128906, 92.546524047852, 100)
		GMR.DefineProfileCenter(-3263.7805175781, -1749.3518066406, 91.668075561523, 100)
		GMR.DefineProfileCenter(-3331.0583496094, -1865.9245605469, 92.722915649414, 100)
		GMR.DefineProfileCenter(-3436.2531738281, -1839.4534912109, 91.66716003418, 100)
		GMR.DefineProfileCenter(-3509.1616210938, -1838.6540527344, 91.666664123535, 100)
		GMR.DefineProfileCenter(-3585.0754394531, -1892.3405761719, 93.077407836914, 150)
		GMR.DefineRepairVendor(-2374.2587890625, -1948.7938232422, 96.086715698242, 10380)
		GMR.DefineSellVendor(-2374.2587890625, -1948.7938232422, 96.086715698242, 10380)
		GMR.DefineGoodsVendor(-2376.267578125, -1995.7365722656, 96.705146789551, 7714)
		GMR.DefineAmmoVendor(-351.18438720703, -2556.5119628906, 95.78768157959, 3488)
		GMR.DefineProfileMailbox(-2351.5825195312, -1944.7510986328, 95.791107177734)
		GMR.BlacklistId(3236)
		GMR.BlacklistId(3235)
		GMR.BlacklistId(3237)
		GMR.BlacklistId(5797)
		GMR.BlacklistId(5798)
		GMR.BlacklistId(5799)
		GMR.BlacklistId(5800)
		end	
	})
	GMR.DefineQuest({
	    QuestName = "Grind to Level 30",
		QuestType = "GrindTo",
		Level = 30,
		Faction = "Horde",
		Profile = function()
		GMR.DefineProfileCenter(-5281.4194335938, -1821.9285888672, -56.890739440918, 150)
		GMR.DefineProfileCenter(-5363.9135742188, -1807.4241943359, -49.231117248535, 150)
		GMR.DefineProfileCenter(-5472.5991210938, -1853.9183349609, -54.565017700195, 150)
		GMR.DefineProfileCenter(-5555.2231445313, -1944.2895507813, -60.132743835449, 150)
		GMR.DefineProfileCenter(-5473.9331054688, -1982.0762939453, -58.619117736816, 90)
		GMR.DefineProfileCenter(-5439.064453125, -2059.826171875, -64.648506164551, 90)
		GMR.DefineProfileCenter(-5477.4477539063, -2112.7307128906, -58.712070465088, 90)
		GMR.DefineProfileCenter(-5414.9384765625, -2212.7182617188, -57.872669219971, 90)
		GMR.DefineProfileCenter(-5519.3569335938, -2264.9475097656, -59.526023864746, 90)
		GMR.DefineRepairVendor(-5453.3486328125, -2408.3784179688, 89.276596069336, 9551)
		GMR.DefineSellVendor(-5453.3486328125, -2408.3784179688, 89.276596069336, 9551)
		GMR.DefineAmmoVendor(-5453.3486328125, -2408.3784179688, 89.276596069336, 9551)
		GMR.DefineGoodsVendor(-5467.5400390625, -2432.6389160156, 89.699806213379, 4875)
		end
	})
end)

GMR.LoadQuester("Rooni")


GMR.CreateTableEntry("Unstuck")
GMR.DefineUnstuck(8782.009765625, -7187.31640625, 48.08948135376, 4)
GMR.DefineUnstuck(8781.7197265625, -7169.994140625, 41.687114715576)
GMR.DefineUnstuck(8799.8349609375, -7137.6567382812, 40.930744171143)
GMR.CreateTableEntry("Unstuck")
GMR.DefineUnstuck(8788.4521484375, -7189.001953125, 54.653335571289, 5)
GMR.DefineUnstuck(8781.5166015625, -7187.919921875, 48.271125793457)
GMR.CreateTableEntry("Unstuck")
GMR.DefineUnstuck(8938.73828125, -7456.5034179688, 84.315223693848, 5)
GMR.DefineUnstuck(8925.6591796875, -7464.509765625, 85.053718566895)
GMR.DefineUnstuck(8934.5830078125, -7467.20703125, 83.950836181641)
GMR.DefineMeshAreaBlacklist(10328.61328125, -6477.4516601562, 50.905918121338, 5)