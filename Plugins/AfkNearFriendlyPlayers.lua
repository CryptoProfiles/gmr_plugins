C_Timer.NewTicker(0.5, function()
	if not GMR.IsExecuting() or GMR.UnitIsDeadOrGhost("player") then 
		return 
	end 
	for i = 1, #GMR.Tables.FriendlyPlayers do 
		local friendlyPlayer = GMR.Tables.FriendlyPlayers[i]
		if GMR.ObjectExists(friendlyPlayer)
		and GMR.GetDistance("player", friendlyPlayer, "<", 40)
		and not GMR.InCombat("player", true) then 
			if not GMR.GetDelay("Execute") then 
				GMR.ClearTarget();
				GMR.ResetSetObject();
				GMR.StopMoving()
			end
			GMR.SetDelay("Execute", 2);
			if GMR.IsRecovering() then 
				GMR.Recover()
			elseif GMR.IsPreparing() then 
				GMR.Prepare()
			end 
			break
		end 
	end
end)