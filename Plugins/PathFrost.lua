local pathoffrost = GetSpellInfo(3714)
    C_Timer.NewTicker(1, function()
      if not GMR.InCombat() and GMR.IsCastable(pathoffrost) and not GMR.HasBuff("player", pathoffrost) then
          GMR.Cast(pathoffrost)
      end
    end)