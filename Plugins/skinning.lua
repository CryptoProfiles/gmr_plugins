--skinning plugin
C_Timer.NewTicker(0.3, function()
  if GMR.IsSpellKnown("Skinning") and GMR.IsItemInBags(7005) and Skinning_Checkbox:GetChecked() then
   local objectlist = {GMR.GetNearbyObjects(30)}
    if #objectlist[1] then
      for key, arr in pairs(objectlist[1]) do
        local objectN = GMR.GetObjectWithInfo({id = arr.ID, rawType = 5})
        local x1, y1, z1 = GMR.ObjectPosition(objectN)
        local distplayerobjectN = GMR.GetDistance(objectN, "player", ">", 5)

        if not GMR.IsAlive(objectN) and 
        not GMR.IsObjectLootable(objectN) and
        GMR.IsObjectSkinnable(objectN) and
        distplayerobjectN and
        not GMR.InCombat("player") then
          GMR.SetDelay("Execute", 0.5)
          GMR.MeshTo(x1, y1, z1)
        elseif not GMR.IsAlive(objectN) and 
        not GMR.IsObjectLootable(objectN) and
        GMR.IsObjectSkinnable(objectN) and 
        not distplayerobjectN and 
        not GMR.IsCasting("player") then
          GMR.SetDelay("Execute", 0.5)
          GMR.InteractObject(objectN)
        end
      end
    end
  end
end)
