-- AlteracValleyClustering
-- Author: Mons#0885
-- Version: 0.0.3

-- Usage, run with [Alterac Valley] - Clustering
-- For unstucks and archer blacklist

-- WORK IN PROGRESS
function GMR.IsValidClusterPosition(x, y, z)
    -- TODO: distinguish between horde and alliance blacklist
    -- TODO: add more battlegrounds and points of interest
    local faction, _ = GMR.GetFaction("player")
    local isAlliance = true

    battlegroundsAlliance = {
        ["Alterac Valley"] = {
            { 212.868, -364.971, 42.396, 50 }, -- Icewing bunker
            { -571.584, -262.509, 68.469, 50}, -- Iceblood Tower
            { -768.397, -362.27, 84.355, 50 }, -- Tower Point
            {-1189.243, -333.602, 49.236, 200}, -- Horde base entryway 200 yards ( mesh is broken )
            {-945.605, -122.969, 78.457, 150}, -- Coldtooth mine
            {758.323, -488.956, 96.015, 100}, --Alliance Gatebe
            {712.367, -14.763, 50.135, 70} -- Vans room ( no reason to be here as alliance )
        }
    }
    battlegroundsHorde = {
        ["Alterac Valley"] = {
            { 212.868, -364.971, 42.396, 50 }, -- Icewing bunker
            { -571.584, -262.509, 68.469, 50}, -- Iceblood Tower
            { -768.397, -362.27, 84.355, 50 }, -- Tower Point
            {-945.605, -122.969, 78.457, 150}, -- Coldtooth mine
            {-1383.133, -546.642, 54.921, 100} -- Horde gate
    }}
    local bgInfo = GMR.GetBattlegroundInfo()
    if bgInfo then
        local blacklist = {}
        if isAlliance and battlegroundsAlliance[bgInfo.Name] then
            blacklist = battlegroundsAlliance[bgInfo.Name]
        elseif not isAlliance and battlegroundsHorde[bgInfo.Name] then
            blacklist = battlegroundsHorde[bgInfo.Name]
        end

        for i = 1, #blacklist do
            local bX, bY, bZ, bRadius = blacklist[i][1], blacklist[i][2], blacklist[i][3], blacklist[i][4]
            if GMR.GetDistanceBetweenPositions(x, y, z, bX, bY, bZ) <= bRadius then
                return false
            end
        end
    end
    return true
end
function GMR.GetNearestPartyPlayerCluster()
    local countMembers, countEnemies, clusterUnit = 0, 0, nil
    local partyType, partyMemberMax
    local faction, _ = GMR.GetFaction("player")
    local isAlliance = true
    if faction == "Horde" then
        isAlliance = false
    end

    if UnitInRaid("player") then
        partyType, partyMemberMax = "raid", UnitInRaid("player") * 5
    elseif UnitInParty("player") then
        partyType, partyMemberMax = "party", 5
    end
    if not partyType then
        return
    end
    -- save all cluster units that does not make us go "backwards"
    local bestClusterUnits = {}
    for i = 1, partyMemberMax do
        local unit = partyType .. i
        if not UnitExists(unit) then
            break
        end
        if not UnitIsDeadOrGhost(unit) and not UnitIsUnit("player", unit) and GMR.ObjectExists(unit) then
            local membersAroundUnit = GMR.GetNumPartyMembersAroundUnit(unit)
            local enemiesAroundUnit = GMR.GetNumEnemyPlayersAroundUnit(unit)

            -- This adds all valid clusterUnits
            countMembers, clusterUnit = membersAroundUnit, unit
            if clusterUnit then
                local cY, cX, cZ = GMR.ObjectPosition(clusterUnit)
                local pY, _,_ = GMR.ObjectPosition("player")
                local isValid = GMR.IsValidClusterPosition(cY,cX,cZ)
                if isValid then
                    if clusterUnit and cY and pY then
                        table.insert(bestClusterUnits,
                            { countMembers = countMembers, countEnemies = countEnemies, clusterUnit = clusterUnit, distY = cY, memberRatio=(countMembers+1) / (enemiesAroundUnit+1) })
                    end
                end
            end
        end
    end

    local bestClusterUnit = nil
    for k, v in pairs(bestClusterUnits) do
        if bestClusterUnit == nil and v then
            bestClusterUnit = v
        else
            -- Here we can do some scoring of the clusters.
            -- In initial testing i thought clusters closer to south was better, its actually not
            -- Clusters that are big in comparison to enemies are much better for honor, doesnt matter where they are
            
            -- If cluster has more members or better member to enemy ratio
            if v.countMembers >= bestClusterUnit.countMembers or v.memberRatio >= bestClusterUnit.memberRatio then
                
                -- Figure out a good weight for size versus ratio. 
                local dRatio = v.memberRatio - bestClusterUnit.memberRatio
                local dCount = v.countMembers - bestClusterUnit.countMembers

                local score = dRatio * 2 + dCount
                if score > 0 then
                    bestClusterUnit = v
                end
            end     
        end
    end
    if bestClusterUnit ~= nil and GMR.ObjectPosition(bestClusterUnit.clusterUnit) then
        return GMR.ObjectPointer(bestClusterUnit.clusterUnit), bestClusterUnit.countMembers,
            bestClusterUnit.countEnemies
    else
        return nil,0,0
    end
end

-- Hook the GMR.UpdateBattlegroundCluster() function
function GMR.UpdateBattlegroundCluster()
    local preparationOnPlayer = AuraUtil.FindAuraByName(GetSpellInfo(44521), "player", "HELPFUL")
    if not GMR.GetDelay("DefineCluster") and GMR.IsInBattleground() and not GMR.IsBattlegroundFinished() and
        not preparationOnPlayer then
        GMR.SetDelay("DefineCluster", 1)
        if GMR.Variables.ClusterUnit then

            local clusterUnit, countMembers, countEnemies = GMR.GetNearestPartyPlayerCluster() -- This function is also hooked
            local membersAroundClusterUnit = GMR.GetNumPartyMembersAroundUnit(GMR.Variables.ClusterUnit)
            local enemiesAroundClusterUnit = GMR.GetNumEnemyPlayersAroundUnit(GMR.Variables.ClusterUnit)
            if (not GMR.Variables.ClusterUnit
                or not GMR.ObjectPosition(GMR.Variables.ClusterUnit)
                or GMR.UnitIsDeadOrGhost(GMR.Variables.ClusterUnit)
                or membersAroundClusterUnit == 0
                or (countMembers and membersAroundClusterUnit + 2 < countMembers)
                or (enemiesAroundClusterUnit == 0 and countEnemies and countEnemies > 0)) then
                if clusterUnit then
                    GMR.Variables.ClusterUnit = clusterUnit;
                    GMR.SetDelay("DenyBattlegroundDefaultPath", 3);
                    GMR.Log("DEFINE_CLUSTER_UNIT")
                else
                    print("Clearing clusterUnit")
                    GMR.Variables.ClusterUnit = nil;
                    GMR.Log("RESET_CLUSTER_UNIT")
                end
            end
            -- TODO Check that the current clusterUnit isent jank and remove it if it moves into ban area
            local cY, cX, cZ = GMR.ObjectPosition( GMR.Variables.ClusterUnit)
            if not GMR.IsValidClusterPosition(cY,cX,cZ) then
                print("Clearing clusterUnit not valid position")
                GMR.Variables.ClusterUnit = nil;
                GMR.Log("RESET_CLUSTER_UNIT")
            end
        else
            local clusterUnit, countMembers, countEnemies = GMR.GetNearestPartyPlayerCluster()
            if clusterUnit and not preparationOnPlayer then
                GMR.Variables.ClusterUnit = clusterUnit;
                GMR.SetDelay("DenyBattlegroundDefaultPath", 3);
                GMR.Log("DEFINE_CLUSTER_UNIT")
            end
        end
        
    end
end

-- String multiple dices together for a vector of random inputs
bg_actions = {
    [1] = function(x)
        print("Moving forwards")
        GMR.MoveForwardStart()
        C_Timer.After(3, GMR.MoveForwardStop)
    end,
    [2] = function(x)
        print("Turning left")
        GMR.MoveForwardStart()
        GMR.TurnLeftStart()
        C_Timer.After(1, GMR.TurnLeftStop)
        C_Timer.After(3, GMR.MoveForwardStop)
    end,
    [3] = function(x)
        print("Turning right")
        GMR.MoveForwardStart()
        GMR.TurnRightStart()
        C_Timer.After(1, GMR.TurnRightStop)
        C_Timer.After(3, GMR.MoveForwardStop)
    end,
    [4] = function(x)
        print("Jumping")
        GMR.Jump()
    end,
}

-- start the main loop ( starts when preparation buff is not present )
print("Loaded Mons BG Plugin")
local faceClusterunit = false
local avGateFlag = false
C_Timer.NewTicker(0.5, function()
    if GMR.IsInBattleground() and GetZoneText() == "Alterac Valley" then
        local player = GMR.GetPlayerPointer("player")
        local clusterUnit = GMR.Variables.ClusterUnit
        local preparationOnPlayer = AuraUtil.FindAuraByName(GetSpellInfo(44521), "player", "HELPFUL")

        if clusterUnit and player and not preparationOnPlayer then
            GMR.FocusUnit(clusterUnit)
            local clusterUnitName, _ = GMR.UnitName(clusterUnit)
            local cu_currentSpeed, cu_maxRunSpeed = GMR.GetUnitSpeed(clusterUnit)
            local distanceToClusterUnit = GMR.GetDistanceBetweenObjects(player, clusterUnit)

            local cu_spellName, _, _, cu_castingTimeStart, cu_castingTimeEnd = GMR.UnitCastingInfo("focus")
            -- Dismount if within mounting range of clusterUnit, and clusterUnit is dismounted
            if cu_maxRunSpeed and distanceToClusterUnit then
                if cu_maxRunSpeed < 9.0 and distanceToClusterUnit < GMR.GetGroundMountingRange() then
                    if IsMounted("player") then
                        Dismount()
                    end
                end
                if cu_maxRunSpeed > 9.0 and not IsMounted("player") and
                    distanceToClusterUnit < GMR.GetGroundMountingRange() and not GMR.InCombat(player) and IsOutdoors() then
                    print("Casting mount")
                    GMR.SetDelay("DefineCluster", 2)
                    CastSpellByName(GMR_SavedVariablesPerCharacter.SelectedMount)
                end
                -- If we are close to the clusterUnit, and the clusterUnit is standing still, we initialize
                -- the random movement block
                -- Its a random dice throw between 4 actions we repeat twice
                -- Its pretty jank but i think its okay
                if cu_currentSpeed and cu_currentSpeed < 1.0 and distanceToClusterUnit < 20.0 and
                    not GMR.InCombat(player) then

                    if not faceClusterunit and distanceToClusterUnit < 7.0 then
                        -- If we are right on the clusterUnit, face away
                        print("bg_plugin: facing clusterUnit and turning away")
                        GMR.FaceDirection(clusterUnit)
                        C_Timer.After(0.5, GMR.MoveBackwardStart)
                        C_Timer.After(0.8, GMR.TurnRightStart)

                        C_Timer.After(1.8, GMR.TurnRightStop) -- 180 turn
                        C_Timer.After(2.0, GMR.MoveBackwardStop)
                        faceClusterunit = true

                    end
                end
            else
                faceClusterunit = false
            end
        end
    else
        GMR.ClearFocus()
    end
end)
