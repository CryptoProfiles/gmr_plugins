C_Timer.NewTicker(0.5, function()
  if AuraUtil.FindAuraByName("Frost Nova", "target", "HARMFUL") or AuraUtil.FindAuraByName("Frostbite", "target", "HARMFUL") then
    if GMR.GetDistance("player", "target", "<", 6) then
      print("Move Away")
      GMR.SpellStopCasting()
      GMR.SetDelay("Execute", 1)
      -- GMR.Jump()
      GMR.MoveBackwardStart()
      C_Timer.After(1, GMR.MoveBackwardStop)
    end
  end
end)