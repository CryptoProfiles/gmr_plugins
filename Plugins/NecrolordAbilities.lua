C_Timer.NewTicker(1, function()
  local spellTable = {
    ["Druid"] = GetSpellInfo(325727),        --- Druid, Adaptive Swarm
    ["Death Knight"] = GetSpellInfo(315443),  --- Death Knight, Abomination Limb
    ["Hunter"] = GetSpellInfo(325028),       --- Hunter, Death Chakram
    ["Monk"] = GetSpellInfo(325216),         --- Monk, Bonedust Brew
    ["Paladin"] = GetSpellInfo(328204),      --- Paladin, Vanquishers Hammer
    ["Priest"] = GetSpellInfo(324724),       --- Priest, Unholy Nova
    ["Rogue"] = GetSpellInfo(328547),        --- Rogue, Serrated Bone Spike
    ["Shaman"] = GetSpellInfo(326059),       --- Shaman, Primodial Wave
    ["Warlock"] = GetSpellInfo(325289),      --- Warlock, Decimating Bolt
    ["Warrior"] = GetSpellInfo(324143),      --- Warrior, Conqueror's Banner
    ["Mage"] = GetSpellInfo(324220),         --- Mage, Deathborne
  }

  mainAbility = spellTable[UnitClass("player")]

  local enemiesAttacking = GMR.GetNumAttackingEnemies("player", 60)
  mounted = IsMounted()

  if not mounted and GMR.IsExecuting() then
    if GetSpellCooldown(324631) == 0 and GMR.GetHealth("player") <= 90 then
      GMR.SpellStopCasting()
      GMR.Cast(GetSpellInfo(324631)) -- Cast Fleshcraft
    end
    if UnitClass("player") == "Demon Hunter" then
      return
    end
    if enemiesAttacking >= 2 or (UnitClassification("target") == "elite")  then
      if GetSpellInfo(mainAbility) and
        GMR.GetHealth("player") <= 90 and
        UnitAffectingCombat("player") and
        not GMR.HasDebuff("target",mainAbility) and
        GetSpellCooldown(mainAbility) == 0 and
        GMR.InLoS("target") and
        GMR.IsSpellInRange(mainAbility, "target") == 1 and
        UnitExists("target") then
          print("Casting " .. mainAbility)
          GMR.SpellStopCasting()
          if UnitClass("player") == "Monk" then
            GMR.Cast(mainAbility)
            local x1, y1, z1 = GMR.ObjectPosition("target")
            GMR.ClickPosition(x1, y1, z1)
          else
            GMR.Cast(mainAbility)
          end
      end
    end
  end
end)
