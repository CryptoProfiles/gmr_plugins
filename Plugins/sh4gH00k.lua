---,,_.-+~^~+._,,_.-+~^~+._,,_.-+~^~+._,,_.-+~^~+._,,_.-+~^~+._,
---,,   Name:                   -sh4gH00k-
---,,   Usage:                  Custom Discord Webhook Plugin & Guide
---,,   Type:                   Plugin, Guide
---,,   Release Date:           11.22.2022 - Nov 22 22 (v0.04)
---,,   Author:                 sh4ggY#9999
---,,   Credits:                ----->  Mons#0885 - Alpha alpha tester, helped me test, suggested sub.string() which was a game-changer for formatting
---,,   Credits(cont.):         ----->  Orway#1731 - Wrote some great functions for this as well as helped a lot with data structure.
---     Rights:                 Free usage For GMR Users and Developers ;
------  Rights, cont.:          MIT No Attribution
--
--Permission is hereby granted, free of charge, to any person obtaining a copy of this
--software and associated documentation files (the "Software"), to deal in the Software
--without restriction, including without limitation the rights to use, copy, modify,
--merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
--permit persons to whom the Software is furnished to do so.
--
--THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
--INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
--PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
--HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
--OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
--SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE
---,,
---,,  ██████  ██░ ██  ▄▄▄        ▄████   ▄████▓██   ██▓
---,,▒██    ▒ ▓██░ ██▒▒████▄     ██▒ ▀█▒ ██▒ ▀█▒▒██  ██▒
---,,░ ▓██▄   ▒██▀▀██░▒██  ▀█▄  ▒██░▄▄▄░▒██░▄▄▄░ ▒██ ██░
---,,  ▒   ██▒░▓█ ░██ ░██▄▄▄▄██ ░▓█  ██▓░▓█  ██▓ ░ ▐██▓░
---,,▒██████▒▒░▓█▒░██▓ ▓█   ▓██▒░▒▓███▀▒░▒▓███▀▒ ░ ██▒▓░
---,,▒ ▒▓▒ ▒ ░ ▒ ░░▒░▒ ▒▒   ▓▒█░ ░▒   ▒  ░▒   ▒   ██▒▒▒
---,,░ ░▒  ░ ░ ▒ ░▒░ ░  ▒   ▒▒ ░  ░   ░   ░   ░ ▓██ ░▒░
--,, ░  ░  ░   ░  ░░ ░  ░   ▒   ░ ░   ░ ░ ░   ░ ▒ ▒ ░░
--,,      ░   ░  ░  ░      ░  ░      ░       ░ ░ ░
--,,                                           ░ ░
--,,
--,,  Thank you for trying sh4gH00k! Feel free to reach out to me with feedback, comments, and requests.
--,,
--,,                                       AUTHOR: sh4ggY#9999
---,, @GUIDE BEGIN
---,, Description:
---,, Every 15 seconds, the plugin will grab some data from the bot running, and then distribute it to the webhook URL of your choice.
---,, Simple Setup ->
---,,,,,,,,,,    :.:.:.: --- Install Guide:
---,,:.:.:.: --- :.:.:.:   1.   Create a new Discord Server
---,,:.:.:.: --- :.:.:.:   2.   Create a new Discord Channel
---,,:.:.:.: --- :.:.:.:   3.   Create a new Discord Channel Webhook (`Edit Channel -> Integrations`)
---,,:.:.:.: --- :.:.:.:   4.   Copy the Webhook link into your GMR Panel, under the `Security` tab.
---,,:.:.:.: --- :.:.:.:   5.   Download `sh4gH00k.lua` and place it in your /plugins/ folder in your unlocker installation.
---,.
C_Timer.NewTicker(15, function()

    ---@CONFIG
    local MODE = 1
    local DEBUG = false
    local TIME_SYSTEM = 24 -- choose 12 or 24 hour time and date system
        ---@VAR
        local webhook = GMR.GetDiscordWebhook()
        local finStr = {}
        local divBeginStr = ":.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:\n"
        local divEndStr = ":.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:.:"

        ---@MAIN LOOP
        if MODE == 1 then
            ---@BUILD DISCORD MESSAGE

            if divBeginStr then
                table.insert(finStr, divBeginStr)
            end

            if TIME_SYSTEM == 12 then
                 if GMR.GetDate() then
                    local day, month, year, hour, min, sec = GMR.GetDate()
                    table.insert(finStr,
                            ":clock1: **Time:**:  `"..
                                    tostring(hour..":"..min..":"..sec).."`\n")
                    table.insert(finStr,
                            ":calendar_spiral: **Date:**  `"..
                                    tostring(month.."."..day.."."..year).."`\n")
                end
            elseif TIME_SYSTEM == 24 then
                    if GMR.GetDate() then
                        local day, month, year, hour, min, sec = GMR.GetDate()
                        table.insert(finStr,
                                ":clock1: **Time:**:  `"..
                                        tostring(hour..":"..min..":"..sec).."`\n")
                        table.insert(finStr,
                                ":calendar_spiral: **Date:**  `"..
                                        tostring(day.."."..month.."."..year).."`\n")
                    end
            else
                GMR.Print("[sh4gH00k]{err0r}: Time System not set to 12 or 24.")
            end

            if GMR.IsExecuting() then
                table.insert(finStr, ":robot: **GMR Active:**  `"..tostring(GMR.IsExecuting()) .. "`\n")
            end

            if GMR.GetProfileName() then
                table.insert(finStr, ":scroll: **Profile:**  `"..tostring(GMR.GetProfileName()) .. "`\n")
            end

            if GMR.GetProfileName() then
                table.insert(finStr, ":scroll: **Type:**  `"..tostring(GMR.GetProfileType()) .. "`\n")
            end

            if GMR.UnitName("player") then
                table.insert(finStr, ":detective: **Agent:**  `"..tostring(GMR.UnitName("player")) .. "`\n")
            end

            if GMR.GetRace("player") then
                table.insert(finStr, ":dna: **Race:**  `"..tostring(GMR.GetRace("player")) .. "`\n")
            end

            if GMR.GetClass("player") then
                table.insert(finStr, ":dna: **Class:**  `"..tostring(GMR.GetClass("player")) .. "`\n")
            end

            if GMR.UnitLevel("player") then
                table.insert(finStr, ":dna: **Level:**  `"..tostring(GMR.UnitLevel("player")) .. "`\n")
            end

            if GMR.UnitHealth("player") and GMR.UnitHealthMax("player") then
                table.insert(finStr,
                        ":heart: **Health:**  `" ..tostring(GMR.UnitHealth("player")).." / "..tostring(GMR.UnitHealthMax("player")).."`\n")
            end
            local curClass = GMR.GetClass("player")
            if curClass == "PRIEST" or "MAGE" or "SHAMAN" or "WARLOCK" or "PALADIN" or "HUNTER" then
                if DEBUG then
                    GMR.Print("[sh4gH00k{DEBUG}: Caster User Detected.")
                end
                if GMR.GetMana("player") ~= nil then
                    table.insert(finStr, ":blue_heart: **Mana:**  `"..string.sub(tostring(GMR.GetMana("player")), 1, 2).."%`\n")
                end
            elseif GMR.GetClass("player") == "WARRIOR" then
                if GMR.GetRage("player") then
                    table.insert(finStr, ":izakaya_lantern: **Rage:**  `"..tostring(GMR.GetRage("player")).."`\n")
                end
            elseif GMR.GetClass("player") == "ROGUE" then
                if GMR.UnitPower("player") then
                    table.insert(finStr, ":zap: **Energy:**  `"..tostring(GMR.UnitPower("player")).."`\n")
                end
                elseif GMR.GetClass("player") == "DRUID" then
                if DEBUG then
                    GMR.Print("[sh4gH00k]{DEBUG}: Druid Detected.")
                end
            else
                if DEBUG then
                    GMR.Print("[sh4gH00k]{err0r}: Error detecting class.")
                end
            end

            if GMR.GetInventorySpace() then
                table.insert(finStr, ":package: **Inventory Space:**  `"..tostring(GMR.GetInventorySpace()) .. "`\n")
            end

            if GMR.GetRepairStatus() then
                table.insert(finStr, ":hammer: **Repair Status:**  `"..string.sub(tostring(GMR.GetRepairStatus()), 1, 2).. "%`\n")
            end

            if GMR.InCombat(player) == true then
                table.insert(finStr, ":crossed_swords: **Combat:**  `"..tostring(GMR.InCombat(player)).. "`\n")
            elseif GMR.InCombat(player) ~= true then
                table.insert(finStr, ":crossed_swords: **Combat:**  `"..tostring(GMR.InCombat(player)).. "`\n")
            end

            if divEndStr then
                table.insert(finStr, divEndStr)
            end
            ---@BUILD FINAL STRING
            local compiledText = ""
            for i = 1, #finStr do
                compiledText = compiledText .. finStr[i]
            end
            ---@SEND
            if GMR.GetDiscordWebhook() ~= nil then
                GMR.SendDiscordMessage(compiledText, GMR.GetDiscordWebhook())
            elseif GMR.GetDiscordWebhook() == nil then
                GMR.Print("[sh4gH00k]--{err0r}: No Webhook added in GMR settings.")
            end
            ---@AUX FUNCTIONS
            if GMR.UnitName("target", true) ~= nil and GMR.InCombat("player")then
                targ = GMR.UnitName("target")
                GMR.SendDiscordMessage(":crossed_swords: :warning: ATTACKING :warning: :crossed_swords: `"..targ.."`", webhook)
            end

            if GMR.UnitName("target", true) ~= nil and GMR.InCombat("player") == false then
                targ = GMR.UnitName("target")
                GMR.SendDiscordMessage(":mag_right: :eye: TARGETING :eye: :mag: `"..targ.."`", webhook)
            end

            if GMR.HasBuff("player", "Rejuvenation") then
                GMR.SendDiscordMessage(":magic_wand: :cactus: HEALING :cactus::magic_wand: `Rejuvenation`", webhook)
            elseif GMR.HasBuff("player", "Regrowth") then
                GMR.SendDiscordMessage(":magic_wand: :cactus: HEALING :cactus: :magic_wand: `Regrowth`", webhook)
            else

            end
            if DEBUG then
                GMR.Print("[sh4gH00k]: Webhook Data Sent.")
            end
        end
end)