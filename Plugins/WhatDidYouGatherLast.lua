local nodesList = {"Cobalt Deposit", "Rich Cobalt Deposit", "Saronite Deposit", "Rich Saronite Deposit", "Titanium Vein", "Goldclover"}

local lastGatheredNode = {Name="none", x=0, y=0, z=0}
local counter = 0;
local countTo = 10; --print the last node every countTo seconds

C_Timer.NewTicker(1.0, function()
      
      local objects = GMR.GetNearbyObjects(6)
      if (objects ~= nil) then
         for i,v in pairs(objects) do 
            if (v ~= nil) then
               for k,z in pairs (nodesList) do
                  if (v.Name == z) then
                     
                     lastGatheredNode.Name = v.Name
                     lastGatheredNode.x = v.x
                     lastGatheredNode.y = v.y
                     lastGatheredNode.z = v.z
                  end
               end
            end
         end
      end
      
      if (counter == countTo) then
         
         print ("Last node gathered is " .. lastGatheredNode.Name .. " at x:" .. lastGatheredNode.x .. " y:" .. lastGatheredNode.y .. " z:" .. lastGatheredNode.z)
         counter = 0   
      else
         counter = counter +1
      end
      
      
end)