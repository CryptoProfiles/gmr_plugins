local shutdown_after_x_minutes = math.random(10, 20)
local shutdown_timer = GetTime()+60*shutdown_after_x_minutes
C_Timer.NewTicker(1, function()
  if shutdown_timer < GetTime() then
    if GMR.GetDiscordWebhook() then
        GMR.SendDiscordMessage("Wow has been closed for "..UnitName("player").." - Time for a break", GMR.GetDiscordWebhook())
    end
    GMR.Shutdown()
  end 
end)


