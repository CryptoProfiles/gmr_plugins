C_Timer.NewTicker(0.5, function()
school = GMR.GetObjectWithInfo({ id = 192059, rawType = 8 })
bobber = GMR.GetObjectWithInfo({ id = 35591, rawType = 8 })
distbobberschool = GMR.GetDistance(school, bobber, "<", 3)
distplayerschool = GMR.GetDistance(school, "player", "<", 21)
fishingspell = GetSpellInfo(51294)
		
		if not IsMounted() and school and not distbobberschool and distplayerschool and GetWeaponEnchantInfo(1) and not GMR.InCombat("player") then
			GMR.SetDelay("Execute", 2)
			GMR.FaceDirection(school)
			GMR.SpellStopCasting()
			GMR.CastSpellByName(fishingspell)
		end
				
		if GMR.UnitChannelInfo("player") and school and distplayerschool and not GMR.InCombat("player") then
			GMR.SetDelay("Execute", 2)
			bobberstatus = GMR.ObjectAnimationFlag(bobber)
			
				if bobberstatus==1 then
					GMR.ObjectInteract(bobber)
				end
		end
		
end)