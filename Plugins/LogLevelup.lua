LevelupLogdir = "Logs"
LevelupLogFile = "Levelup.txt"


---- Function to convert seconds to days, hours, mins, secs ----
local function DisplayTime(TotalSeconds)
  local days    = floor(TotalSeconds/86400)
  local hours   = floor(mod(TotalSeconds, 86400)/3600)
  local minutes = floor(mod(TotalSeconds,3600)/60)
  local seconds = floor(mod(TotalSeconds,60))
  return(days .. " days, " .. hours .. " hours, " .. minutes .. " minutes, " .. seconds .. " seconds")
end


--- Create Frame ---
local PlayedTimeFrame = CreateFrame("Frame");


--- Register Events against frame ---
PlayedTimeFrame:RegisterEvent("TIME_PLAYED_MSG");
PlayedTimeFrame:RegisterEvent("PLAYER_LEVEL_UP");


--- Function to handle TIME_PLAYED_MSG & PLAYER_LEVEL_UP events ---
local function PlayedEventHandler(self, event, ...)
  local XPHourLastLevel
  local PlayedLastLevel_Display

  if event == "PLAYER_LEVEL_UP" then
    RequestTimePlayed()

  elseif event == "TIME_PLAYED_MSG" then
    totalTimePlayed, timePlayedThisLevel = ...

    -- Only log if just levelled up
    if timePlayedThisLevel <= 4 then
      local totalTimePlayed_Display = DisplayTime(totalTimePlayed)
      local CharLevel = GMR.UnitLevel("player")

      if OldLevel == CharLevel - 1 then
        local PlayedLastLevel = totalTimePlayed - timePlayedThisLevel - OldLevelPlayedStart
        PlayedLastLevel_Display = DisplayTime(PlayedLastLevel)
        XPHourLastLevel = string.format("%.0f", (OldLevelXP / PlayedLastLevel) * 60 * 60)
      else
        PlayedLastLevel_Display = "na"
        XPHourLastLevel = 0
      end

      local CharName = UnitName("player")
      local CharRealm = GetRealmName() 
      local CharClass = GMR.GetClass()
      local ProfileName = GMR.GetProfileName()
      local LogMessage = date("%d/%m/%y %H:%M:%S: ") .. "Name:" .. CharName .. " Realm:`" .. CharRealm ..  "` Level:" .. CharLevel .. " Class:" .. CharClass .. " Played:" .. totalTimePlayed_Display .. " CurrentProfile:`"  .. ProfileName .. 
                         "` LastLevel:" .. PlayedLastLevel_Display .. " XP/Hour:" .. XPHourLastLevel
      local GMRDir = GMR.Variables.Directory
      GMR.WriteFile(GMRDir .. "/" .. LevelupLogdir .. "/" .. LevelupLogFile, LogMessage .. "\n", true)

    end 

    --- Update level and level played start for calc on next level up ---
    OldLevel=GMR.UnitLevel("player")
    OldLevelPlayedStart=totalTimePlayed - timePlayedThisLevel
    OldLevelXP = UnitXPMax("player")

  end 

end  -- Function end

---- Define Function to be called by Frame OnEvent ----
PlayedTimeFrame:SetScript("OnEvent",  PlayedEventHandler);

-- Request Played time on initial startup --
RequestTimePlayed()



