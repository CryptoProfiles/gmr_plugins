C_Timer.NewTicker(0.5, function()
	if not GMR.IsExecuting() or GMR.UnitIsDeadOrGhost("player") then 
		return 
	end 
	for i = 1, #GMR.Tables.Attackables do 
		local attackable = GMR.Tables.Attackables[i][1]
		if GMR.ObjectExists(attackable)
		and GMR.UnitIsPlayer(attackable)
		and GMR.GetDistance("player", attackable, "<", 40)
		and not GMR.InCombat("player", true) then 
			if not GMR.GetDelay("Execute") then 
				GMR.ClearTarget();
				GMR.ResetSetObject();
				GMR.StopMoving()
			end
			GMR.SetDelay("Execute", 2);
			if GMR.IsRecovering() then 
				GMR.Recover()
			elseif GMR.IsPreparing() then 
				GMR.Prepare()
			end 
			break
		end 
	end
end)