C_Timer.NewTicker(0.1, function()

  frostNova = GetSpellInfo(27088)
  minimumTargetHealth = 20


  if not mounted and GMR.IsExecuting() and GMR.InLoS("target") and 
  UnitAffectingCombat("player") and UnitExists("target") and 
  GMR.GetHealth("target") >= minimumTargetHealth and GMR.GetDistance("player", "target", "<", 10) then  -- 9.9 yards
    if not AuraUtil.FindAuraByName("Frost Nova", "target", "HARMFUL") and not AuraUtil.FindAuraByName("Frostbite", "target", "HARMFUL") then
      if GetSpellCooldown(frostNova) == 0 then
        print("Frost Nova!")
        GMR.SetDelay("Execute", 1.2)
        GMR.SpellStopCasting()
        GMR.Cast(frostNova)
      end
    end
  end
end)