C_Timer.NewTicker(0.1, function()
	local class2 = GMR.GetClass("player")
	local HpotID = 32905
	local MpotID = 32902
	local drums = 49633
	local ApFlask = 46377
	local boots = 44306
	local HPotName = GetItemInfo(HpotID)
	local MPotName = GetItemInfo(MpotID)
	local drumsName = GetItemInfo(drums)
	local ApFlaskName = GetItemInfo(ApFlask)
	local catForm = GetSpellInfo(768)
	local prowl = GetSpellInfo(5215)
	local drumsBuff = GetSpellInfo(69378)
	local ApFlaskBuff = GetSpellInfo(53760)
	local abolish = GetSpellInfo(2893)
	local VP = GetSpellInfo(16427)
	local bootsName = GetItemInfo(boots)

	if GMR.IsExecuting() then
		if GMR.GetInventorySpace() <= GMR.GetMinimumInventorySpace() and GetZoneText() == "Netherstorm" then
			GMR.RunMacroText("/script ResetInstances();")
		end
		
		if GMR.IsPlayerPosition(3408, 1487, 183, 15) and GMR.GetInventorySpace() > GMR.GetMinimumInventorySpace() and (GMR.HasBuff("player", 32245) or GMR.HasBuff("player", 32246) or GMR.HasBuff("player", 32239)) 
		and GMR.GetNumDungeonTimers() < 5 and GetZoneText() == "Netherstorm" then 
			GMR.Dismount()
		end
		
		if  GetZoneText() == "The Botanica" then 
			if class2 == "DRUID" and not GMR.InCombat("player") and not GMR.HasBuff("player", prowl) then
				if not GMR.HasBuff("player", drumsBuff) and GMR.IsItemInBags(drums) then
					GMR.RunMacroText("/use "..drumsName)
				end
				
				if not GMR.HasBuff("player", ApFlaskBuff) and GMR.IsItemInBags(ApFlask) then
					GMR.RunMacroText("/use "..ApFlaskName)
				end
				if GMR.HasDebuff("player", VP) and not GMR.HasBuff("player", abolish) then
					GMR.CastSpellByName(abolish)
				end
			end
			
			if GMR.ItemExists(boots) and GetItemCooldown(boots) == 0 and not GMR.InCombat("player") and GetUnitSpeed("player") >= 7 then
				GMR.RunMacroText("/use "..bootsName)
				print("boots")
			end
			
			if class2 == "DRUID" and not GMR.InCombat("player") then
				if GMR.IsPlayerPosition(142.30613708496, 299.81820678711, -3.8390989303589, 50) and not GMR.HasBuff("player", prowl) then
					if GMR.IsCastable(catForm) and not GMR.HasBuff("player", catForm) then
						GMR.CastSpellByName(catForm)
					end
				
					if GMR.HasBuff("player", catForm) and GMR.IsCastable(prowl) and GMR.IsPlayerPosition(142.30613708496, 299.81820678711, -3.8390989303589, 40) then
						GMR.CastSpellByName(prowl)
					end
				end
					
				if not GMR.IsPlayerPosition(142.30613708496, 299.81820678711, -3.8390989303589, 50) and GMR.HasBuff("player", prowl) then
					GMR.RunMacroText("/cancelaura "..prowl)
				end
			end
			
			if class2 == "DEATHKNIGHT" and not GMR.InCombat("player") and not GMR.HasBuff("player", drumsBuff) and GMR.IsItemInBags(drums) then
				GMR.RunMacroText("/use "..drumsName)
			end
			
			if (class2 == "DEATHKNIGHT" or class2 == "PALADIN") and not GMR.InCombat("player") and not GMR.HasBuff("player", ApFlaskBuff) and GMR.IsItemInBags(ApFlask) then
				GMR.RunMacroText("/use "..ApFlaskName)
			end
		
			if (class2 == "DRUID" and GMR.GetHealth("player") < 70) or (class2 == "DEATHKNIGHT" and GMR.GetHealth("player") < 60) and GMR.InCombat("player") then
				if GMR.IsItemInBags(HpotID) and GetItemCooldown(HpotID) == 0 then 
					GMR.RunMacroText("/use "..HPotName)
				end
			end
			
			if class2 == "PALADIN" and GMR.GetMana("player") < 40 then
				if GMR.IsItemInBags(MpotID) and GetItemCooldown(MpotID) == 0 then 
					GMR.RunMacroText("/use "..MPotName)
				end
			end
		end
	end
 end)
 
 