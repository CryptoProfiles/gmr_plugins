--Author: Lahtinen#5396
--Edited for all WOTLK schools: Evagoras#0442
--Edited for all schools in WoW: Caver#5714
C_Timer.NewTicker(0.5, function()
    schools = {
        ["Glacial Salmon"] = GMR.GetObjectWithInfo({ id = 192050, rawType = 8 }),
        ["Musselback Sculpin"] = GMR.GetObjectWithInfo({ id = 192046, rawType = 8 }),
        ["Nettlefish"] = GMR.GetObjectWithInfo({ id = 192057, rawType = 8 }),
        ["Dragonfin Angelfish"] = GMR.GetObjectWithInfo({ id = 192048, rawType = 8 }),
        ["Glassfin Minnow"] = GMR.GetObjectWithInfo({ id = 192059, rawType = 8 }),
        ["Imperial Manta Ray"] = GMR.GetObjectWithInfo({ id = 192052, rawType = 8 }),
        ["Borean Man O' War"] = GMR.GetObjectWithInfo({id = 192051, rawType = 8}),
        ["Fangtooth Herring"] = GMR.GetObjectWithInfo({id = 192049, rawType = 8}),
        ["Moonglow Cuttlefish"] = GMR.GetObjectWithInfo({id = 192054, rawType = 8}),
		["Abundant Firefin Snapper School"] = GMR.GetObjectWithInfo({ id = 180902, rawType = 8 }),
		["Abundant Oily Blackmouth School"] = GMR.GetObjectWithInfo({ id = 180900, rawType = 8 }),
		["Bloodsail Wreckage"] = GMR.GetObjectWithInfo({ id = 180901, rawType = 8 }),
		["Bluefish School"] = GMR.GetObjectWithInfo({ id = 182959, rawType = 8 }),
		["Brackish Mixed School"] = GMR.GetObjectWithInfo({ id = 182954, rawType = 8 }),
		["Deep Sea Monsterbelly School"] = GMR.GetObjectWithInfo({ id = 192053, rawType = 8 }),
		["Firefin Snapper School"] = GMR.GetObjectWithInfo({ id = 180683, rawType = 8 }),
		["Floating Debris"] = GMR.GetObjectWithInfo({ id = 180655, rawType = 8 }),
		["Floating Wreckage"] = GMR.GetObjectWithInfo({ id = 180751, rawType = 8 }),
		["Greater Sagefish School"] = GMR.GetObjectWithInfo({ id = 180684, rawType = 8 }),
		["Highland Mixed School"] = GMR.GetObjectWithInfo({ id = 182957, rawType = 8 }),
		["Lesser Oily Blackmouth School"] = GMR.GetObjectWithInfo({ id 180582, = XXXX, rawType = 8 }),
		["Lesser Sagefish School"] = GMR.GetObjectWithInfo({ id 180656 = XXXX, rawType = 8 }),
		["Muddy Churning Waters"] = GMR.GetObjectWithInfo({ id = 180369, rawType = 8 }),
		["Mudfish School"] = GMR.GetObjectWithInfo({ id = 182958, rawType = 8 }),
		["Oil Spill"] = GMR.GetObjectWithInfo({ id = 180661, rawType = 8 }),
		["Oily Blackmouth Schoo"] = GMR.GetObjectWithInfo({ id = 180682, rawType = 8 }),
		["Patch of Elemental Water"] = GMR.GetObjectWithInfo({ id = 180753, rawType = 8 }),
		["Pool of Blood"] = GMR.GetObjectWithInfo({ id = 194479, rawType = 8 }),
		["Pure Water"] = GMR.GetObjectWithInfo({ id = 182951, rawType = 8 }),
		["Sagefish School"] = GMR.GetObjectWithInfo({ id = 180663, rawType = 8 }),
		["School of Darter"] = GMR.GetObjectWithInfo({ id = 182956, rawType = 8 }),
		["School of Deviate Fish"] = GMR.GetObjectWithInfo({ id = 180658, rawType = 8 }),
		["School of Fish Bloom"] = GMR.GetObjectWithInfo({ id = 180184, rawType = 8 }),
		["School of Tastyfish"] = GMR.GetObjectWithInfo({ id = 180248, rawType = 8 }),
		["Schooner Wreckage"] = GMR.GetObjectWithInfo({ id = 180662, rawType = 8 }),
		["Sparse Firefin Snapper School"] = GMR.GetObjectWithInfo({ id = 180657, rawType = 8 }),
		["Sparse Oily Blackmouth School"] = GMR.GetObjectWithInfo({ id = 180664, rawType = 8 }),
		["Sporefish School"] = GMR.GetObjectWithInfo({ id = 182953, rawType = 8 }),
		["Steam Pump Flotsam"] = GMR.GetObjectWithInfo({ id = 182952, rawType = 8 }),
		["Strange Pool"] = GMR.GetObjectWithInfo({ id = 184956, rawType = 8 }),
		["Stonescale Eel Swarm"] = GMR.GetObjectWithInfo({ id = 180712, rawType = 8 }),
		["Teeming Firefin Snapper School"] = GMR.GetObjectWithInfo({ id = 180752, rawType = 8 }),
		["Teeming Oily Blackmouth School"] = GMR.GetObjectWithInfo({ id = 180750, rawType = 8 }),
		["Waterlogged Wreckage"] = GMR.GetObjectWithInfo({ id = 180685, rawType = 8 }),						
		 }
    bobber = GMR.GetObjectWithInfo({ id = 35591, rawType = 8 })
    fishingspell = GetSpellInfo(51294)
    currentCenterIndex = GMR.GetCentralIndex()
    for key, school in pairs(schools) do
        distbobberschool = GMR.GetDistance(school, bobber, "<", 3)
        distplayerschool = GMR.GetDistance(school, "player", "<", 21)
        if not IsMounted() and school and not distbobberschool and distplayerschool and GetWeaponEnchantInfo(1) and not GMR.InCombat("player") then
            GMR.SetDelay("Execute", 2)
            GMR.FaceDirection(school)
            GMR.SpellStopCasting()
            GMR.CastSpellByName(fishingspell)
            GMR.Print("Aiming for "..key.." school!")          
        end              
        if GMR.UnitChannelInfo("player") and school and distplayerschool and not GMR.InCombat("player") then
            GMR.SetDelay("Execute", 2)
            bobberstatus = GMR.ObjectAnimationFlag(bobber)
                if bobberstatus==1 then
                    GMR.ObjectInteract(bobber)
                end
        end
    end
end)
