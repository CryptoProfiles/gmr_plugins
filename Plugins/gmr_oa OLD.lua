-- GMR Object Avoidance by Snoogens

local known_sizes = {
    -- Example (ID as Key, Radius as Value): 
    -- [177025] = 5,
    [190130] = 4, -- Brazier in Onslaught Camp, Dragonblight
    [190131] = 4, -- Brazier in Onslaught Camp, Dragonblight
    [190132] = 4, -- Brazier in Onslaught Camp, Dragonblight
    [190133] = 4, -- Brazier in Onslaught Camp, Dragonblight
    [190134] = 4, -- Brazier in Onslaught Camp, Dragonblight

    [190660] = 5,
    [190661] = 5,
}

local exceptions = {
    233282,
    251353,
    251352,
    303148,
    335621,
    336432,
    233282,
    59271,
    59262,
    191083,
    180379,
    180145,
    191605,
    335620,
    227741,
    227740,
    186812,
    181621,
}

local function FastDistance(x1, y1, z1, x2, y2, z2) return math.sqrt(((x1-x2)^2)+((y1-y2)^2)+((z1-z2)^2)) end
local function GetPositionFromPosition(x, y, z, dist, angle) return math.cos(angle) * dist + x, math.sin(angle) * dist + y, z end

C_Timer.NewTicker(0.1, function()

    if not GMR then return end

    local path = GMR.Variables.MeshPath

    if GMR.IsExecuting and not GMR.IsExecuting() or not path then return end

    if #path > 2 then
        local objects = {
            -- Here you can add your own custom avoidance areas
            -- Keep in mind that this isn't coded to be "smart", so whatever you put here from one zone will carry over to another
            -- Example, x, y, z, being the center of the Circle, Radius being how big the circle should be: 
            -- { x, y, z, radius},
        }

        -- Loop over all objects and collect ones we want to avoid
        for i = 1, GMR.ObjectCount(), 1 do
            local guid = GMR.GetObjectWithIndex(i)
            local type = GMR.ObjectRawType(guid)
            if type == 8 then
                local id = GMR.ObjectId(guid)
                local ox, oy, oz = GMR.ObjectPosition(guid)
                if id and ox and oy and oz and not tContains(exceptions, id) then
                    local orad = (known_sizes[id] or 1.5)
                    table.insert(objects, { ox, oy, oz, orad })
                end
            end
        end

        -- Adjust path points based on proximity to objects
        local extra_buffer = 1.5
        for i, point in ipairs(path) do
            for oi, object in ipairs(objects) do
                if point.x and object[1] then
                    local dist = FastDistance(point.x, point.y, point.z, object[1], object[2], object[3])
                    if dist < (object[4] * extra_buffer) then
                        local ab = GMR.GetAnglesBetweenPositions(object[1], object[2], 0, point.x, point.y, 0)
                        local sx, sy, sz = GetPositionFromPosition(point.x, point.y, point.z, (object[4] * extra_buffer) - dist, ab)
                        point.x, point.y, point.z = sx, sy, sz
                    end
                end
            end
        end
    end
end)
