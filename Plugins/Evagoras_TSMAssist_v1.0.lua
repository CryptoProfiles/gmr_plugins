--TSM Assist v1.0
--Author: Evagoras#0442
--Requires TSM to work.
--For Destroy you need the TSM Destroy window to be showing, so do "/tsm destroy". Make sure to set up TSM settings on what to destroy, to avoid accidents.
--For Auction House you need to do a Post/Cancel Scan with TSM, then it will click the Post/Cancel button when the scan is done.

local loaded = false
if not loaded then
    C_Timer.After(1, function()
        if (IsAddOnLoaded("TradeSkillMaster")) then
            loaded=true
            GMR.Print("TSM Assist Loaded Sucessfully")
            local tsmDestroyButton = createCheckbutton(UIParent, 1300, -5, "TSM Destroy");
            tsmDestroyButton.tooltip = "Toggle TSM Destroy";
            tsmDestroyButton:SetScript("OnClick",
                function()
                    if tsmDestroyButton:GetChecked() == true then
                        GMR.Print("TSM Auto-Destroy Enabled")
                        myTimer = C_Timer.NewTicker(0.5, function()
                        GMR.RunMacroText("/click TSMDestroyBtn")
                        end)
                    else
                        GMR.Print("TSM Auto-Destroy Disabled")
                        myTimer:Cancel()
                    end
                end
            );
            local tsmPostButton = createCheckbutton(UIParent, 1300, -25, "TSM Post");
            tsmPostButton.tooltip = "Toggle TSM Post";
            tsmPostButton:SetScript("OnClick",
                function()
                    if tsmPostButton:GetChecked() == true then
                        GMR.Print("TSM Auto-Post Enabled")
                        myTimer2 = C_Timer.NewTicker(0.5, function()
                        GMR.RunMacroText("/click TSMAuctioningBtn")
                        end)
                    else
                        GMR.Print("TSM Auto-Post Disabled")
                        myTimer2:Cancel()
                    end
                end
            );
        end
    end)
end

--Check box factory function
local uniquealyzer = 1;
function createCheckbutton(parent, x_loc, y_loc, displayname)
    uniquealyzer = uniquealyzer + 1;
    local checkbutton = CreateFrame("CheckButton", "my_addon_checkbutton_0" .. uniquealyzer, parent, "ChatConfigCheckButtonTemplate");
    checkbutton:SetPoint("TOPLEFT", x_loc, y_loc);
    getglobal(checkbutton:GetName() .. 'Text'):SetText(displayname);
    return checkbutton;
end
