--nethergon vapor potion-user for Botanica farm
local useid = 32905
local nethergon = GetItemInfo(useid) -- if you want to use a different item replace the ID here :) 

C_Timer.NewTicker(0.1, function()
        if not IsMounted() and GMR.IsExecuting() then
            if GMR.GetHealth("player") < 30 -- change to the percentage u want/need
             and GMR.IsItemInBags(useid) then
                GMR.UseItemByName(nethergon)
            end
        end
end)