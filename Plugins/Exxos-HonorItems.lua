--OffPiece information
local itemsPositionOp = {} --position the item is in the vendor frame
local itemCostOp = {} --how much that item costs 
						--i.e. Deadly Gladiator Bracers costs 27580 Honor so you would put {27580}
						--a second item, Deadly Gladiator Pendant costs 33180 and then it would become {27580,33180}

--Main Piece information
local itemsPositionMp = {} --position the item is in the vendor frame
local itemCostMp = {} --how much that item costs
						--i.e. Savage Gladiator Gloves costs 35000 Honor so you would put {35000}
						--a second item, Savage Gladiator Legs costs 42000 and then it would become {35000,42000}

--Hateful Piece information
local itemsPositionHp = {} --position the item is in the vendor frame
local itemCostHp = {} --how much that item costs 
						--i.e. Hateful Gladiator Bracers costs 13790 Honor so you would put {13790}
						--a second item, Hateful Gladiator Pendant costs 16590 and then it would become {13790,16590}

--items bought
local itemsBoughtOp = {}
local itemsBoughtMp = {}
local itemsBoughtHp = {}

--this function checks to see if we bought the item already
local function has_value (tab, val)
    for g=1, table.getn(tab) do
        if tab[g] == val then
            return true
        end
    end
    return false
end

--this function grabs the item ID
local function getId (position)
	local t = ""
	local itemId = GetMerchantItemLink(position)
	if itemId ~= nil then
		for k, v in string.gmatch(itemId, "(%w+):(%w+)") do
			t = v
		end
	else
		return 0
	end
	return t
end

C_Timer.NewTicker(1,
	function()
		if UnitInBattleground("player") == nil then
			if GMR.GetFaction("player") == "Alliance" then
				--Alliance vendor logic
				if table.getn(itemsPositionOp) > 0 then
					--Offpiece vendor logic
					for i=1, table.getn(itemsPositionOp) do
						local Currency,CurrentAmount = GetCurrencyInfo(1901)		
						if has_value(itemsBoughtOp,itemsPositionOp[i]) == false then	
							if itemCostOp[i] <= CurrentAmount then
								GMR.RunMacroText("/target Lieutenant Tristia")
								GMR.InteractUnit("target")
								if GetItemCount(getId(itemsPositionOp[i]),true) > 0 then
									table.insert(itemsBoughtOp, itemsPositionOp[i])
								else
									BuyMerchantItem(itemsPositionOp[i])
								end
							end
						end
					end
				end
				if table.getn(itemsPositionMp) > 0 then
					--Main Piece vendor logic
					for i=1, table.getn(itemsPositionMp) do
						local Currency,CurrentAmount = GetCurrencyInfo(1901)		
						if has_value(itemsBoughtMp,itemsPositionMp[i]) == false then	
							if itemCostMp[i] <= CurrentAmount then
								GMR.RunMacroText("/target Knight-Lieutenant Moonstrike")
								GMR.InteractUnit("target")
								if GetItemCount(getId(itemsPositionMp[i]),true) > 0 then
									table.insert(itemsBoughtMp, itemsPositionMp[i])
								else
									BuyMerchantItem(itemsPositionMp[i])
								end
							end
						end
					end
				end
				if table.getn(itemsPositionHp) > 0 then
					--hateful gladiator logic
					for i=1, table.getn(itemsPositionHp) do
						local Currency,CurrentAmount = GetCurrencyInfo(1901)		
						if has_value(itemsBoughtHp,itemsPositionHp[i]) == false then	
							if itemCostHp[i] <= CurrentAmount then
								GMR.RunMacroText("/target Captain Dirgehammer")
								GMR.InteractUnit("target")
								if GetItemCount(getId(itemsPositionHp[i]),true) > 0 then
									table.insert(itemsBoughtHp, itemsPositionHp[i])
								else
									BuyMerchantItem(itemsPositionHp[i])
								end
							end
						end
					end
				end
			else
				--Horde vendor logic
				if table.getn(itemsPositionOp) > 0 then
					--Offpiece vendor logic
					for i=1, table.getn(itemsPositionOp) do
						local Currency,CurrentAmount = GetCurrencyInfo(1901)		
						if has_value(itemsBoughtOp,itemsPositionOp[i]) == false then	
							if itemCostOp[i] <= CurrentAmount then
								GMR.RunMacroText("/target Doris Volanthius")
								GMR.InteractUnit("target")
								if GetItemCount(getId(itemsPositionOp[i]),true) > 0 then
									table.insert(itemsBoughtOp, itemsPositionOp[i])
								else
									BuyMerchantItem(itemsPositionOp[i])
								end
							end
						end
					end
				end
				if table.getn(itemsPositionMp) > 0 then
					--Main Piece vendor logic
					for i=1, table.getn(itemsPositionMp) do
						local Currency,CurrentAmount = GetCurrencyInfo(1901)		
						if has_value(itemsBoughtMp,itemsPositionMp[i]) == false then	
							if itemCostMp[i] <= CurrentAmount then
								GMR.RunMacroText("/target Blood Guard Zar'Shi")
								GMR.InteractUnit("target")
								if GetItemCount(getId(itemsPositionMp[i]),true) > 0 then
									table.insert(itemsBoughtMp, itemsPositionMp[i])
								else
									BuyMerchantItem(itemsPositionMp[i])
								end
							end
						end
					end
				end
				if table.getn(itemsPositionHp) > 0 then
					--hateful gladiator logic
					for i=1, table.getn(itemsPositionHp) do
						local Currency,CurrentAmount = GetCurrencyInfo(1901)		
						if has_value(itemsBoughtHp,itemsPositionHp[i]) == false then	
							if itemCostHp[i] <= CurrentAmount then
								GMR.RunMacroText("/target Sergeant Thunderhorn")
								GMR.InteractUnit("target")
								if GetItemCount(getId(itemsPositionHp[i]),true) > 0 then
									table.insert(itemsBoughtHp, itemsPositionHp[i])
								else
									BuyMerchantItem(itemsPositionHp[i])
								end
							end
						end
					end
				end
			end
		end	
	end
)