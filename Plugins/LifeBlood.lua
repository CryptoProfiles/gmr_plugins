
local glb = GetSpellInfo(55503) -- Life Blood

C_Timer.NewTicker(0.1, function()
        if not IsMounted() and GMR.IsExecuting() then
            if GMR.GetHealth("player") < 50 -- change to the percentage u want/need
             and GMR.IsCastable(glb) then
                GMR.Cast(glb, "player")
            end
        end
end)