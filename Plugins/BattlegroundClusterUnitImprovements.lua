-- BattlegroundClusterUnitImprovements
-- Author: Mons#0885
-- Version: 0.0.1


-- String multiple dices together for a vector of random inputs
bg_actions = {
    [1] = function (x) 
        print("Moving forwards")
        GMR.MoveForwardStart()
        C_Timer.After(2, GMR.MoveForwardStop) end,
    [2] = function (x) 
        print("Turning left")
        GMR.MoveForwardStart()
        GMR.TurnLeftStart()
        C_Timer.After(1, GMR.TurnLeftStop)
        C_Timer.After(2, GMR.MoveForwardStop)
     end,
    [3] = function (x)
        print("Turning right")
        GMR.MoveForwardStart()
        GMR.TurnRightStart()
        C_Timer.After(1, GMR.TurnRightStop)
        C_Timer.After(2, GMR.MoveForwardStop) end,
    [4] = function (x)
        print("Jumping")
        GMR.Jump() end,
}

bg_movementFlag = false
-- start the main loop ( starts when preparation buff is not present )
C_Timer.NewTicker(0.5, function()
    if  GMR.IsInBattleground() then

        local player = GMR.GetPlayerPointer("player")
        local clusterUnit = GMR.Variables.ClusterUnit
        local preparationOnPlayer = AuraUtil.FindAuraByName(GetSpellInfo(44521), "player", "HELPFUL")

        if clusterUnit and player and not preparationOnPlayer then
            GMR.FocusUnit(clusterUnit)
            local clusterUnitName,_ = GMR.UnitName(clusterUnit)
            local cu_currentSpeed,cu_maxRunSpeed = GMR.GetUnitSpeed(clusterUnit)
            local distanceToClusterUnit = GMR.GetDistanceBetweenObjects(player,clusterUnit)
            
            local cu_spellName,_,_,cu_castingTimeStart, cu_castingTimeEnd = GMR.UnitCastingInfo("focus")
            
            -- Dismount if within mounting range of clusterUnit, and clusterUnit is dismounted
            if cu_maxRunSpeed and distanceToClusterUnit then
                if cu_maxRunSpeed < 9.0 and distanceToClusterUnit < GMR.GetGroundMountingRange() then
                    if IsMounted("player") then
                        Dismount()
                    end
                end
                if cu_maxRunSpeed > 9.0 and not IsMounted("player") and distanceToClusterUnit < GMR.GetGroundMountingRange() and not GMR.InCombat(player) then
                    -- TODO: FIX THIS WITH GMR MOUNT
                    UseItemByName('Swift White Steed') -- switch this to the GMR selected mount when you figure out what that is
                
                -- If we are close to the clusterUnit, and the clusterUnit is standing still, we initialize
                -- the random movement block
                -- Its a random dice throw between 4 actions we repeat twice
                -- Its pretty jank but i think its okay
                elseif cu_currentSpeed and cu_currentSpeed < 1.0 and distanceToClusterUnit < 20.0 and not GMR.InCombat(player) then
                    local bg_movementTimer = C_Timer.NewTimer(1.6, function() -- timer can also be random
                        print("bg_plugin: rolling random action")

                        local dice = math.random(1,4)
                        bg_actions[dice]()
                    end,2) 
                end
            end
        else
            GMR.ClearFocus()
        end
end
end)
