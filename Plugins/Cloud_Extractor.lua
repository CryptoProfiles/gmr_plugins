--Author: Lahtinen#5396
--Northrend gas cloud collector
zapthrottle = GetItemInfo(23821)
C_Timer.NewTicker(0.5, function()
    nclouds = {
        ["Arctic Cloud"] = GMR.GetObjectWithInfo({ id = 24879, rawType = 5 }),
        ["Steam Cloud"] = GMR.GetObjectWithInfo({ id = 32544, rawType = 5 }),
        ["Cinder Cloud"] = GMR.GetObjectWithInfo({ id = 32522, rawType = 5 }),								
		 }
     
    for key, ncloud in pairs(nclouds) do
      ax, ay, az = GMR.ObjectPosition(ncloud)
      distplayercloud = GMR.GetDistance(ncloud, "player", ">", 10)
        
        if GMR.IsExecuting() and ncloud and distplayercloud and not GMR.InCombat("player") then 
          GMR.SetDelay("Execute", 2)
          GMR.MeshTo(ax, ay, az)
        end     
        
        if ncloud and not distplayercloud and GMR.IsExecuting() then 
          if IsMounted() then
            GMR.Dismount()
          else
            GMR.Use(zapthrottle)
          end 
        end
    end
end)
