--[[Evagoras' Blacklister v2.0]]--

--[[
Author: Evagoras#0442 (feel free to message me for suggestions, improvements, bug reports or anything else)

Description:
Plugin that adds blacklist areas and unstucks to encrypted profiles.

My testing was limited so any reports are appreciated.

FEATURES:
*Auto-Load Profiles based on character name
*Auto-Load last saved profile if character name not specified
*Univesal Blacklists to add to every profile
*Universal Unstucks to add to every profile
*Specific Blacklists to add to specified profile
*Specific Unstucks to add to specified profile


*Checks if blacklist already exists before adding

Notes:
This will not work as is, you have to edit the names of the profiles and add the blackslists and unstucks for each.
Make sure that the profiles and blacklists match. Meaning that the first entry in blacklists is for the first profile etc.

Future Plans:
Remove blacklists/unstucks
Checks if unstuck already exists before adding

Instructions:
!!!!!!!!! 
r is the radius around a point
!!!!!!!!!
*Auto-Loader: Set autoload to true, then edit autoLoadData accordingly. Required character name, folder of the profile and the profile you want to load
*Universal Blacklist: Add the coordinates and radius of the blacklist you want within quotes in universalBlacklists. Separate by COMMA(,).
        local universalBlacklists = 
        {
            "x1, y1, z1, r1",
            "x2, y2, z2, r2",
        }
*Universal Unstuck: Add the coordinates and radius of the unstucks you want within quotes in universalUnstucks. Separate by COMMA(,). 
        One Unstuck Path should look like this. Make sure to include the radius on all points. Might work without radius after first point but haven't tested.
        local universalUnstucks = 
        {
            --Unstuck Path 1
            {
            "x1, y1, z1, r",
            "x2, y2, z2, r",
            "x3, y3, z3, r",
            "x4, y4, z4, r",
            },
            --Unstuck Path2
            {
            "x5, y5, z5, r5",
            "x6, y6, z6, r6",
            }  
        }
*Specific Profile Blacklists and Unstucks:
        Add Profile Name in profiles
        Add Blacklists for that profile in blacklists
        Add Unstucks for that profile in unstucks

        Make sure they match, so for first profile you want to add the points in the first { } of blacklists and unstucks.
        Example:
        local profiles =
        {
            "Profile 1",
            "Profile 2",
            "Profile 3",
        }
        local blacklists =
        {
            --Profile 1
            {
                "x1, y1, z1, r1",
                "x2, y2, z2, r2",
            },
            --Profile 2
            {
                "x1, y1, z1, r1",
                "x2, y2, z2, r2",
            },
            --Profile 3
            {
                --No blacklists
            },
        }
        local unstucks = {
        --Profile1
        {
            --Unstuck Path 1 for Profile1
            {
                "x1, y1, z1, r",
                "x2, y2, z2, r",
            },
            --Unstuck Path 2 for Profile1
            {
                "x1, y1, z1, r",
                "x2, y2, z2, r",
            }
        },
        --Profile2
        {
            --Unstuck Path 1 for Profile2
            {
                "x1, y1, z1, r",
                "x2, y2, z2, r",
            },
        },
        --Profile3
        {
            --No unstucks
            {
            },
            
        },
        
    }
]]--

local autoload = false
local autoStart = true
local showChatLog = true --Show what is happening in chat.

--Setting for auto loading, requires your character name, the folder of the profile you want to autoload and the profile name (file name, WITHOUT .lua at the end)
--format:
--["character name"]={["profile folder"] = "profile name"},
local autoLoadData = {
        ["char1"]={["folder"] = "profile"},
        ["char2"]={[""] = ""},
        ["char3"]={[""] = ""},
}
--Universal
--Blacklists that will be added to all profiles
local universalBlacklists = 
{
    --"x, y, z, r"
    "",
    
}
--Unstucks that will be added to all profiles
local universalUnstucks = 
{
    --Unstuck Path 1
    --"x, y, z, r"
    {
        "",
        "",
    },
    --Unstuck Path 2
    {

    },
}

--List of profiles you want to add blacklists to
local profiles =
{
    --Profile1
    --"Name of profile" (withouth the .lua)
    "",

    --Profile2
    "",

    --Profile3
    "",
}

--list of blacklists for each profile
local blacklists =
{
    --Profile 1 Blacklists:
    {
        --"x, y, z, radius",
        "",
    },

    --Profile 2 Blacklists:
    {

    },

    --Profile 3 Blacklists:
    {

    },
}

local unstucks = {

    --Profile1 Unstucks
    {
        --Profile1 Unstuck Path 1
        {
            --"x, y, z, r",
            "",
            "",
        },
        --Profile1 Unstuck Path 2
        {
            "",
            "",
        }
    },
    --Profile 2
    {
        --Profile2 Unstuck Path 1
        {
            "",
            "",
        },

        --Profile2 Unstuck Path 2
        {

        },
    },
    --Profile3
    {
        --Profile2 Unstuck Path 1
        {

        },

    },
    
}



local VERSION = 2.0
local ID = "E>B"
local done = false
local function print(msg)
    if showChatLog then
        GMR.Print("|cffa335ee[" .. ID .. "]|r" .. msg)
    end
end
C_Timer.After(2, function()
    if not GMR.Frames.Blacklister then
        GMR.Frames.Blacklister = CreateFrame("frame")
        print("|cFFa335ee[Evagoras]|r |cffff8000Blacklister v2.0 Loaded|r")
        if autoload then
            local nameFound = false
            for char, toLoad in pairs(autoLoadData) do
                if GetUnitName("player") == char then
                    for folder, profile in pairs(toLoad) do
                        nameFound = true
                        GMR.LoadProfile(folder, profile)
                        GMR.UpdateSavedProfileFolder(folder)
                        GMR.UpdateSavedProfile(profile)
                    end
                    break
                end
            end
            --if no character set but autoload is on it will autoload the saved profile
            --See what it is by doing /dump GMR.GetSavedProfile()
            if GMR.GetSavedProfile() and GMR.GetSavedProfileFolder() and not nameFound then
                GMR.LoadProfile(tostring(GMR.GetSavedProfileFolder()),tostring(GMR.GetSavedProfile()))
            end

            print("AutoLoaded Profile: |r "..tostring(GMR.GetProfileName()))
            if autoStart then
                C_Timer.After(2, function()GMR.Execute() end) 
            end
        end

        --This delay is set so that a profile can properly load before adding blacklists (for encrypted profiles)
        GMR.SetDelay("WaitForProfileToLoad", 2)
        GMR.Frames.Blacklister:SetScript("OnUpdate", function()
            if not done and GMR.IsFullyLoaded() and not GMR.GetDelay("WaitForProfileToLoad") then
                AddBlacklists()
            else
                --Check if profile is changed, to add again. (Will add universal unstucks again)
                if GMR.GetProfileName()~= GMR.GetSavedProfile() then
                    GMR.UpdateSavedProfile(GMR.GetProfileName())
                    done = true
                    for pkey, profile in ipairs(profiles) do
                        if GMR.GetProfileName() == profile then
                            done = false
                            GMR.UpdateSavedProfile(GMR.GetProfileName())
                            GMR.UpdateSavedProfileFolder(GMR.Tables.LoadedProfile.ProfileFolder)
                            print("Changed profile, adding new stuff.")
                            break
                        end
                    end
                end
            end
        end)
    end
end)


--function for adding blacklists/unstucks
function AddBlacklists()
    done=true
    local finished = false
    --Add Universal Blacklists
    for _, blacklist in pairs(universalBlacklists) do
        if blacklist~=nil and blacklist~="" then
            finished=true 
            if not BlacklistAlreadyExists(blacklist) then
                GMR.DefineAreaBlacklist(ConvertString(blacklist))
                print("Universal Blacklist added: ["..blacklist.."].")
            else
                print("Blacklist: ["..blacklist.."] already exists, skipping.")
            end
            
        end
    end
    --Add Universal Unstucks
    for pathKey, unstuckPath in pairs(universalUnstucks) do
        if unstuckPath[1]~=nil and unstuckPath[1]~=""then
            GMR.CreateTableEntry("Unstuck")
            local count = #GMR.Tables.UnstuckXYZ + pathKey
            print("Universal Unstuck Path ["..count.."] created:")
            for pointKey, unstuckPoint in pairs(unstuckPath) do
                if unstuckPoint~=nil and unstuckPoint~="" then
                    finished=true
                    GMR.DefineUnstuck(ConvertString(unstuckPoint))
                    print("Added ["..count.."]["..pointKey.."]: ["..unstuckPoint.."]")
                end
            end
        end
    end
    
    --Go through profiles to find if our current profile matches any.
    for pkey, profile in ipairs(profiles) do
        if GMR.GetProfileName() == profile then
            --Go through blacklists from above for our selected profile
            for bkey, blacklist in ipairs(blacklists[pkey]) do
                if blacklist~= nil and blacklist~="" then 
                    --Check if the blacklist already exists
                    --Add blacklist
                    if not BlacklistAlreadyExists(blacklist) then
                        finished=true
                        GMR.DefineAreaBlacklist(ConvertString(blacklist))
                        print("Blacklist added: ["..blacklist.."]")
                    else 
                        print("Blacklist: ["..blacklist.."] already exists, skipping.")
                        finished=true
                    end
                end
            end
            for pathKey, unstuckPath in pairs(unstucks[pkey]) do
                --Creates new unstuck path entry
                if unstuckPath[1]~=nil and unstuckPath[1]~=""then
                    GMR.CreateTableEntry("Unstuck")
                    local count = #GMR.Tables.UnstuckXYZ + pathKey
                    print("Unstuck path ["..count.."] created:")
                    for pointKey, unstuckPoint in pairs(unstuckPath) do
                        if unstuckPoint~=nil and unstuckPoint~="" then
                            --Add unstuck
                            finished = true
                            GMR.DefineUnstuck(ConvertString(unstuckPoint))
                            print("Added ["..count.."]["..pointKey.."]: ["..unstuckPoint.."]")
                        end
                    end
                end

                
            end
            done = true
            if finished then
                print("|cFF00CC00Blacklister Finished")
            end
            break
        end
    end
end

--Used to check if something exists in an array, needs work
function Set(list)
    local set = {}
    for _, l in pairs(list) do set[l]=true end
    return set
end

--Function to modify the blacklist text so that special characters are escaped, useful for writing to file, not used now
function SanitizePattern(pattern)
    local sanitized = string.gsub(pattern, "%.","%%." )
    sanitized = string.gsub(sanitized, "%-","%%-" )
    sanitized = string.gsub(sanitized, "%(","%%(")
    sanitized = string.gsub(sanitized, "%)","%%)")
    return sanitized
end

--Conver string of coordinates to number values and return them
function ConvertString(entry)
    local x, y, z, r = entry:match("([^,]+),([^,]+),([^,]+),([^,]+)")
    return tonumber(x), tonumber(y), tonumber(z), tonumber(r)
end

--Return if a blacklist already exists
function BlacklistAlreadyExists(entry)
    local isEqual = false
    for i=1, #GMR.Tables.ObjectAreaBlacklist, 1 do
        local tempSet = Set(GMR.Tables.ObjectAreaBlacklist[i])
        local x, y, z, r = entry:match("([^,]+),([^,]+),([^,]+),([^,]+)")
        if tempSet[tonumber(x)] and tempSet[tonumber(y)] and tempSet[tonumber(z)] then
            isEqual = true
            return isEqual
        end
    end
    return isEqual
end

--[[ Below left for posterity, could have been done with GMR.DefineAreaBlacklist(x,y,z,r) all along
function UpdateBlacklistTable(blacklist)
    local bx, by, bz, br = blacklist:match("([^,]+),([^,]+),([^,]+),([^,]+)")
    
    local spot = #GMR.Tables.ObjectAreaBlacklist + key
    table.insert(GMR.Tables.ObjectAreaBlacklist, {}) --not sure if necessary
    table.insert(GMR.Tables.ObjectAreaBlacklist[spot], bx)
    table.insert(GMR.Tables.ObjectAreaBlacklist[spot], by)
    table.insert(GMR.Tables.ObjectAreaBlacklist[spot], bz)
    table.insert(GMR.Tables.ObjectAreaBlacklist[spot], br)
    GMR.Print("|cFF2DE58FBlacklist added: "..blacklist)
end
]]

