C_Timer.NewTicker(0.1, function()
  mounted = IsMounted()

  local interruptSpell = GetSpellInfo(78675) -- Druid, Solar Beam

  if not mounted and GMR.InCombat("player", true) and GMR.IsExecuting() then
    if GetSpellCooldown(interruptSpell) == 0 and
      GMR.IsInterruptable("target") and
      GMR.InLoS("target") and
      GMR.IsSpellInRange(interruptSpell, "target") == 1 and
      GMR.UnitCastingTime("target", 0.6) then
        print("Casting Interrupt!")
        GMR.SpellStopCasting()
        GMR.Cast(interruptSpell)
    end
  end
end)
