theArtOfWar = GetSpellInfo(59578)
exorcism = GetSpellInfo(48801)
flashOfLight = GetSpellInfo(48785)

playerHealth = 50  -- if player's health is bellow this value, it will use flash light instead of exorcism

C_Timer.NewTicker(0.1, function()
  if UnitClass("player") == "Paladin" then
    if not mounted and GMR.IsExecuting() and GMR.InLoS("target") and UnitExists("target") then
      if AuraUtil.FindAuraByName(theArtOfWar, "player", "HELPFUL") then
        if GetSpellInfo(flashOfLight) and IsUsableSpell(flashOfLight) and GMR.GetHealth("player") <= playerHealth then
          GMR.Cast(flashOfLight)
        elseif GetSpellInfo(exorcism) and GetSpellCooldown(exorcism) == 0 and
        IsUsableSpell(exorcism) and IsSpellInRange(exorcism, "target") then
          GMR.Cast(exorcism)
        end
      end
    end
  end
end)