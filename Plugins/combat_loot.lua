C_Timer.NewTicker(0.5, function()
	if GMR.IsExecuting() and GMR.InCombat("player") then 
		for i = 1, #GMR.Tables.Lootables do
			local lootable = GMR.Tables.Lootables[i][1]
			if GMR.ObjectExists(lootable) and GMR.GetDistance("player", lootable, "<", 5) and GMR.IsCasting("player") == nill 
			and GMR.IsObjectLootable(lootable) and GMR.GetInventorySpace() > GMR.GetMinimumInventorySpace() then
				GMR.InteractObject(lootable)
				GMR.SetDelay("Execute", 0.5)
			end
		end
	end
end)