local configurations = {}

--for each one of your characters just add configurations with the syntax below, being sure to use an incremental index for every new line  of configuration
---- CONFIGURATION EXAMPLE -----
--configurations[0] = { characterName = "ULTRACHAD", profileFolder = "01. Custom", profileName = "35-37 Stranglethorn Vale" }

configurations[0] = { characterName = "character0", profileFolder = "thefolder", profileName = "profilename" }

configurations[1] = { characterName = "character1", profileFolder = "thefolder", profileName = "profilename" }

configurations[2] = { characterName = "character2", profileFolder = "thefolder", profileName = "profilename" }

configurations[3] = { characterName = "character3", profileFolder = "thefolder", profileName = "profilename" }

local playerName = GMR.UnitName("player")

do
   for  i=0,table.getn(configurations) do
      if (configurations[i].characterName == playerName) then
         GMR.Print ("Loading profile " .. configurations[i].profileName .. " for character " .. configurations[i].characterName)
         GMR.LoadProfile(configurations[i].profileFolder, configurations[i].profileName)
         break
      end
      
   end
end