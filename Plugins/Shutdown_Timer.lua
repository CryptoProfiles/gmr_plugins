local shutdown_after_x_minutes = math.random(180,300)
local shutdown_timer = GetTime()+60*shutdown_after_x_minutes
C_Timer.NewTicker(1, function()
  if shutdown_timer < GetTime() then 
    GMR.Shutdown()
  end 
end)