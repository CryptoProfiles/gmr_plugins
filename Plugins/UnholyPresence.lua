local spells = {
    unholyPresence = GetSpellInfo(48265)
}

C_Timer.NewTicker(1, function()
	local unholyPresenceActive = AuraUtil.FindAuraByName(spells.unholyPresence, "player", "HELPFUL")

	if IsMounted("player") and not unholyPresenceActive then
		GMR.Cast(spells.unholyPresence, "player")
	end
end)