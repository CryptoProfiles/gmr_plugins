--Author: Lahtinen#5396
--Edited for all WOTLK schools: Evagoras#0442
--Edited for all schools in WoW: Caver#5714
--Edited for Retail DF

C_Timer.NewTicker(1, function()
    school = {
        
        ["Aileron Seamoth School"] = GMR.GetObjectWithInfo({ id = 381098, rawType = 8 }),
        ["Cerulean Spinefish School"] = GMR.GetObjectWithInfo({ id = 381099, rawType = 8 }),
        ["Temporal Dragonhead Pool"] = GMR.GetObjectWithInfo({ id = 381100, rawType = 8 }),
        ["Thousandbite Piranha Swarm"] = GMR.GetObjectWithInfo({ id = 381097, rawType = 8 }),
       	["Islefin Dorado Pool"] = GMR.GetObjectWithInfo({ id = 381101, rawType = 8 }),
       		
		 }
    bobber = GMR.GetObjectWithInfo({ id = 35591, rawType = 8 })
    fishingspell = GetSpellInfo(51294)
    for key, school in pairs(school) do
        distbobberschool = GMR.GetDistance(school, bobber, "<", 3)
        distplayerschool = GMR.GetDistance(school, "player", "<", 21)
        if not IsMounted() and school and not distbobberschool and distplayerschool and not GMR.InCombat("player") then
            GMR.SetDelay("Execute", 4)
            GMR.FaceDirection(school)
            GMR.SpellStopCasting()
            GMR.CastSpellByName(fishingspell)
            GMR.Print("Aiming for "..key.." school!")          
        end              
        if GMR.UnitChannelInfo("player") and school and distplayerschool and not GMR.InCombat("player") then
            GMR.SetDelay("Execute", 3)
            bobberstatus = GMR.ObjectAnimationFlag(bobber)
                if bobberstatus==1 then
                    GMR.ObjectInteract(bobber)
                end
        end
    end
     
end)