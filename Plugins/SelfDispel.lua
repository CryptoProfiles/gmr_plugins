-- Plugin for self dispel
playerClass = "PALADIN"  -- Change to your player class

-- Your first dispel spell
dispel_one = GetSpellInfo(4987)  -- Current SpellID is for Purify, change to your SpellID
dispel_one_debuff_types = {"Magic", "Disease", "Poison"}  -- The types of debuffs that dispel_one can remove

-- Your second dispel spell (optional)
dispel_two = GetSpellInfo(1152)  -- Current SpellID is for Purify, change to your SpellID 
dispel_two_debuff_types = {"Disease", "Poison"}  -- The types of debuffs that dispel_two can remove


function inTable(tbl, item)
    for key, value in pairs(tbl) do
        if value == item then return key end
    end
    return false
end

function GetdispelableDebuffOnPlayer(debuff_types)
	for i = 1, 40 do
		local debuff_name, _, _, debuff_type = UnitAura("player", i, "HARMFUL")
		if debuff_type and inTable(debuff_types, debuff_type) then return debuff_name, debuff_type end
	end
	return nil, nil
end

if GMR.GetClass("player") == playerClass then 
	C_Timer.NewTicker(0.1, function()
		if GMR.IsExecuting() then
			-- first dispel spell
			if GetSpellInfo(dispel_one) then
				local debuff_name, debuff_type = GetdispelableDebuffOnPlayer(dispel_one_debuff_types)
				if debuff_type and GMR.IsCastable(dispel_one) then
					GMR.Cast(dispel_one)
					print(dispel_one, 'removed', '"' .. debuff_name  .. '"', "(" .. debuff_type .. ")")  -- Comment out or delete this line if you don't want to see a print message in the game chat
				end
			end
			-- second dispel spell
			if GetSpellInfo(dispel_two) then
				local debuff_name, debuff_type = GetdispelableDebuffOnPlayer(dispel_two_debuff_types)
				if debuff_type and GMR.IsCastable(dispel_two) then
					GMR.Cast(dispel_two)
					print(dispel_two, 'removed', '"' .. debuff_name  .. '"', "(" .. debuff_type .. ")")  -- Comment out or delete this line if you don't want to see a print message in the game chat
				end
			end
		end
	end)
end
