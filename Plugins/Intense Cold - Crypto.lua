C_Timer.NewTicker(0.5, function()
    if not GMR.IsExecuting() or GMR.UnitIsDeadOrGhost("player") then 
        return 
    end 
    local theBUFF = GetSpellInfo(48094)
    local enemy = GMR.GetObjectWithInfo({ id = 123456789, canAttack = true, isAlive = true })
    local x, y, z = GMR.ObjectPosition(enemy)
    
    if GMR.HasDebuff("player", theBUFF) then
        if not GMR.GetDelay("CustomQuest") then
            if GMR.IsPlayerPosition(3389.8390, 5318.8770, 39.0415, 80) then
                if GMR.HasBuffStacks("player", "Intense Cold", 2) then
                    GMR.Jump() --; GMR.SetDelay("CustomQuest", 1) Setting a Delay may not be wise, u need to play with it.
                end
            end
        end
    end
end)