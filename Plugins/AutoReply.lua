local playerName = UnitName("player")
local autoReplyMessages = {
    "nty",
    "I'm not interested, thanks!",
    "Im good thanks thhough",
    "not atm",
    "No thanks, maybe some other time!"
}
local isAutoReplyEnabled = true

local frame = CreateFrame("Frame")
frame:RegisterEvent("CHAT_MSG_WHISPER")
frame:RegisterEvent("CHAT_MSG_WHISPER_INFORM")

frame:SetScript("OnEvent", function(self, event, message, sender, _, _, _, _, _, _, _, _, guid)
    if event == "CHAT_MSG_WHISPER" then
        if isAutoReplyEnabled and sender ~= playerName then
            C_Timer.After(5, function() 
                local randomIndex = math.random(1, #autoReplyMessages)
                SendChatMessage(autoReplyMessages[randomIndex], "WHISPER", nil, sender) 
            end)
        end
    elseif event == "CHAT_MSG_WHISPER_INFORM" then
        if message == autoReplyMessage and sender ~= playerName then
            isAutoReplyEnabled = false
        end
    end
end)

SLASH_GMRWOW_AUTO_REPLY_ENABLE1 = "/autoreplyenable"
SlashCmdList["GMRWOW_AUTO_REPLY_ENABLE"] = function()
    isAutoReplyEnabled = true
    print("GMRWOW auto-reply enabled")
end

SLASH_GMRWOW_AUTO_REPLY_DISABLE1 = "/autoreplydisable"
SlashCmdList["GMRWOW_AUTO_REPLY_DISABLE"] = function()
    isAutoReplyEnabled = false
    print("GMRWOW auto-reply disabled")
end

SLASH_GMRWOW_AUTO_REPLY_MESSAGE1 = "/autoreplymessage"
SlashCmdList["GMRWOW_AUTO_REPLY_MESSAGE"] = function(msg)
    autoReplyMessage = msg
    print("GMRWOW auto-reply message set to: " .. autoReplyMessage)
end
