local CFG = {
	maxDistance = 30,
	maxPower = 90,
}
C_Timer.NewTicker(1, function()
	if select(2, UnitClass("player")) ~= "DEATHKNIGHT" then
		return
	end

	if not GMR.IsExecuting() or not UnitExists("target") or not GMR.InLoS("target") then
		return
	end

	if GMR.GetDistance("player", "target", ">", CFG.maxDistance) then
		return
	end

	local power = UnitPower("player", 6)
	if power > CFG.maxPower then
		return
	end

	local hornOfWinterSpell = GetSpellInfo(57330)
	if not IsUsableSpell(hornOfWinterSpell) then
		return
	end

	if select(2, GetSpellCooldown(hornOfWinterSpell)) ~= 0 then
		return
	end

	GMR.Cast(hornOfWinterSpell)
end)