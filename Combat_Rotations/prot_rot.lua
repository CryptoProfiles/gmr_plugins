C_Timer.NewTicker(0.1, function()
local class = GMR.GetClass("player")
	if class == "PALADIN" then 

  --spell IDs
  local SealLight = GetSpellInfo(20165)
  local SealCommand = GetSpellInfo(20375)
  local HolyShield = GetSpellInfo(48952)
  local JudgementWisdom = GetSpellInfo(53408)
  local JudgementLight = GetSpellInfo(20271)
  local HammerWrath = GetSpellInfo(24275)
  local Consecration = GetSpellInfo(26573)
  local HolyWrath = GetSpellInfo(2812)
  local Freedom = GetSpellInfo(1044)
  local HammerJustice = GetSpellInfo(853)
  local ArcaneTorrent = GetSpellInfo(28730)
  local HolyLight = GetSpellInfo(635)
  local Exorcism = GetSpellInfo(879)
  local DivinePlea = GetSpellInfo(54428)
  local HoReck = GetSpellInfo(62124)
  local AvS = GetSpellInfo(31935)
  local HoRight = GetSpellInfo(53595)
  local ShoRight = GetSpellInfo(53600)
  local AW = GetSpellInfo(31884)
  local DP = GetSpellInfo(498)
   
  if GMR.IsExecuting() and not GMR.IsDrinking("player") and not GMR.HasBuff("player", 32245) then
	
	if GMR.IsCastable(DP) and GMR.GetHealth("player") <= 50 and GMR.InCombat("player") then
		GMR.CastSpellByName(DP)
	end
	
    if GMR.IsCastable(Freedom) and GetUnitSpeed("player") > 0 and GetUnitSpeed("player") < 7 then
      GMR.CastSpellByName(Freedom)
	end
	
	--sasting Interrupt HoJ
	if GMR.IsCastable(HammerJustice) and GMR.IsInterruptable("target") and GMR.UnitCastingTime("target", 2.5) then
	  GMR.CastSpellByName(HammerJustice)
	end
	
	--casting Interrupt Arcane Torrent
	if GMR.IsCastable(ArcaneTorrent, "target") and GMR.InLoS("target") and UnitExists("target") and GMR.IsInterruptable("target") and GMR.UnitCastingTime("target", 0.5)
	and GMR.GetDistance("player", "target", "<", 6.5) then
	  GMR.CastSpellByName(ArcaneTorrent)
	end
	
	--heals out of combat
	if not GMR.InCombat("player") and GMR.GetMana("player") > 30 and GMR.GetHealth("player") < 60 and GMR.IsCastable(HolyLight) then
	  GMR.RunMacroText("/cancelaura "..DivinePlea)
	  GMR.CastSpellByName(HolyLight)
	end
	
	-- seal swap
	if GMR.GetMana("player") > 30 and GMR.GetHealth("player") > 70 and GMR.IsCastable(SealCommand) and not GMR.HasBuff("player", SealCommand) then 
		GMR.CastSpellByName(SealCommand)
	end
	
	if GMR.GetMana("player") > 20 and GMR.GetHealth("player") < 50 and GMR.IsCastable(SealLight) and not GMR.HasBuff("player", SealLight) then 
		GMR.CastSpellByName(SealLight)
	end
	
	--pulls w either Hand of Reckoning or Avenger Shield
	if GMR.GetDistance("player", "target", ">", 11) and not GMR.InCombat("target") and not GMR.UnitIsPlayer("target") then 
		if GMR.IsCastable(HoReck, "target") then
		GMR.CastSpellByName(HoReck)
		elseif GMR.IsCastable(AvS, "target")  then
		GMR.CastSpellByName(AvS)
		end
	end
  
	if not GMR.UnitIsPlayer("target") and GMR.UnitIsAttackable("target") then
    mobcount = GMR.GetNumAttackingEnemies("player", 10)

		if mobcount > 1 then
		--AOE Rotation
	
		if GMR.IsCastable(AW) and mobcount > 5 and GMR.InCombat("player") and GMR.GetHealth("player") > 70 then
			GMR.CastSpellByName(AW)
		end
	
		--Hammer of Righteous
		if GMR.IsCastable(HoRight) then
		GMR.CastSpellByName(HoRight)
		end
	
		--Consecration
		if GMR.IsCastable(Consecration) and GMR.GetDistance("player", "target", "<", 6.5) and GetUnitSpeed("player") == 0 then
			GMR.CastSpellByName(Consecration)
		end

		--Shield of Righteous
		if GMR.IsCastable(ShoRight) then
			GMR.CastSpellByName(ShoRight)
		end
	
		--Judgement
		if GMR.IsCastable(JudgementLight) then
			GMR.CastSpellByName(JudgementLight)
		end
	
		--Holy Shield
		if GMR.IsCastable(HolyShield) then
			GMR.Cast(HolyShield)
		end
	
		--Holy Wrath
		if GMR.IsCastable(HolyWrath, "target") and mobcount > 2 and (UnitCreatureType("target") == "Demon" or UnitCreatureType("target") == "Undead") then
			GMR.CastSpellByName(HolyWrath)
		end

	else
		--Single Target Rotation
	
		--Hammer of Righteous
		if GMR.IsCastable(HoRight) then
			GMR.CastSpellByName(HoRight)
		end
	
		--Shield of Righteous
		if GMR.IsCastable(ShoRight) then
			GMR.CastSpellByName(ShoRight)
		end
	
		--Judgement
		if GMR.IsCastable(JudgementLight) then
			GMR.CastSpellByName(JudgementLight)
		end
		--Hammer of Wrath
		if GMR.IsCastable(HammerWrath) then
			GMR.CastSpellByName(HammerWrath)
		end
	
		--Consecration
		if GMR.IsCastable(Consecration) and GMR.GetDistance("player", "target", "<", 6.5) and GetUnitSpeed("player") == 0 then
			GMR.CastSpellByName(Consecration)
		end

		--Holy Wrath
		if GMR.IsCastable(HolyWrath) and (UnitCreatureType("target") == "Demon" or UnitCreatureType("target") == "Undead") then
			GMR.CastSpellByName(HolyWrath)
		end
	
		--Exorcism
		if GMR.IsCastable(Exorcism) and (UnitCreatureType("target") == "Demon" or UnitCreatureType("target") == "Undead") then
			GMR.CastSpellByName(Exorcism)
		end
		end
  end
  end
end
end)
