function GMR.CustomCombatConditions()
	local class = GMR.GetClass("player")
	if class == "PRIEST" then
	local Vampiric_Touch = GetSpellInfo(34917)
	local Shadow_Word_Pain = GetSpellInfo(25368)
	local Devoring_Plague = GetSpellInfo(25467)
		for i = 1, #GMR.Tables.Attackables do
		local attackable = GMR.Tables.Attackables[i][1]
			if GMR.ObjectExists(attackable)
			and GMR.IsCastable(Vampiric_Touch)
			and GMR.GetDistance("player", attackable, "<", 33)
			and not AuraUtil.FindAuraByName(Vampiric_Touch, attackable, "HARMFUL")
			and UnitAffectingCombat(attackable) then
				GMR.Cast(Vampiric_Touch, attackable)
			end
		end
		for i = 1, #GMR.Tables.Attackables do
		local attackable = GMR.Tables.Attackables[i][1]
			if GMR.ObjectExists(attackable)
			and GMR.IsCastable(Shadow_Word_Pain)
			and GMR.GetDistance("player", attackable, "<", 33)
			and not AuraUtil.FindAuraByName(Shadow_Word_Pain, attackable, "HARMFUL")
			and UnitAffectingCombat(attackable) then
				GMR.Cast(Shadow_Word_Pain, attackable)
			end
		end
	    for i = 1, #GMR.Tables.Attackables do
	    local attackable = GMR.Tables.Attackables[i][1]
		    if GMR.ObjectExists(attackable)
		    and GMR.IsCastable(Devoring_Plague)
		    and GMR.GetDistance("player", attackable, "<", 33)
			and not AuraUtil.FindAuraByName(Devoring_Plague, attackable, "HARMFUL") then
				GMR.Cast(Devoring_Plague, attackable)
		    end
		end
	end
end
