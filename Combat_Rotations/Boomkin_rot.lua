C_Timer.NewTicker(0.1, function() 
local class = GMR.GetClass("player")
if class == "DRUID" and GMR.IsCastable(24858) then 
	local barkskin = GetSpellInfo(22812)
	local HT = GetSpellInfo(5185)
	local RJ = GetSpellInfo(774)
	local regrowth = GetSpellInfo(8936)
	local mark = GetSpellInfo(1126)
	local thorns = GetSpellInfo(467)
	local FF = GetSpellInfo(16857)
	local innervate = GetSpellInfo(29166)
	local prowl2 = GetSpellInfo(5215)
	local catForm2 = GetSpellInfo(768)
	local wrath = GetSpellInfo(48461)
	local starfall = GetSpellInfo(53201)
	local moonkin = GetSpellInfo(24858)
	local OF = GetSpellInfo(48393)
	local swarm = GetSpellInfo(48468)
	local hurricane = GetSpellInfo(48467)
	local treants = GetSpellInfo(33831)
	local moonfire = GetSpellInfo(48463)
	local nourish = GetSpellInfo(50464)
	
	if not mounted and not GMR.IsDrinking("player") and GMR.IsExecuting() and not GMR.HasBuff("player", prowl2) then
		
		--buffs out of combat if character has more then 95% health
		if GMR.GetHealth("player") > 95 and not GMR.InCombat("player") then
			--mark of the wild
			if GMR.IsCastable(mark) and not GMR.HasBuff("player", mark) then
				GMR.CastSpellByName(mark)
			end
		
			--thorns
			if GMR.IsCastable(thorns) and not GMR.HasBuff("player", thorns) then
				GMR.CastSpellByName(thorns)
			end
		end
		
		--innervate if below 40% mana
		if GMR.IsCastable(innervate) and GMR.GetMana("player") <= 40 then
				GMR.CastSpellByName(innervate)
		end
		
		--heals out of combat
		if GMR.GetHealth("player") < 95 and not GMR.InCombat("player") then
			
			--regrowth below 50%
			if GMR.IsCastable(regrowth) and GMR.GetHealth("player") < 50 and not GMR.HasBuff("player", regrowth) then
				GMR.CastSpellByName(regrowth)
			end
			
			--nourish (if known - lvl 80 spell) below 70% with either rejuvenation or regrowth buff present
			if GMR.IsCastable(nourish) and GMR.GetHealth("player") < 70 and (GMR.HasBuff("player", RJ) or GMR.HasBuff("player", regrowth)) then
				GMR.CastSpellByName(nourish)
			end
			
			--if nourish isnt known - healing touch below 60% with either rejuvenation or regrowth buff present
			if GMR.IsCastable(HT) and GMR.GetHealth("player") < 60 and not GMR.IsSpellKnown(nourish) 
			and (GMR.HasBuff("player", RJ) or GMR.HasBuff("player", regrowth)) then 
				GMR.CastSpellByName(HT)
			end
			
			--rejuvenation below 90%
			if GMR.IsCastable(RJ) and GMR.GetHealth("player") < 90 and not GMR.HasBuff("player", RJ) then
				GMR.CastSpellByName(RJ)
			end
		end
		
		--heals in combat if character have barkskin/owlkin frenzy
		if GMR.GetHealth("player") < 50 and GMR.InCombat("player") and mobcount < 4 and (GMR.HasBuff("player", OF) or GMR.HasBuff("player", barkskin))then 
			
			--regrowth below 50%
			if GMR.IsCastable(regrowth) and GMR.GetHealth("player") < 50 and not GMR.HasBuff("player", regrowth) then
				GMR.CastSpellByName(regrowth)
			end
			
			--rejuvenation below 50%
			if GMR.IsCastable(RJ) and GMR.GetHealth("player") < 50 and not GMR.HasBuff("player", RJ) then
				GMR.CastSpellByName(RJ)
			end
			
		end
		
		--boomkin form in combat if character has 50+% health
		if GMR.IsCastable(moonkin) and not GMR.HasBuff("player", moonkin) and GMR.InCombat("player") and (GMR.GetHealth("player") >= 50 or ((GMR.HasBuff("player", regrowth) or GMR.HasBuff("player", rejuvenation)))) then
			GMR.CastSpellByName(moonkin)
		end
		
		--barkskin in combat if attacked by 3+ enemies and Owlkin frenzy hasnt procced
		if GMR.InCombat("player") and GMR.GetNumAttackingEnemies("player", 6) > 2 and (not GMR.HasBuff("player", OF) or GMR.GetHealth("player") < 50) and GMR.IsCastable(barkskin) then
			GMR.CastSpellByName(barkskin)		
		end
		
		--treants in combat
		if GMR.IsCastable(treants) and GMR.UnitIsAttackable("target") and GMR.InCombat("player") then
				GMR.CastSpellByName(treants, "player")
		end
		
		--defining number of attacning enemies
		mobcount = GMR.GetNumAttackingEnemies("player", 10)
		if mobcount > 2 then
				
				--starfall
				if GMR.IsCastable(starfall) and GMR.UnitIsAttackable("target") then
					GMR.CastSpellByName(starfall)
					GMR.RunMacroText("/petdefensive")
				end
				
				--hurricane if character has barkskin/owlkin frenzy buff
				if GMR.IsCastable(hurricane) and GMR.UnitIsAttackable("target") and (GMR.HasBuff("player", barkskin) or GMR.HasBuff("player", OF))
				and not GMR.HasDebuff("player", 60846) then
					GMR.CastSpellByName(hurricane, "player")
					GMR.PetAttack()
					GMR.RunMacroText("/petdefensive")
				end
				
			else
				--starfall (if 2 enemies within 36 yards)
				if GMR.IsCastable(starfall) and mobcount == 2 and GMR.UnitIsAttackable("target") and GMR.GetDistance("player", "target", "<", 36) then
					GMR.CastSpellByName(starfall)
				end
				
				--moonfire if target's health below 10%
				if GMR.IsCastable(moonfire) and GMR.UnitIsAttackable("target") and GMR.GetHealth("target") < 10 and GMR.GetMana("player") > 40 then
					GMR.CastSpellByName(moonfire)
				end
				
				--swarm to pull
				if GMR.IsCastable(swarm) and GMR.UnitIsAttackable("target") and not GMR.HasDebuff("target", swarm) then
					GMR.CastSpellByName(swarm)
					GMR.PetAttack()
					GMR.RunMacroText("/petdefensive")
				end
				
				--wrath to spam
				if GMR.IsCastable(wrath) and GMR.UnitIsAttackable("target") then
					GMR.CastSpellByName(wrath)
					GMR.PetAttack()
					GMR.RunMacroText("/petdefensive")
				end
			end
	end
	end
end)