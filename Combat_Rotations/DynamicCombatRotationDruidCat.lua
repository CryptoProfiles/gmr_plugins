  -- This combat logic tries to follow the rotation from the Icy-Veins leveling guide for Feral druid (TBC), make sure that you take the same talents in there for the best results.
  -- It will open with Prowl and Pounce if the spells have been learned, otherwise it will range pull with Faerie Fire (Feral) if the talent is taken. 
  -- Then it will use Mangle if the talent has been taken, otherwise it will use Claw if that spell has been learned.
  -- It will dynamically determine if it should cast Rip or use Ferocious Bite as a finishing move depending on the target's current health.

  -- For this plugin to work as intended you MUST have the following spells unchecked in GMR > Spells section: Faerie Fire, Mangle, Claw, Rip, Rake, Ferocious Bite, Prowl, Pounce
  -- You must have the following spells checked in GMR > Spells section: Cat Form. You can have other spells/buffs checked.
  -- You should have at least the following spells learned: Cat Form, Claw, Rip, Ferocious Bite

  -- For fine tuning modifications can be made on line 15, 16, 18 and 19

C_Timer.NewTicker(0.1, function()
  if UnitClass("player") == "Druid" then

    ripModifier = 4  -- A higher value means it will cast Rip earlier, if you assign negative value it will cast Rip later.
    ferociousBiteModifer = 0.3 -- A higher value means it will cast Ferocious Bite earlier, if you assign negative value it will cast Ferocious Bite later.
    
    minimumHealthTargetRip = 50  -- Rip won't be cast bellow this value
    prowlDistance = 13  -- (yards) from how far away GMR will cast prowl

    minimumHealthTargetTigersFury = 15  -- Bellow this value of health target, Tiger's Fury won't be cast
    tigersFuryEnergyThreshold = 50  -- Bellow this value of player energy, Tiger's Fury won't be cast

    minimumHealthTargetBerserk = 50  -- Bellow this value of health target, Berser won't be cast


    faerieFireFeral = GetSpellInfo(16857)
    prowl = GetSpellInfo(5215)
    pounce = GetSpellInfo(27006)
    rip = GetSpellInfo(27008)
    ferociousBite = GetSpellInfo(24248)
    berserk = GetSpellInfo(50334)
    tigersFury = GetSpellInfo(50213)
    if GetSpellInfo(GetSpellInfo(33983)) then
      mangleOrClaw = GetSpellInfo(33983)
    else
      mangleOrClaw = GetSpellInfo(27049)  -- Claw
    end

    if not mounted and GMR.IsExecuting() and GMR.InLoS("target") and UnitExists("target") then
      if AuraUtil.FindAuraByName("Cat Form", "player", "HELPFUL") then
        if UnitAffectingCombat("player") then
          -- Tiger's Fury
          if GetSpellInfo(tigersFury) and GetSpellCooldown(tigersFury) == 0 and 
          IsUsableSpell(tigersFury) and UnitPower("player") <= tigersFuryEnergyThreshold and
          GMR.GetHealth("target") >= minimumHealthTargetTigersFury and not 
          AuraUtil.FindAuraByName(tigersFury, "player", "HELPFUL") and not
          AuraUtil.FindAuraByName(berserk, "player", "HELPFUL") then
            GMR.Cast(tigersFury)
          end
          -- Berserk (Feral)
          if GetSpellInfo(berserk) and GetSpellCooldown(berserk) == 0 and 
          IsUsableSpell(berserk) and GMR.GetHealth("target") >= minimumHealthTargetBerserk and not 
          AuraUtil.FindAuraByName(berserk, "player", "HELPFUL") and not
          AuraUtil.FindAuraByName(tigersFury, "player", "HELPFUL") then
            GMR.Cast(berserk)
          end
          -- Faerie Fire (Feral)
          if GetSpellInfo(faerieFireFeral) and GetSpellCooldown(faerieFireFeral) == 0 and not
          AuraUtil.FindAuraByName(faerieFireFeral, "target", "HARMFUL") and 
          GMR.IsSpellInRange(faerieFireFeral, "target") == 1 and GMR.GetHealth("target") > 0 then
            GMR.Cast(faerieFireFeral)
            -- print("Faerie Fire (Feral)! (not stealthed)")
          end
          -- Mangle
          if GetSpellInfo(GetSpellInfo(33983)) and IsUsableSpell(mangleOrClaw) and not 
          AuraUtil.FindAuraByName(mangleOrClaw, "target", "HARMFUL") then
            GMR.Cast(mangleOrClaw)
            -- print("Mangle! because debuff is down)")
          end
          -- Rip
          if GetSpellInfo(rip) and IsUsableSpell(rip) and not 
          AuraUtil.FindAuraByName(rip, "target", "HARMFUL") and
          GetComboPoints("player", "target") >= GMR.GetHealth("target") / 10 - ripModifier and 
          GMR.GetHealth("target") >= minimumHealthTargetRip then
            GMR.Cast(rip)
            -- print("Rip!")
          -- Ferocious Bite
          elseif GetSpellInfo(ferociousBite) and IsUsableSpell(ferociousBite) and 
          GetComboPoints("player", "target") >= GMR.GetHealth("target") / 10 - ferociousBiteModifer then
            GMR.Cast(ferociousBite)
            -- print("Ferocious Bite!")
          end
          -- Mangle or Claw (filler)
          GMR.Cast(mangleOrClaw)
        else
        -- Out of combat
           -- Prowl
          if GetSpellInfo(prowl) and IsUsableSpell(prowl) and 
            GetSpellCooldown(prowl) == 0 and GetSpellInfo(pounce) then
            if not AuraUtil.FindAuraByName(prowl, "player", "HELPFUL") and 
            GMR.GetDistance("player", "target", "<", prowlDistance) and 
            GMR.GetHealth("target") > 0 then      
              GMR.Cast(prowl)
              -- print("Prowl")
            end
          -- Faerie Fire (Feral) ranged
          elseif GetSpellInfo(faerieFireFeral) and 
          GetSpellCooldown(faerieFireFeral) == 0 and not
          AuraUtil.FindAuraByName(prowl, "player", "HELPFUL") and not 
          AuraUtil.FindAuraByName(faerieFireFeral, "target", "HARMFUL") and
          GMR.IsSpellInRange(faerieFireFeral, "target") == 1 and GMR.GetHealth("target") > 0 then
            GMR.Cast(faerieFireFeral)
            -- print("Faerie Fire (Feral)! (From range, because no stleath used!")
          end
          -- Pounce
          if GetSpellInfo(pounce) and IsUsableSpell(pounce) and
          AuraUtil.FindAuraByName(prowl, "player", "HELPFUL") and 
          GMR.IsSpellInRange(pounce, "target") == 1 then
            GMR.Cast(pounce)
            -- print("Pounce!")
          end
        end
      end
    end
  end
end)