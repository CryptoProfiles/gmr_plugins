if UnitClass("player") == "Hunter" then
  ChimeraShot = GetSpellInfo(53209)
  SerpentSting= GetSpellInfo(27016)
  ViperSting = GetSpellInfo(65881)
  AimedShot = GetSpellInfo(27065)
  ArcaneShot = GetSpellInfo(27019)
  SteadyShot = GetSpellInfo(49051)
  Disengage = GetSpellInfo(781)
  KillShot = GetSpellInfo(53351)
  SilencingShot = GetSpellInfo(34490)
  Explosive = GetSpellInfo(49066)
  Misdirection = GetSpellInfo(34477)
  Multishot = GetSpellInfo(27021)
  ManaThresh = 55

  ExplosiveThresh = 2

  C_Timer.NewTicker(0.1, function()
  if not mounted and GMR.IsExecuting() and GMR.InLoS("target") and UnitExists("target") and GMR.GetHealth("target") > 0 then
    
    --Disengage
    if GetSpellInfo(SilencingShot)
      and IsUsableSpell(SilencingShot)
      and GMR.UnitCastingTime("target", 0.1)
      and GMR.IsInterruptable("target")
      and GMR.IsCastable(SilencingShot, "target") then
        GMR.Cast(SilencingShot, "target")
    
      elseif GetSpellInfo(Misdirection)
      and GMR.InCombat("player") 
      and IsUsableSpell(Misdirection)
      and GMR.IsCastable(Misdirection, "pet") then
        GMR.Cast(Misdirection, "pet")
      
    
    elseif GetSpellInfo(Disengage)
      and IsUsableSpell(Disengage)
      and GMR.IsCastable(Disengage)
      and GMR.InCombat("player") 
      and GMR.GetDistance("player", "target", "<", 10) then
      GMR.Cast(Disengage)
  
      elseif
      GetSpellInfo(ViperSting) 
      and IsUsableSpell(ViperSting)
      and GMR.GetMana("player") < ManaThresh
      and GMR.IsCastable(ViperSting, "target") 
      and GMR.GetDistance("player", "target", "<", 35) 
      and not GMR.HasDebuff("target", ViperSting) then
        GMR.Cast(ViperSting)


    --SerpentSting
    elseif
      GetSpellInfo(SerpentSting) 
      and IsUsableSpell(SerpentSting) 
      and GMR.GetMana("player") > ManaThresh
      and GMR.IsCastable(SerpentSting, "target") 
      and GMR.GetDistance("player", "target", "<", 35) 
      and not GMR.HasDebuff("target", SerpentSting) then
      GMR.Cast(SerpentSting)
    

    --KillShot
    elseif GetSpellInfo(KillShot) 
      and IsUsableSpell(KillShot) 
      and GMR.IsCastable(KillShot, "target") 
      and GMR.GetDistance("player", "target", "<", 35) then
      GMR.Cast(KillShot)
    

--AimedShot
    elseif GetSpellInfo(AimedShot) 
      and IsUsableSpell(AimedShot) 
      and GMR.IsCastable(AimedShot, "target") 
      and GMR.GetDistance("player", "target", "<", 35) then
      GMR.Cast(AimedShot)
    

    --ChimeraShot
    elseif GetSpellInfo(ChimeraShot) 
      and IsUsableSpell(ChimeraShot) 
      and GMR.IsCastable(ChimeraShot, "target") 
      and GMR.GetDistance("player", "target", "<", 35) then
      GMR.Cast(ChimeraShot)
    

    --ArcaneShot
    elseif GetSpellInfo(ArcaneShot) 
      and IsUsableSpell(ArcaneShot) and GMR.IsCastable(ArcaneShot, "target") 
      and GMR.GetDistance("player", "target", "<", 35)  then
      GMR.Cast(ArcaneShot)
    

    elseif GetSpellInfo(Explosive)
    and GMR.GetNumAttackingEnemies("target", 6.5) >= ExplosiveThresh
    and GMR.GetDistance("player", "target", "<", 10)
    and GMR.IsCastable(Explosive) then
      GMR.Cast(Explosive)

    elseif GetSpellInfo(Multishot)
    and GMR.GetNumAttackingEnemies("target", 35) > ExplosiveThresh
    and GMR.GetDistance("player","target", "<", 35)
    and GMR.IsCastable(Multishot) then
      GMR.Cast(Multishot)
  
    --SteadyShot
    elseif GetSpellInfo(SteadyShot) 
      and IsUsableSpell(SteadyShot) 
      and GMR.IsCastable(SteadyShot, "target") 
      and GMR.GetDistance("player", "target", "<", 35) then
      GMR.Cast(SteadyShot)
    end

  end
end)
end
