local receive_server_updates = false

function GMR.ClassRotation()
	local t = GMR.Tables.Spells.WARLOCK
	local playerAttackingEnemy = GMR.GetPlayerAttackingEnemy()

	if not IsMounted() and GMR.IsExecuting() and GMR.InLoS("target") and UnitExists("target") then 

		local dotted_units = {
			["Corruption"] = {

			},
			["CurseOfAgony"] = {

			},
			["Unstable Affliction"] = {

			},

		}

		-- Run through the attackables list and check if they are dotted
		for i = 1, #GMR.Tables.Attackables do
			local enemyunit = GMR.Tables.Attackables[i][1]
			local name = ""
			if GMR.ObjectExists(enemyunit) then
				name = GMR.ObjectName(enemyunit)
				if GMR.GetDebuffExpiration(enemyunit, t.Corruption, true) >= 3 then
					table.insert(dotted_units["Corruption"], enemyunit)
				end
				if GMR.GetDebuffExpiration(enemyunit, t.CurseOfAgony, true) >= 3 then
					table.insert(dotted_units["CurseOfAgony"], enemyunit)
				end
				if GMR.GetDebuffExpiration(enemyunit, t.UnstableAffliction, true) >= 3 then
					table.insert(dotted_units["Unstable Affliction"], enemyunit)
				end
			end
		end

		-- Run through each table in the dotted list and remove
		-- the units that have expired
		for k, v in pairs(dotted_units) do
			for i = 1, #v do
				local enemyunit = v[i]
				local name = ""
				if GMR.ObjectExists(enemyunit) then
					name = GMR.ObjectName(enemyunit)
					if GMR.GetDebuffExpiration(enemyunit, t[k], true) <= 3 then
						table.remove(dotted_units[k], i)
					end
				end
			end
		end




		--print("Dotted units Corruption: " .. #dotted_units["Corruption"])
		--print("Dotted units CurseOfAgony: " .. #dotted_units["CurseOfAgony"])
		--print("Dotted units UnstableAffliction: " .. #dotted_units["Unstable Affliction"])
		--print("Dotted units Haunt: " .. #dotted_units["Haunt"])


		-- PetAttack
		if GMR.PlayerHasPet() then
			if playerAttackingEnemy then
				if not GMR.UnitIsUnit(playerAttackingEnemy, "pettarget") then
					GMR.PetAttack(playerAttackingEnemy)
				end
			elseif not GMR.UnitIsUnit("target", "pettarget") then
				GMR.PetAttack()
			end
		end

		-- Somehow work pullcount into this

		-- Healthstone
		if GMR.IsCheckedSpell("Healthstone")
			and GMR.GetHealthFilter("Healthstone")
			and GMR.GetHealth("player") <= GMR.GetHealthFilter("Healthstone")
			and GMR.HealthstoneExists()
			and GetItemCooldown(GMR.GetItemId(GMR.HealthstoneExists())) == 0 then
			GMR.Use(GMR.HealthstoneExists())

			-- How of Terror
		elseif GMR.IsCheckedSpell("HowlOfTerror")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetUnitFilter("HowlOfTerror")
			and GMR.GetNumAttackingEnemies("target", 8) >= GMR.GetUnitFilter("HowlOfTerror")
			and GMR.IsCastable(t.HowlOfTerror) then
			GMR.Cast(t.HowlOfTerror)

			-- Fear
		elseif GMR.IsCheckedSpell("Fear")
			and GMR.UnitHealthMax("target") > 10
			and GMR.IsCastable(fear)
			and GMR.GetCCUnit()
			and GMR.IsCastable(fear, GMR.GetCCUnit()) then
			GMR.CCUnit(GMR.GetCCUnit())

			-- Fel Domination
		elseif GMR.IsCheckedSpell("FelDomination")
			and not GMR.IsPetDenied()
			and (not GMR.PlayerHasPet()
				or (GMR.IsSpellKnown(t.Firebolt)
					and (GMR.IsCheckedSpell("SummonVoidwalker")
						or GMR.IsCheckedSpell("SummonFelhunter")
						or GMR.IsCheckedSpell("SummonSuccubus"))))
			and GMR.IsCastable(t.FelDomination)
			and not GMR.IsMoving() then
			GMR.Cast(t.FelDomination)

			-- Summon Voidwalker
		elseif GMR.IsCheckedSpell("SummonVoidwalker")
			and not GMR.IsPetDenied()
			and (not GMR.PlayerHasPet()
				or GMR.IsSpellKnown(t.Firebolt))
			and GMR.HasBuff("player", t.FelDomination)
			and GMR.IsCastable(t.SummonVoidwalker)
			and not GMR.IsMoving()
			and GMR.SoulshardExists() then
			GMR.Cast(t.SummonVoidwalker)

			-- Summon Felguard
		elseif GMR.IsCheckedSpell("SummonFelguard")
			and not GMR.IsPetDenied()
			and (not GMR.PlayerHasPet()
				or GMR.IsSpellKnown(t.Firebolt))
			and GMR.HasBuff("player", t.FelDomination)
			and GMR.IsCastable(t.SummonFelguard)
			and not GMR.IsMoving()
			and GMR.SoulshardExists() then
			GMR.Cast(t.SummonFelguard)

			-- Summon Felhunter
		elseif GMR.IsCheckedSpell("SummonFelhunter")
			and not GMR.IsPetDenied()
			and (not GMR.PlayerHasPet()
				or GMR.IsSpellKnown(t.Firebolt))
			and GMR.HasBuff("player", t.FelDomination)
			and GMR.IsCastable(t.SummonFelhunter)
			and not GMR.IsMoving()
			and GMR.SoulshardExists() then
			GMR.Cast(t.SummonFelhunter)

			-- Summon Succubus
		elseif GMR.IsCheckedSpell("SummonSuccubus")
			and not GMR.IsPetDenied()
			and (not GMR.PlayerHasPet()
				or GMR.IsSpellKnown(t.Firebolt))
			and GMR.HasBuff("player", t.FelDomination)
			and GMR.IsCastable(t.SummonSuccubus)
			and not GMR.IsMoving()
			and GMR.SoulshardExists() then
			GMR.Cast(t.SummonSuccubus)

			-- Summon Imp
		elseif GMR.IsCheckedSpell("SummonImp")
			and not GMR.IsPetDenied()
			and not GMR.PlayerHasPet()
			and GMR.HasBuff("player", t.FelDomination)
			and GMR.IsCastable(t.SummonImp)
			and not GMR.IsMoving() then
			GMR.Cast(t.SummonImp)

			-- Soul Link
		elseif GMR.IsCheckedSpell("SoulLink")
			and not GMR.IsPetDenied()
			and GMR.PlayerHasPet()
			and GMR.GetDistance("player", "pet", "<", 40)
			and not GMR.HasBuff("player", t.SoulLink)
			and GMR.IsCastable(t.SoulLink) then
			GMR.Cast(t.SoulLink)

			-- Fel Armor
		elseif GMR.IsCheckedSpell("FelArmor")
			and GMR.GetBuffExpiration("player", t.FelArmor) < 10
			and GMR.IsCastable(t.FelArmor) then
			GMR.Cast(t.FelArmor)

			-- Demon Armor
		elseif GMR.IsCheckedSpell("DemonArmor")
			and not GMR.HasBuff("player", t.FelArmor)
			and GMR.GetBuffExpiration("player", t.DemonArmor) < 10
			and GMR.IsCastable(t.DemonArmor) then
			GMR.Cast(t.DemonArmor)

			-- Demon Skin
		elseif GMR.IsCheckedSpell("DemonArmor")
			and not GMR.HasBuff("player", t.FelArmor)
			and not GMR.HasBuff("player", t.DemonArmor)
			and GMR.GetBuffExpiration("player", t.DemonSkin) < 10
			and GMR.IsCastable(t.DemonSkin) then
			GMR.Cast(t.DemonSkin)

			-- Wand MaxHealth < 10
		elseif GMR.UnitHealthMax("target") < 10
			and GMR.IsCheckedSpell("Wand")
			and GMR.IsWandUsable()
			and GMR.IsCastable(t.Wand, "target")
			and not GMR.IsMoving() then
			if not GMR.IsWanding()
				and not GMR.GetDelay("CombatRotation") then
				GMR.Cast(t.Wand, "target");
				GMR.SetDelay("CombatRotation", 0.5)
			end

			-- Shadow Bolt (Shadow Trance)
		elseif GMR.IsCheckedSpell("ShadowBolt")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("ShadowBolt")
			and GMR.GetMana("player") >= GMR.GetManaFilter("ShadowBolt")
			and GMR.HasBuff("player", t.ShadowTrance)
			and GMR.IsCastable(t.ShadowBolt, "target") then
			GMR.Cast(t.ShadowBolt, "target")

			-- Incinerate (Backlash)
		elseif GMR.IsCheckedSpell("Incinerate")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("Incinerate")
			and GMR.GetMana("player") >= GMR.GetManaFilter("Incinerate")
			and GMR.HasBuff("player", t.Backlash)
			and GMR.IsCastable(t.Incinerate, "target") then
			GMR.Cast(t.Incinerate, "target")

			-- Shadow Bolt (Backlash)
		elseif GMR.IsCheckedSpell("ShadowBolt")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("ShadowBolt")
			and GMR.GetMana("player") >= GMR.GetManaFilter("ShadowBolt")
			and GMR.HasBuff("player", t.Backlash)
			and GMR.IsCastable(t.ShadowBolt, "target") then
			GMR.Cast(t.ShadowBolt, "target")

			-- Sacrifice
		elseif GMR.IsCheckedSpell("Sacrifice")
			and GMR.GetHealthFilter("Sacrifice")
			and GMR.GetHealth("player") < GMR.GetHealthFilter("Sacrifice")
			and GMR.PlayerHasPet()
			and GMR.IsCastable(t.Sacrifice) then
			GMR.Cast(t.Sacrifice)

			-- Death Coil
		elseif GMR.IsCheckedSpell("DeathCoil")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetHealthFilter("DeathCoil")
			and GMR.GetHealthFilter("DeathCoil") > GMR.GetHealth("player")
			and GMR.IsCastable(t.DeathCoil, "target") then
			GMR.Cast(t.DeathCoil, "target")

			-- Shadowfury
		elseif GMR.IsCheckedSpell("Shadowfury")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetUnitFilter("Shadowfury")
			and GMR.GetNumAttackingEnemies("target", 10) >= GMR.GetUnitFilter("Shadowfury")
			and GMR.IsAoECastable(t.Shadowfury, "target") then
			GMR.CastAoE(t.Shadowfury, "target")

			-- Drain Life (Defensive)
		elseif GMR.IsCheckedSpell("DrainLifeDefensive")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetHealthFilter("DrainLifeDefensive")
			and GMR.GetHealthFilter("DrainLifeDefensive") > GMR.GetHealth("player")
			and GMR.IsCastable(t.DrainLife, "target")
			and not GMR.IsMoving() then
			GMR.Cast(t.DrainLife, "target")

			-- Dark Pact
		elseif GMR.IsCheckedSpell("DarkPact")
			and not GMR.IsPetDenied()
			and GMR.GetManaFilter("DarkPact")
			and GMR.GetManaFilter("DarkPact") > GMR.GetMana("player")
			and GMR.PlayerHasPet()
			and GMR.GetMana("pet") > 20
			and GMR.GetDistance("player", "pet", "<", 18.5)
			and GMR.IsCastable(t.DarkPact) then
			GMR.Cast(t.DarkPact)

			-- Life Tap
		elseif GMR.IsCheckedSpell("LifeTap")
			and GMR.GetHealthFilter("LifeTap")
			and GMR.GetManaFilter("LifeTap")
			and GMR.GetMana("player") <= GMR.GetManaFilter("LifeTap")
			and GMR.GetHealthFilter("LifeTap") < GMR.GetHealth("player")
			and GMR.IsCastable(t.LifeTap) then
			GMR.Cast(t.LifeTap)
			-- Haunt

		end
		-- Unstable Affliction
		if GMR.IsCheckedSpell("UnstableAffliction") then
			for i = 1, #GMR.Tables.Attackables do
				local attackable = GMR.Tables.Attackables[i][1]
				if GMR.ObjectExists(attackable)
					and GMR.UnitHealthMax(attackable) > 10
					and GMR.InCombat()
					and GMR.GetHealthFilter("UnstableAffliction")
					and GMR.GetHealth(attackable) >= GMR.GetHealthFilter("UnstableAffliction")
					and GMR.GetManaFilter("UnstableAffliction")
					and GMR.GetDebuffExpiration(attackable, t.UnstableAffliction, true) < 3
					and GMR.GetMana("player") >= GMR.GetManaFilter("UnstableAffliction")
					and GMR.GetHealth(attackable) > 15
					and GMR.IsCastable(t.UnstableAffliction, attackable) then
					if #dotted_units["Unstable Affliction"] < GMR.GetPullCount() then
						GMR.Cast(t.UnstableAffliction, attackable)
					end
				end
			end
		end
		-- Corruption
		if GMR.IsCheckedSpell("Corruption") then
			for i = 1, #GMR.Tables.Attackables do
				local attackable = GMR.Tables.Attackables[i][1]
				if GMR.ObjectExists(attackable)
					and GMR.UnitHealthMax(attackable) > 10
					and GMR.InCombat()
					and GMR.GetHealthFilter("Corruption")
					and GMR.GetHealth(attackable) >= GMR.GetHealthFilter("Corruption")
					and GMR.GetManaFilter("Corruption")
					and GMR.GetDebuffExpiration(attackable, t.Corruption, true) < 1
					and GMR.GetMana("player") >= GMR.GetManaFilter("Corruption")
					and GMR.IsCastable(t.Corruption, attackable) then
					if #dotted_units["Corruption"] < GMR.GetPullCount() then
						GMR.Cast(t.Corruption, attackable)
					end
				end
			end
		end
		-- Curse of Agony
		if GMR.IsCheckedSpell("CurseOfAgony") then
			for i = 1, #GMR.Tables.Attackables do
				local attackable = GMR.Tables.Attackables[i][1]
				if GMR.ObjectExists(attackable)
					and GMR.UnitHealthMax(attackable) > 10
					and GMR.InCombat()
					and GMR.GetHealthFilter("CurseOfAgony")
					and GMR.GetHealth(attackable) >= GMR.GetHealthFilter("CurseOfAgony")
					and GMR.GetManaFilter("CurseOfAgony")
					and GMR.GetDebuffExpiration(attackable, t.CurseOfAgony, true) < 2
					and GMR.GetMana("player") >= GMR.GetManaFilter("CurseOfAgony")
					and GMR.IsCastable(t.CurseOfAgony, attackable) then
					if #dotted_units["CurseOfAgony"] < GMR.GetPullCount() then
						GMR.Cast(t.CurseOfAgony, attackable)
					end
				end
			end
		end

		--UA @target
		if GMR.IsCheckedSpell("UnstableAffliction")
			and GMR.UnitHealthMax("target") > 10 
			and GMR.InCombat()
			and GMR.GetHealthFilter("UnstableAffliction")
			and GMR.GetHealth("target") >= GMR.GetHealthFilter("UnstableAffliction")
			and GMR.GetManaFilter("UnstableAffliction")
			and GMR.GetDebuffExpiration("target", t.UnstableAffliction, true) < 3
			and GMR.GetMana("player") >= GMR.GetManaFilter("UnstableAffliction")
			and GMR.GetHealth("target") > 15
			and GMR.IsCastable(t.UnstableAffliction, "target") then
				GMR.Cast(t.UnstableAffliction, "target")
		end
		
		--CoA @target
		if GMR.IsCheckedSpell("CurseOfAgony")
			and GMR.UnitHealthMax("target") > 10 
			and GMR.InCombat()
			and GMR.GetHealthFilter("CurseOfAgony")
			and GMR.GetHealth("target") >= GMR.GetHealthFilter("CurseOfAgony")
			and GMR.GetManaFilter("CurseOfAgony")
			and GMR.GetDebuffExpiration("target", t.CurseOfAgony, true) < 1
			and GMR.GetMana("player") >= GMR.GetManaFilter("CurseOfAgony")
			and GMR.GetHealth("target") > 10
			and GMR.IsCastable(t.CurseOfAgony, "target") then
				GMR.Cast(t.CurseOfAgony, "target")
		end
		
		--C @target
		if GMR.IsCheckedSpell("Corruption")
			and GMR.UnitHealthMax("target") > 10 
			and GMR.InCombat()
			and GMR.GetHealthFilter("Corruption")
			and GMR.GetHealth("target") >= GMR.GetHealthFilter("Corruption")
			and GMR.GetManaFilter("Corruption")
			and GMR.GetDebuffExpiration("target", t.Corruption, true) < 1
			and GMR.GetMana("player") >= GMR.GetManaFilter("Corruption")
			and GMR.GetHealth("target") > 10
			and GMR.IsCastable(t.Corruption, "target") then
				GMR.Cast(t.Corruption, "target")
		end
		
		-- Haunt
		if GMR.IsCheckedSpell("Haunt")
			and GMR.UnitHealthMax("target") > 10 
			and GMR.GetManaFilter("Haunt")
			and GMR.GetMana("player") >= GMR.GetManaFilter("Haunt")
			and GMR.IsCastable(t.Haunt, "target")
			and not GMR.IsMoving() then
				GMR.Cast(t.Haunt, "target")
		
		-- Health Funnel
		elseif GMR.IsCheckedSpell("HealthFunnel")
			and not GMR.IsPetDenied()
			and GMR.PlayerHasPet()
			and GMR.GetDistance("player", "pet", "<", 18.5)
			and GMR.GetHealthFilter("HealthFunnel")
			and GMR.GetPetHealthFilter("HealthFunnel")
			and GMR.GetHealthFilter("HealthFunnel") < GMR.GetHealth("player")
			and GMR.GetPetHealthFilter("HealthFunnel") > GMR.GetHealth("pet")
			and GMR.IsCastable(t.HealthFunnel)
			and not GMR.IsMoving() then
			GMR.Cast(t.HealthFunnel)

			-- Shadowburn
		elseif GMR.IsCheckedSpell("Shadowburn")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("Shadowburn")
			and GMR.GetMana("player") >= GMR.GetManaFilter("Shadowburn")
			and GMR.GetHealthFilter("Shadowburn")
			and GMR.GetHealth("target") <= GMR.GetHealthFilter("Shadowburn")
			and GMR.IsCastable(t.Shadowburn, "target") then
			GMR.Cast(t.Shadowburn, "target")

			-- Drain Soul
		elseif GMR.IsCheckedSpell("DrainSoul")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetHealthFilter("DrainSoul")
			and GMR.GetManaFilter("DrainSoul")
			and GMR.GetShardsFilter("DrainSoul")
			and GMR.GetMana("player") >= GMR.GetManaFilter("DrainSoul")
			and GMR.GetHealthFilter("DrainSoul") > GMR.GetHealth("target")
			and IsUsableSpell(t.DrainSoul)
			and GMR.GetSpellCooldown(t.DrainSoul) == 0
			and GMR.InLoS("player", "target")
			and not GMR.IsMoving()
			and GMR.GetInventorySpace() ~= 0
			and GMR.GetShardsFilter("DrainSoul") > GetItemCount(6265) then
			if (CastingInfo() or ChannelInfo()) then
				GMR.SpellStopCasting()
			else
				GMR.Cast(t.DrainSoul, "target")
			end

			-- Rain of Fire
		elseif GMR.IsCheckedSpell("RainOfFire")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("RainOfFire")
			and GMR.GetUnitFilter("RainOfFire")
			and GMR.ObjectPosition("target")
			and GMR.GetMana("player") >= GMR.GetManaFilter("RainOfFire")
			and GMR.GetUnitFilter("RainOfFire") <= GMR.GetNumAttackingEnemies("target", 8.5)
			and GMR.IsAoECastable(t.RainOfFire, "target")
			and not GMR.IsMoving() then
			GMR.CastAoE(t.RainOfFire, "target")

			-- Hellfire
		elseif GMR.IsCheckedSpell("Hellfire")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("Hellfire")
			and GMR.GetMana("player") >= GMR.GetManaFilter("Hellfire")
			and GMR.GetHealthFilter("Hellfire")
			and GMR.GetHealth("player") >= GMR.GetHealthFilter("Hellfire")
			and GMR.GetUnitFilter("Hellfire")
			and GMR.GetNumAttackingEnemies("target", 6.5) >= GMR.GetUnitFilter("Hellfire")
			and GMR.GetDistance("player", "target", "<", 6.5)
			and GMR.IsCastable(t.Hellfire) then
			GMR.Cast(t.Hellfire)

			-- Soul Fire
		elseif GMR.IsCheckedSpell("SoulFire")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("SoulFire")
			and GMR.GetMana("player") >= GMR.GetManaFilter("SoulFire")
			and GMR.IsCastable(t.SoulFire, "target")
			and not GMR.InCombat()
			and not GMR.IsMoving() then
			GMR.Cast(t.SoulFire, "target")

			-- Curse of the Elements
		elseif GMR.IsCheckedSpell("CurseOfTheElements")
			and GMR.UnitHealthMax("target") > 10
			and GMR.InCombat()
			and not GMR.HasDebuff("target", t.CurseOfTheElements)
			and GMR.IsCastable(t.CurseOfTheElements, "target") then
			GMR.Cast(t.CurseOfTheElements, "target")

			-- Conflagrate
		elseif GMR.IsCheckedSpell("Conflagrate")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("Conflagrate")
			and GMR.GetMana("player") >= GMR.GetManaFilter("Conflagrate")
			and GMR.IsCastable(t.Conflagrate, "target") then
			GMR.Cast(t.Conflagrate, "target")

			-- Immolate
		elseif GMR.IsCheckedSpell("Immolate")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("Immolate")
			and GMR.GetDebuffExpiration("target", t.Immolate, true) < 3
			and GMR.GetMana("player") >= GMR.GetManaFilter("Immolate")
			and not GMR.HasBuff("player", t.Backdraft)
			and GMR.IsCastable(t.Immolate, "target")
			and not GMR.IsMoving() then
			GMR.Cast(t.Immolate, "target")

			-- SF
		elseif GMR.IsCheckedSpell("Shadowflame")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("Shadowflame")
			and GMR.GetMana("player") >= GMR.GetManaFilter("Shadowflame")
			and GMR.GetUnitFilter("Shadowflame")
			and GMR.GetNumAttackingEnemies("target", 6.5) >= GMR.GetUnitFilter("Shadowflame")
			and GMR.GetDistance("player", "target", "<", 6.5)
			and GMR.IsCastable(t.Shadowflame) then
			GMR.Cast(t.Shadowflame)

			-- Inferno
		elseif GMR.IsCheckedSpell("Inferno")
			and ((GMR.GetHealthFilter("Inferno")
				and GMR.GetHealth("target") >= GMR.GetHealthFilter("Inferno"))
				or (GMR.GetUnitFilter("Inferno")
					and GMR.GetNumAttackingEnemies("target", 6.5) >= GMR.GetUnitFilter("Inferno")))
			and GMR.ItemExists(5565)
			and GMR.IsAoECastable(t.Inferno, "target") then
			GMR.Cast(t.Inferno, "target")

			-- Demonic Empowerment
		elseif GMR.IsCheckedSpell("DemonicEmpowerment")
			and ((GMR.GetHealthFilter("DemonicEmpowerment")
				and GMR.GetHealth("target") >= GMR.GetHealthFilter("DemonicEmpowerment"))
				or (GMR.GetUnitFilter("DemonicEmpowerment")
					and GMR.GetNumAttackingEnemies("target", 6.5) >= GMR.GetUnitFilter("DemonicEmpowerment")))
			and GMR.GetDistance("player", "target", "<", 25)
			and GMR.IsCastable(t.DemonicEmpowerment) then
			GMR.Cast(t.DemonicEmpowerment, "target")

			-- Metamorphosis
		elseif GMR.IsCheckedSpell("Metamorphosis")
			and ((GMR.GetHealthFilter("Metamorphosis")
				and GMR.GetHealth("target") >= GMR.GetHealthFilter("Metamorphosis"))
				or (GMR.GetUnitFilter("Metamorphosis")
					and GMR.GetNumAttackingEnemies("target", 6.5) >= GMR.GetUnitFilter("Metamorphosis")))
			and GMR.GetDistance("player", "target", "<", 25)
			and GMR.IsCastable(t.Metamorphosis) then
			GMR.Cast(t.Metamorphosis, "target")

			-- Chaos Bolt
		elseif GMR.IsCheckedSpell("ChaosBolt")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("ChaosBolt")
			and GMR.GetMana("player") >= GMR.GetManaFilter("ChaosBolt")
			and GMR.IsCastable(t.ChaosBolt, "target")
			and not GMR.IsMoving() then
			GMR.Cast(t.ChaosBolt, "target")

			-- Incinerate
		elseif GMR.IsCheckedSpell("Incinerate")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("Incinerate")
			and GMR.GetMana("player") >= GMR.GetManaFilter("Incinerate")
			and GMR.IsCastable(t.Incinerate, "target")
			and not GMR.IsMoving() then
			GMR.Cast(t.Incinerate, "target")


			-- Unstable Affliction


			-- Drain Life
		elseif GMR.IsCheckedSpell("DrainLife")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("DrainLife")
			and GMR.GetMana("player") >= GMR.GetManaFilter("DrainLife")
			and GMR.IsCastable(t.DrainLife, "target")
			and not GMR.IsMoving() then
			GMR.Cast(t.DrainLife, "target")

			-- ShadowBolt
		elseif GMR.IsCheckedSpell("ShadowBolt")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("ShadowBolt")
			and GMR.GetMana("player") >= GMR.GetManaFilter("ShadowBolt")
			and GMR.IsCastable(t.ShadowBolt, "target")
			and not GMR.IsMoving() then
			GMR.Cast(t.ShadowBolt, "target")

			-- Searing Pain
		elseif GMR.IsCheckedSpell("SearingPain")
			and GMR.UnitHealthMax("target") > 10
			and GMR.GetManaFilter("SearingPain")
			and GMR.GetMana("player") >= GMR.GetManaFilter("SearingPain")
			and GMR.IsCastable(t.SearingPain, "target")
			and not GMR.IsMoving() then
			GMR.Cast(t.SearingPain, "target")

			-- Wand
		elseif GMR.IsCheckedSpell("Wand")
			and GMR.IsWandUsable()
			and GMR.IsCastable(t.Wand, "target")
			and not GMR.IsMoving() then
			if not GMR.IsWanding()
				and not GMR.GetDelay("CombatRotation") then
				GMR.Cast(t.Wand, "target");
				GMR.SetDelay("CombatRotation", 0.5)
			end
		end
	end
end
