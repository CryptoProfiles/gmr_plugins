C_Timer.NewTicker(0.1, function()
    if UnitClass("player") == "WARRIOR" then
      enemyCount = GetNumAttackingEnemies(unit, distance)
      execute = GetSpellInfo(25236)
      hamstring = GetSpellInfo(1715)
      sweepingStrikes = GetSpellInfo(12328)
      bladestorm = GetSpellInfo(46924)
      overpower = GetSpellInfo(7384)
      mortalStrike = GetSpellInfo(30330)
      charge = GetSpellInfo(11578)
      rend = GetSpellInfo(25208)
      if not mounted and GMR.IsExecuting() and GMR.InLoS("target") and UnitExists("target") then
        if AuraUtil.FindAuraByName("Battle Stance", "player", "HELPFUL") then
          if UnitAffectingCombat("player") then
            -- (Looks for Sudden Death Proc and Executes otherwise normal execute behavior)
            if AuraUtil.FindAuraByName("Sudden Death", "player", "HELPFUL") and IsUsableSpell(execute) then
                GMR.Cast(execute)
            elseif GetSpellInfo(execute) and GMR.GetHealth("target") <= 20 and IsUsableSpell(execute)  then
              GMR.Cast(execute)
            end
            -- (Applies Hamstring if missing)
            if not AuraUtil.FindAuraByName("Hamstring", "target", "HARMFUL") and IsUsableSpell(hamstring) then
              GMR.Cast(hamstring)
            end
            -- (Charge if target is out of melee range)
            if GMR.GetDistance("target") >= 8 and GMR.GetDistance("target") <= 25 then
                GMR.Cast(Charge)
            end
            -- (Rend)
            if not AuraUtil.FindAuraByName("Rend","target","HARMFUL") and IsUsableSpell(rend) then
              GMR.Cast(rend)
            end
            -- (Mortal Strike)
            if GetSpellInfo(mortalStrike) and IsUsableSpell(mortalStrike) then
              GMR.Cast(mortalStrike)
            end
            -- (Overpower if usable)
            if GetSpellInfo(overpower) and IsUsableSpell(overpower) then
                GMR.Cast(overpower)
            end
            -- (Sweeping strikes if more than 2 enemey's within 8 yards)
            if GetSpellInfo(sweepingStrikes) and enemyCount >= 2 and IsUsableSpell(sweepingStrikes) then
                GMR.Cast(sweepingStrikes)
            end
            -- (Bladestorms if there's 3 or more enemy's within 8 yards)
            if GetSpellInfo(bladestorm) and enemyCount >= 3 and IsUsableSpell(bladestorm) then
                GMR.Cast(bladestorm)
            end
          end
        end
      end
    end
  end)