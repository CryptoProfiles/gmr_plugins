C_Timer.NewTicker(0.1, function()
	if UnitClass("player") == "Monk" then 

			-- Spells
		tigerpalm = GetSpellInfo(100780)
		blackoutkick = GetSpellInfo(100784)
		risingsunkick = GetSpellInfo(107428)
		fistsoffury = GetSpellInfo(113656)
		spinningcranekick = GetSpellInfo(101546)
		strikeoftheWindlord = GetSpellInfo(392983)
		touchofDeath = GetSpellInfo(322109)
		touchofKarma = GetSpellInfo(122470)
		vivifyHEAL = GetSpellInfo(116670)
		expelHarm = GetSpellInfo(322101)
		chiWave = GetSpellInfo(115098)
		stormEarthFireCD = GetSpellInfo(137639)
		invokeAXuenCD = GetSpellInfo(123904)
		fortifyingBrewDefensive = GetSpellInfo(115203)
		roll = GetSpellInfo(109132)
		interupt = GetSpellInfo(116705)
		statueAOE = GetSpellInfo(388686)
		

		local palmBuff = GetSpellInfo(129914)
		local risingSunKickBuff = GetSpellInfo(202090)
		local blackoutKickBuff = GetSpellInfo(116768)
		local spinningCraneKickBuff = GetSpellInfo(325202)
		local vivifyBuff = GetSpellInfo(392883)
		local stormEarthFireBUFF = GetSpellInfo(137639)


		-- ITEMS
		local rousingFrost = GetItemInfo(190328)
		local rousingFire = GetItemInfo(190320)
		local rousingEarth = GetItemInfo(190315)
		local rousingAir = GetItemInfo(190326)
		local rousingOrder = GetItemInfo(190322)
		local rousingDecay = GetItemInfo(190330)
		local rousingIre = GetItemInfo(190451)
		if GetItemCount(rousingFrost) >= 10 then
			GMR.Use(rousingFrost)
		end
		if GetItemCount(rousingFire) >= 10 then
			GMR.Use(rousingFire)
		end
		if GetItemCount(rousingEarth) >= 10 then
			GMR.Use(rousingEarth)
		end
		if GetItemCount(rousingAir) >= 10 then
			GMR.Use(rousingAir)
		end
		if GetItemCount(rousingOrder) >= 10 then
			GMR.Use(rousingOrder)
		end
		if GetItemCount(rousingDecay) >= 10 then
			GMR.Use(rousingDecay)
		end
		if GetItemCount(rousingIre) >= 10 then
			GMR.Use(rousingIre)
		end

		if not GMR.GetDelay("CustomQuest") then
			if GossipFrame:IsShown() then
				SelectGossipOption(2); GMR.SetDelay("CustomQuest", 3)
			end
		end
		
		if not mounted and GMR.IsExecuting() and UnitExists("target") then
			if GMR.IsExecuting() then
				for i = 1, #GMR.Tables.Attackables do
					local enemyunit = GMR.Tables.Attackables[i][1]
					if GMR.ObjectExists(enemyunit) then
						if GMR.GetDistance("player", "target", ">", 20) and GMR.GetDistance("player", "target", "<", 50) then
							if not GMR.InCombat("player", true) then
								if not GMR.GetDelay("CustomShit") then
									if GMR.GetSpellCooldown(109132) == 0 then
										if GetSpellInfo(roll) and IsUsableSpell(roll) and GMR.IsCastable(roll) then
											GMR.Cast(roll); GMR.SetDelay("CustomShit", 2)
										end
									end
								end
							end
						end
					end
				end
			end

			if GMR.InCombat("player") then
				-- Storm Earth and Fire - Cooldowns
				-- Multiple target rotation
				if GMR.GetNumEnemies("player", 10) >= 2 then
					if GMR.GetHealth("player") < 60 then
						-- touch of karma
						if GMR.GetSpellCooldown(122470) == 0 then
							if GetSpellInfo(touchofKarma) and IsUsableSpell(touchofKarma) and GMR.IsCastable(touchofKarma) and GMR.GetHealth("target") > 20 then
								GMR.Cast(touchofKarma)
							end
						-- Fortifying Brew
						elseif GMR.GetSpellCooldown(115203) == 0 then
							if GetSpellInfo(fortifyingBrewDefensive) and IsUsableSpell(fortifyingBrewDefensive) and GMR.IsCastable(fortifyingBrewDefensive) and GMR.GetHealth("target") > 20 then
								GMR.Cast(fortifyingBrewDefensive)
							end
						-- chi wave
						elseif GMR.GetSpellCooldown(115098) == 0 then
							if GetSpellInfo(chiWave) and IsUsableSpell(chiWave) and GMR.IsCastable(chiWave, "target") and GMR.GetHealth("target") > 20 then
								GMR.Cast(chiWave)
							end
						-- Expel Harm
						elseif GMR.GetSpellCooldown(322101) == 0 then
							if GMR.UnitPower("player") >= 30 then
								if GetSpellInfo(expelHarm) and IsUsableSpell(expelHarm) and GMR.IsCastable(expelHarm) then
									GMR.Cast(expelHarm)
								end
							end
						-- touch of death
						elseif GMR.GetSpellCooldown(322109) == 0 then
							if GetSpellInfo(touchofDeath) and IsUsableSpell(touchofDeath) and GMR.IsCastable(touchofDeath, "target") and GMR.GetHealth("target") > 30 then
								GMR.Cast(touchofDeath)
							end
						-- Vivify Heal
						elseif GMR.GetSpellCooldown(116670) == 0 then
							if GMR.UnitPower("player") >= 30 then
								if GetSpellInfo(vivifyHEAL) and IsUsableSpell(vivifyHEAL) and GMR.IsCastable(vivifyHEAL) then
									GMR.Cast(vivifyHEAL)
								end
							end
						end
					end
					-- Vivify Buff Cast
					if GMR.GetHealth("player") <= 85 and GMR.HasBuff("player", vivifyBuff) then
						if GMR.GetSpellCooldown(116670) == 0 then
							if GMR.UnitPower("player") >= 30 then
								if GetSpellInfo(vivifyHEAL) and IsUsableSpell(vivifyHEAL) and GMR.IsCastable(vivifyHEAL) then
									GMR.Cast(vivifyHEAL)
								end
							end
						end
					end
					-- Interupt
					if GMR.UnitCastingInfo("target") and GMR.GetSpellCooldown(116705) == 0 then
						if GetSpellInfo(interupt) and IsUsableSpell(interupt) and GMR.IsCastable(interupt, "target") and GMR.GetHealth("target") > 20 then
							GMR.Cast(interupt)
						end
					-- touch of death
					elseif GMR.GetSpellCooldown(322109) == 0 then
						if GetSpellInfo(touchofDeath) and IsUsableSpell(touchofDeath) and GMR.IsCastable(touchofDeath, "target") and GMR.GetHealth("target") > 30 then
							GMR.Cast(touchofDeath)
						end
					-- touch of karma
					elseif GMR.GetSpellCooldown(122470) == 0 then
						if GetSpellInfo(touchofKarma) and IsUsableSpell(touchofKarma) and GMR.IsCastable(touchofKarma) and GMR.GetHealth("target") > 20 then
							GMR.Cast(touchofKarma)
						end
					-- fortifying brrew
					elseif GMR.GetSpellCooldown(115203) == 0 then
						if GetSpellInfo(fortifyingBrewDefensive) and IsUsableSpell(fortifyingBrewDefensive) and GMR.IsCastable(fortifyingBrewDefensive) and GMR.GetHealth("target") > 20 then
							GMR.Cast(fortifyingBrewDefensive)
						end
					-- statue
					elseif GMR.GetSpellCooldown(388686) == 0 then
						if GetSpellInfo(statueAOE) and IsUsableSpell(statueAOE) and GMR.IsCastable(statueAOE) and GMR.GetHealth("target") > 20 then
							GMR.Cast(statueAOE)
							GMR.ClickPosition(GMR.ObjectPosition("player"))
						end
					-- storm earth fire
					elseif GMR.GetSpellCooldown(137639) == 0 then
						if not GMR.HasBuff("player", stormEarthFireBUFF) then
							if GetSpellInfo(stormEarthFireCD) and IsUsableSpell(stormEarthFireCD) and GMR.IsCastable(stormEarthFireCD, "target") and GMR.GetHealth("target") > 20 then
								GMR.Cast(stormEarthFireCD)
							end
						end
						--invoke xuen CD
					elseif GMR.GetSpellCooldown(123904) == 0 then
						if GetSpellInfo(invokeAXuenCD) and IsUsableSpell(invokeAXuenCD) and GMR.IsCastable(invokeAXuenCD, "target") and GMR.GetHealth("target") > 20 then
							GMR.Cast(invokeAXuenCD)
						end
						-- chi wave
					elseif GMR.GetSpellCooldown(115098) == 0 then
						if GetSpellInfo(chiWave) and IsUsableSpell(chiWave) and GMR.IsCastable(chiWave, "target") and GMR.GetHealth("target") > 20 then
							GMR.Cast(chiWave)
						end
						-- spinning crane kick
					end
					--spinning crane
					if GMR.HasBuff("player", spinningCraneKickBuff) then
						if GMR.GetSpellCooldown(101546) == 0 then
							if GetSpellInfo(spinningcranekick) and IsUsableSpell(spinningcranekick) and GMR.IsCastable(spinningcranekick, "target") and GMR.GetHealth("target") > 10 then
								GMR.Cast(spinningcranekick)
							end
						end
					end
					-- rising sun kick
					if GMR.HasBuff("player", risingSunKickBuff) then
						if GetSpellInfo(risingsunkick) and IsUsableSpell(risingsunkick) and GMR.IsCastable(risingsunkick, "target") and GetSpellCooldown(risingsunkick) == 0 and GMR.GetDistance("player", "target", "<", 19) then
							GMR.Cast(risingsunkick)
						end
					end
					-- blackoutkick
					if GMR.HasBuff("player", blackoutKickBuff) then
						if GMR.GetSpellCooldown(116768) == 0 then
							if GetSpellInfo(blackoutkick) and IsUsableSpell(blackoutkick) and GMR.IsCastable(blackoutkick, "target") and GMR.GetHealth("target") > 10 then
								GMR.Cast(blackoutkick)
							end
						end
					end
					-- fists of fury
					if GetSpellInfo(fistsoffury) and IsUsableSpell(fistsoffury) and GMR.IsCastable(fistsoffury, "target") and GetSpellCooldown(fistsoffury) == 0 and GMR.GetHealth("target") > 15 
					and GMR.GetDistance("player", "target", "<", 19) then
						GMR.Cast(fistsoffury)
					end
					-- tigerpalm
					if GetSpellInfo(tigerpalm) and IsUsableSpell(tigerpalm) and GMR.IsCastable(tigerpalm, "target") and GetSpellCooldown(tigerpalm) == 0 and GMR.GetHealth("target") > 15 
					and GMR.GetDistance("player", "target", "<", 19) then
						GMR.Cast(tigerpalm)
					end
				else
					-- Single target Rotation
					-- Vivify Buff Cast
					if GMR.GetHealth("player") < 85 and GMR.HasBuff("player", vivifyBuff) then
						if GMR.GetSpellCooldown(116670) == 0 then
							if GMR.UnitPower("player") >= 30 then
								if GetSpellInfo(vivifyHEAL) and IsUsableSpell(vivifyHEAL) and GMR.IsCastable(vivifyHEAL) then
									GMR.Cast(vivifyHEAL)
								end
							end
						end
					end
					-- Healing Rotation
					if GMR.GetHealth("player") < 60 then
						if GMR.GetSpellCooldown(322101) == 0 then
							if GMR.UnitPower("player") >= 30 then
								if GetSpellInfo(expelHarm) and IsUsableSpell(expelHarm) and GMR.IsCastable(expelHarm) then
									GMR.Cast(expelHarm)
								end
							end
						elseif GMR.GetSpellCooldown(116670) == 0 then
							if GMR.UnitPower("player") >= 30 then
								if GetSpellInfo(vivifyHEAL) and IsUsableSpell(vivifyHEAL) and GMR.IsCastable(vivifyHEAL) then
									GMR.Cast(vivifyHEAL)
								end
							end
						end
					end
					-- Interupt
					if GMR.UnitCastingInfo("target") and GMR.GetSpellCooldown(116705) == 0 then
						if GetSpellInfo(interupt) and IsUsableSpell(interupt) and GMR.IsCastable(interupt, "target") and GMR.GetHealth("target") > 20 then
							GMR.Cast(interupt)
						end
					end
					-- rising sun kick
					if GMR.HasBuff("player", risingSunKickBuff) then
						if GetSpellInfo(risingsunkick) and IsUsableSpell(risingsunkick) and GMR.IsCastable(risingsunkick, "target") and GetSpellCooldown(risingsunkick) == 0 and GMR.GetDistance("player", "target", "<", 19) then
							GMR.Cast(risingsunkick)
						end
					end
					-- blackoutkick
					if GMR.HasBuff("player", blackoutKickBuff) then
						if GMR.GetSpellCooldown(116768) == 0 then
							if GetSpellInfo(blackoutkick) and IsUsableSpell(blackoutkick) and GMR.IsCastable(blackoutkick, "target") then
								GMR.Cast(blackoutkick)
							end
						end
					end
					if GMR.HasBuff("player", spinningCraneKickBuff) then
						if GetSpellInfo(spinningcranekick) and IsUsableSpell(spinningcranekick) and GMR.IsCastable(spinningcranekick) then
							GMR.Cast(spinningcranekick)
						end
					end
					-- touch of death
					if GMR.GetSpellCooldown(322109) == 0 then
						if GetSpellInfo(touchofDeath) and IsUsableSpell(touchofDeath) and GMR.IsCastable(touchofDeath, "target") and GMR.GetHealth("target") > 30 then
							GMR.Cast(touchofDeath)
						end
					end
					-- strikeoftheWindlord
					if GetSpellInfo(strikeoftheWindlord) and IsUsableSpell(strikeoftheWindlord) and GMR.IsCastable(strikeoftheWindlord, "target") and GetSpellCooldown(strikeoftheWindlord) == 0 and GMR.GetDistance("player", "target", "<", 19) then
						GMR.Cast(strikeoftheWindlord)
					end
					-- expel harm
					if GMR.GetSpellCooldown(322101) == 0 then
						if GMR.UnitPower("player") >= 20 then
							if GetSpellInfo(expelHarm) and IsUsableSpell(expelHarm) and GMR.IsCastable(expelHarm) then
								GMR.Cast(expelHarm)
							end
						end
					end
					-- chi wave
					if GMR.GetSpellCooldown(115098) == 0 then
						if GetSpellInfo(chiWave) and IsUsableSpell(chiWave) and GMR.IsCastable(chiWave, "target") and GMR.GetHealth("target") > 20 then
							GMR.Cast(chiWave)
						end
					end
					-- tigerpalm
					if GetSpellInfo(tigerpalm) and IsUsableSpell(tigerpalm) and GMR.IsCastable(tigerpalm, "target") and GetSpellCooldown(tigerpalm) == 0 and GMR.GetDistance("player", "target", "<", 19) then
						GMR.Cast(tigerpalm)
					end
					-- blackout kick
					if GetSpellInfo(blackoutkick) and IsUsableSpell(blackoutkick) and GMR.IsCastable(blackoutkick, "target") and GetSpellCooldown(blackoutkick) == 0
					and GMR.GetDistance("player", "target", "<", 19) then
						GMR.Cast(blackoutkick)
					end
				end
			end
		end
	end
end)