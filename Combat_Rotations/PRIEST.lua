local receive_server_updates = false

function GMR.ClassRotation()
	local t = GMR.Tables.Spells.PRIEST
	local VampiricEmbrace = GetSpellInfo(15286)
	
	if not IsMounted() and GMR.IsExecuting() and GMR.InLoS("target") and UnitExists("target") then
		if UnitAffectingCombat("player") and GetSpellInfo(VampiricEmbrace) and IsUsableSpell(VampiricEmbrace)
			and not AuraUtil.FindAuraByName(VampiricEmbrace, "player", "HELPFUL") then
				GMR.Cast(VampiricEmbrace)
		end
		
		local dotted_units = {
		["VampiricTouch"] = {

		},
		["ShadowWordPain"] = {

		},

		}

		-- Run through the attackables list and check if they are dotted
		for i = 1, #GMR.Tables.Attackables do
			local enemyunit = GMR.Tables.Attackables[i][1]
			local name = ""
			if GMR.ObjectExists(enemyunit) then
				name = GMR.ObjectName(enemyunit)
				if GMR.GetDebuffExpiration(enemyunit, t.VampiricTouch, true) >= 3 then
					table.insert(dotted_units["VampiricTouch"], enemyunit)
				end
				if GMR.GetDebuffExpiration(enemyunit, t.ShadowWordPain, true) >= 3 then
					table.insert(dotted_units["ShadowWordPain"], enemyunit)
				end
			end
		end

		-- Run through each table in the dotted list and remove
		-- the units that have expired
		for k, v in pairs(dotted_units) do
			for i = 1, #v do
				local enemyunit = v[i]
				local name = ""
				if GMR.ObjectExists(enemyunit) then
					name = GMR.ObjectName(enemyunit)
					if GMR.GetDebuffExpiration(enemyunit, t[k], true) <= 3 then
						table.remove(dotted_units[k], i)
					end
				end
			end
		end

		-- Silence
		if GMR.IsCheckedSpell("Silence")
		and GMR.UnitCastingTime("target", 0.5)
		and GMR.IsInterruptable("target")
		and GMR.IsCastable(t.Silence, "target")
		and not GMR.GetDelay("Interrupt") then
			GMR.Cast(t.Silence, "target", true);
			GMR.SetDelay("Interrupt", 0.25)

		-- Psychic Horror
		elseif GMR.IsCheckedSpell("PsychicHorror")
		and GMR.UnitCastingTime("target", 0.5)
		and GMR.IsInterruptable("target")
		and GMR.IsCastable(t.PsychicHorror, "target")
		and not GMR.GetDelay("Interrupt") then
			GMR.Cast(t.PsychicHorror, "target", true);
			GMR.SetDelay("Interrupt", 0.25)

		-- Power Word: Shield
		elseif GMR.IsCheckedSpell("PowerWordShield")
		and GMR.GetHealthFilter("PowerWordShield")
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("PowerWordShield")
		and not GMR.HasBuff("player", t.PowerWordShield)
		and not GMR.HasDebuff("player", t.WeakenedSoul)
		and GMR.IsCastable(t.PowerWordShield, "player") then
			GMR.Cast(t.PowerWordShield, "player")

		elseif GMR.IsCheckedSpell("PowerWordShield")
		and GMR.GetHealthFilter("PowerWordShield")
		and GMR.GetPartyHealth(GMR.GetHealthFilter("PowerWordShield"), t.PowerWordShield, t.WeakenedSoul)
		and GMR.IsCastable(t.PowerWordShield, GMR.GetPartyHealth(GMR.GetHealthFilter("PowerWordShield"), t.PowerWordShield, t.WeakenedSoul)) then
			GMR.Cast(t.PowerWordShield, GMR.GetPartyHealth(GMR.GetHealthFilter("PowerWordShield"), t.PowerWordShield, t.WeakenedSoul))

		-- Pain Suppression
		elseif GMR.IsCheckedSpell("PainSuppression")
		and GMR.GetHealthFilter("PainSuppression")
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("PainSuppression")
		and GMR.IsCastable(t.PainSuppression, "player") then
			GMR.Cast(t.PainSuppression, "player")

		elseif GMR.IsCheckedSpell("PainSuppression")
		and GMR.GetHealthFilter("PainSuppression")
		and GMR.GetPartyHealth(GMR.GetHealthFilter("PainSuppression"), t.PainSuppression)
		and GMR.IsCastable(t.PainSuppression, GMR.GetPartyHealth(GMR.GetHealthFilter("PainSuppression"), t.PainSuppression)) then
			GMR.Cast(t.PainSuppression, GMR.GetPartyHealth(GMR.GetHealthFilter("PainSuppression"), t.PainSuppression))

		-- Dispersion
		elseif GMR.IsCheckedSpell("Dispersion")
		and ((GMR.GetHealthFilter("Dispersion")
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("Dispersion"))
		or (GMR.GetManaFilter("Dispersion")
		and GMR.GetMana("player") <= GMR.GetManaFilter("Dispersion")))
		and GMR.IsCastable(t.Dispersion) then
			GMR.Cast(t.Dispersion)

		-- Renew
		elseif GMR.IsCheckedSpell("Renew")
		and GMR.GetHealthFilter("Renew")
		and not GMR.HasBuff("player", t.Renew)
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("Renew")
		and GMR.IsCastable(t.Renew, "player") then
			GMR.Cast(t.Renew, "player")

		elseif GMR.IsCheckedSpell("Renew")
		and GMR.GetHealthFilter("Renew")
		and GMR.GetPartyHealth(GMR.GetHealthFilter("Renew"), t.Renew)
		and not GMR.HasBuff(GMR.GetPartyHealth(GMR.GetHealthFilter("Renew"), t.Renew), t.Renew)
		and GMR.IsCastable(t.Renew, GMR.GetPartyHealth(GMR.GetHealthFilter("Renew"), t.Renew)) then
			GMR.Cast(t.Renew, GMR.GetPartyHealth(GMR.GetHealthFilter("Renew"), t.Renew))

		-- Prayer of Mending
		elseif GMR.IsCheckedSpell("PrayerOfMending")
		and GMR.GetHealthFilter("PrayerOfMending")
		and not GMR.HasBuff("player", t.PrayerOfMending)
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("PrayerOfMending")
		and GMR.IsCastable(t.PrayerOfMending, "player") then
			GMR.Cast(t.PrayerOfMending, "player")

		elseif GMR.IsCheckedSpell("PrayerOfMending")
		and GMR.GetHealthFilter("PrayerOfMending")
		and GMR.GetPartyHealth(GMR.GetHealthFilter("PrayerOfMending"), t.PrayerOfMending)
		and not GMR.HasBuff(GMR.GetPartyHealth(GMR.GetHealthFilter("PrayerOfMending"), t.PrayerOfMending), t.PrayerOfMending)
		and GMR.IsCastable(t.PrayerOfMending, GMR.GetPartyHealth(GMR.GetHealthFilter("PrayerOfMending"), t.PrayerOfMending)) then
			GMR.Cast(t.PrayerOfMending, GMR.GetPartyHealth(GMR.GetHealthFilter("PrayerOfMending"), t.PrayerOfMending))

		-- Wand MaxHealth < 10
		elseif GMR.UnitHealthMax("target") < 10
		and GMR.IsCheckedSpell("Wand")
		and GMR.IsWandUsable()
		and GMR.IsCastable(t.Wand, "target")
		and not GMR.IsMoving() then
			if not GMR.IsWanding()
			and not GMR.GetDelay("CombatRotation") then
				GMR.Cast(t.Wand, "target");
				GMR.SetDelay("CombatRotation", 0.5)
			end

		-- Desperate Prayer
		elseif GMR.IsCheckedSpell("DesperatePrayer")
		and GMR.GetHealthFilter("DesperatePrayer")
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("DesperatePrayer")
		and GMR.IsCastable(t.DesperatePrayer) then
			GMR.Cast(t.DesperatePrayer, "player")

		-- Circle of Healing
		elseif GMR.IsCheckedSpell("CircleOfHealing")
		and GMR.GetHealthFilter("CircleOfHealing")
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("CircleOfHealing")
		and GMR.IsCastable(t.CircleOfHealing, "player") then
			GMR.Cast(t.CircleOfHealing, "player")

		elseif GMR.IsCheckedSpell("CircleOfHealing")
		and GMR.GetHealthFilter("CircleOfHealing")
		and GMR.GetPartyHealth(GMR.GetHealthFilter("CircleOfHealing"), t.CircleOfHealing)
		and GMR.IsCastable(t.CircleOfHealing, GMR.GetPartyHealth(GMR.GetHealthFilter("CircleOfHealing"), t.CircleOfHealing)) then
			GMR.Cast(t.CircleOfHealing, GMR.GetPartyHealth(GMR.GetHealthFilter("CircleOfHealing"), t.CircleOfHealing))

		-- Lightwell
		elseif GMR.IsCheckedSpell("Lightwell")
		and GMR.GetHealthFilter("Lightwell")
		and GMR.GetUnitFilter("Lightwell")
		and GMR.GetPartyHealth(GMR.GetHealthFilter("Lightwell"), t.Lightwell)
		and GMR.GetNumFriends(GMR.GetPartyHealth(GMR.GetHealthFilter("Lightwell"), t.Lightwell), 10) >= GMR.GetUnitFilter("Lightwell")
		and GMR.IsAoECastable(t.Lightwell, GMR.GetPartyHealth(GMR.GetHealthFilter("Lightwell"), t.Lightwell)) then
			GMR.CastAoE(t.Lightwell, GMR.GetPartyHealth(GMR.GetHealthFilter("Lightwell"), t.Lightwell))

		-- Psychic Horror
		elseif GMR.IsCheckedSpell("PsychicHorror")
		and ((GMR.GetHealthFilter("PsychicHorror")
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("PsychicHorror"))
		or (GMR.GetUnitFilter("PsychicHorror")
		and GMR.GetNumAttackingEnemies("player", 6.5) >= GMR.GetUnitFilter("PsychicHorror")))
		and GMR.IsCastable(t.PsychicHorror, "target") then
			GMR.Cast(t.PsychicHorror, "target")

		-- Psychic Scream
		elseif GMR.IsCheckedSpell("PsychicScream")
		and ((GMR.GetHealthFilter("PsychicScream")
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("PsychicScream"))
		or (GMR.GetUnitFilter("PsychicScream")
		and GMR.GetNumAttackingEnemies("player", 6.5) >= GMR.GetUnitFilter("PsychicScream")))
		and GMR.IsCastable(t.PsychicScream, "target") then
			GMR.Cast(t.PsychicScream, "target")

		-- Guardian Spirit
		elseif GMR.IsCheckedSpell("GuardianSpirit")
		and GMR.GetHealthFilter("GuardianSpirit")
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("GuardianSpirit")
		and GMR.IsCastable(t.GuardianSpirit, "player") then
			GMR.Cast(t.GuardianSpirit, "player")

		elseif GMR.IsCheckedSpell("GuardianSpirit")
		and GMR.GetHealthFilter("GuardianSpirit")
		and GMR.GetPartyHealth(GMR.GetHealthFilter("GuardianSpirit"), t.GuardianSpirit)
		and GMR.IsCastable(t.GuardianSpirit, GMR.GetPartyHealth(GMR.GetHealthFilter("GuardianSpirit"), t.GuardianSpirit)) then
			GMR.Cast(t.GuardianSpirit, GMR.GetPartyHealth(GMR.GetHealthFilter("GuardianSpirit"), t.GuardianSpirit))

		-- Power Infusion
		elseif GMR.IsCheckedSpell("PowerInfusion")
		and GMR.GetHealthFilter("PowerInfusion")
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("PowerInfusion")
		and GMR.IsCastable(t.PowerInfusion, "player") then
			GMR.Cast(t.PowerInfusion, "player")

		elseif GMR.IsCheckedSpell("PowerInfusion")
		and GMR.GetHealthFilter("PowerInfusion")
		and GMR.GetPartyHealth(GMR.GetHealthFilter("PowerInfusion"), t.PowerInfusion)
		and GMR.IsCastable(t.PowerInfusion) then
			GMR.Cast(t.PowerInfusion, "player")

		-- Inner Focus
		elseif GMR.IsCheckedSpell("InnerFocus")
		and GMR.GetManaFilter("InnerFocus")
		and GMR.GetMana("player") <= GMR.GetManaFilter("InnerFocus")
		and GMR.IsCastable(t.InnerFocus) then
			GMR.Cast(t.InnerFocus)

		-- Penance
		elseif GMR.IsCheckedSpell("Penance")
		and GMR.GetHealthFilter("Penance")
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("Penance")
		and GMR.IsCastable(t.Penance, "player")
		and not GMR.IsMoving() then
			GMR.Cast(t.Penance, "player")

		elseif GMR.IsCheckedSpell("Penance")
		and GMR.GetHealthFilter("Penance")
		and GMR.GetPartyHealth(GMR.GetHealthFilter("Penance"), t.Penance)
		and GMR.IsCastable(t.Penance, GMR.GetPartyHealth(GMR.GetHealthFilter("Penance"), t.Penance))
		and not GMR.IsMoving() then
			GMR.Cast(t.Penance, GMR.GetPartyHealth(GMR.GetHealthFilter("Penance"), t.Penance))

		-- Binding Heal
		elseif GMR.IsCheckedSpell("BindingHeal ")
		and GMR.GetHealthFilter("BindingHeal")
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("BindingHeal")
		and GMR.GetPartyHealth(GMR.GetHealthFilter("BindingHeal"), t.BindingHeal)
		and GMR.IsCastable(t.BindingHeal, GMR.GetPartyHealth(GMR.GetHealthFilter("BindingHeal"), t.BindingHeal))
		and not GMR.IsMoving() then
			GMR.Cast(t.BindingHeal, GMR.GetPartyHealth(GMR.GetHealthFilter("BindingHeal"), t.BindingHeal))

		-- Greater Heal
		elseif GMR.IsCheckedSpell("GreaterHeal")
		and GMR.GetHealthFilter("GreaterHeal")
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("GreaterHeal")
		and GMR.IsCastable(t.GreaterHeal, "player")
		and not GMR.IsMoving() then
			GMR.Cast(t.GreaterHeal, "player")

		elseif GMR.IsCheckedSpell("GreaterHeal")
		and GMR.GetHealthFilter("GreaterHeal")
		and GMR.GetPartyHealth(GMR.GetHealthFilter("GreaterHeal"), t.GreaterHeal)
		and GMR.IsCastable(t.GreaterHeal, GMR.GetPartyHealth(GMR.GetHealthFilter("GreaterHeal"), t.GreaterHeal))
		and not GMR.IsMoving() then
			GMR.Cast(t.GreaterHeal, GMR.GetPartyHealth(GMR.GetHealthFilter("GreaterHeal"), t.GreaterHeal))

		-- Heal
		elseif GMR.IsCheckedSpell("Heal")
		and GMR.GetHealthFilter("Heal")
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("Heal")
		and GMR.IsCastable(t.Heal, "player")
		and not GMR.IsMoving() then
			GMR.Cast(t.Heal, "player")

		elseif GMR.IsCheckedSpell("Heal")
		and GMR.GetHealthFilter("Heal")
		and GMR.GetPartyHealth(GMR.GetHealthFilter("Heal"), t.Heal)
		and GMR.IsCastable(t.Heal, GMR.GetPartyHealth(GMR.GetHealthFilter("Heal"), t.Heal))
		and not GMR.IsMoving() then
			GMR.Cast(t.Heal, GMR.GetPartyHealth(GMR.GetHealthFilter("Heal"), t.Heal))

		-- Lesser Heal
		elseif GMR.IsCheckedSpell("LesserHeal")
		and GMR.GetHealthFilter("LesserHeal")
		and GMR.GetHealth("player") <= GMR.GetHealthFilter("LesserHeal")
		and GMR.IsCastable(t.LesserHeal, "player")
		and not GMR.IsMoving() then
			GMR.Cast(t.LesserHeal, "player")

		elseif GMR.IsCheckedSpell("LesserHeal")
		and GMR.GetHealthFilter("LesserHeal")
		and GMR.GetPartyHealth(GMR.GetHealthFilter("LesserHeal"), t.LesserHeal)
		and GMR.IsCastable(t.LesserHeal, GMR.GetPartyHealth(GMR.GetHealthFilter("LesserHeal"), t.LesserHeal))
		and not GMR.IsMoving() then
			GMR.Cast(t.LesserHeal, GMR.GetPartyHealth(GMR.GetHealthFilter("LesserHeal"), t.LesserHeal))

		-- Inner Fire
		elseif GMR.IsCheckedSpell("InnerFire")
		and not GMR.HasBuff("player", t.InnerFire)
		and GMR.IsCastable(t.InnerFire) then
			GMR.Cast(t.InnerFire)

		-- Dispel Magic
		elseif GMR.IsCheckedSpell("DispelMagic")
		and GMR.IsCleansable("DispelMagic", "Magic")
		and GMR.IsCastable(t.DispelMagic) then
			GMR.Cast(t.DispelMagic, "player")

		-- Shadowform
		elseif GMR.IsCheckedSpell("Shadowform")
		and not GMR.HasBuff("player", t.Shadowform)
		and GMR.IsCastable(t.Shadowform) then
			GMR.Cast(t.Shadowform)
			
		end	
		
		-- Vampiric Touch
		if GMR.IsCheckedSpell("VampiricTouch") then
			for i = 1, #GMR.Tables.Attackables do
				local attackable = GMR.Tables.Attackables[i][1]
				if GMR.ObjectExists(attackable)
					and GMR.UnitHealthMax(attackable) > 10
					and GMR.InCombat()
					and GMR.GetHealthFilter("VampiricTouch")
					and GMR.GetHealth(attackable) >= GMR.GetHealthFilter("VampiricTouch")
					and GMR.GetManaFilter("VampiricTouch")
					and GMR.GetDebuffExpiration(attackable, t.VampiricTouch, true) < 3
					and GMR.GetMana("player") >= GMR.GetManaFilter("VampiricTouch")
					and GMR.GetHealth(attackable) > 10
					and GMR.GetMana("player") >= 30
					and GMR.IsCastable(t.VampiricTouch, attackable) then
					if #dotted_units["VampiricTouch"] < GMR.GetPullCount() then
						GMR.Cast(t.VampiricTouch, attackable)
					end
				end
			end
		end
		
		-- SWP
		if GMR.IsCheckedSpell("ShadowWordPain") then
			for i = 1, #GMR.Tables.Attackables do
				local attackable = GMR.Tables.Attackables[i][1]
				if GMR.ObjectExists(attackable)
					and GMR.UnitHealthMax(attackable) > 10
					and GMR.InCombat()
					and GMR.GetHealthFilter("ShadowWordPain")
					and GMR.GetHealth(attackable) >= GMR.GetHealthFilter("ShadowWordPain")
					and GMR.GetManaFilter("ShadowWordPain")
					and GMR.GetDebuffExpiration(attackable, t.ShadowWordPain, true) < 1
					and GMR.GetMana("player") >= GMR.GetManaFilter("ShadowWordPain")
					and GMR.GetMana("player") >= 30
					and GMR.IsCastable(t.ShadowWordPain, attackable) then
					if #dotted_units["ShadowWordPain"] < GMR.GetPullCount() then
						GMR.Cast(t.ShadowWordPain, attackable)
					end
				end
			end
		end
		
		-- VT @target
		if GMR.IsCheckedSpell("VampiricTouch")
			and GMR.UnitHealthMax("target") > 10 
			and GMR.InCombat()
			and GMR.GetHealthFilter("VampiricTouch")
			and GMR.GetHealth("target") >= GMR.GetHealthFilter("VampiricTouch")
			and GMR.GetManaFilter("VampiricTouch")
			and GMR.GetDebuffExpiration("target", t.VampiricTouch, true) < 3
			and GMR.GetMana("player") >= GMR.GetManaFilter("VampiricTouch")
			and GMR.GetHealth("target") > 10
			and GMR.IsCastable(t.VampiricTouch, "target") then
				GMR.Cast(t.VampiricTouch, "target")
		end
		
		-- SWP @target
		if GMR.IsCheckedSpell("ShadowWordPain")
			and GMR.UnitHealthMax("target") > 10 
			and GMR.InCombat()
			and GMR.GetHealthFilter("ShadowWordPain")
			and GMR.GetHealth("target") >= GMR.GetHealthFilter("ShadowWordPain")
			and GMR.GetManaFilter("ShadowWordPain")
			and GMR.GetDebuffExpiration("target", t.ShadowWordPain, true) < 1
			and GMR.GetMana("player") >= GMR.GetManaFilter("ShadowWordPain")
			and GMR.GetHealth("target") > 10
			and GMR.IsCastable(t.ShadowWordPain, "target") then
				GMR.Cast(t.ShadowWordPain, "target")
		end
		
			-- Devouring Plague
		if GMR.IsCheckedSpell("DevouringPlague")
		and GMR.UnitHealthMax("target") > 10
		and GMR.GetManaFilter("DevouringPlague")
		and GMR.GetMana("player") >= GMR.GetManaFilter("DevouringPlague")
		and GMR.GetHealthFilter("DevouringPlague")
		and GMR.GetHealth("target") >= GMR.GetHealthFilter("DevouringPlague")
		and GMR.IsCastable(t.DevouringPlague, "target")
		and GMR.GetDebuffExpiration("target", t.DevouringPlague, true) < 1
		and not GMR.IsMoving() then
			GMR.Cast(t.DevouringPlague, "target")
		
	--[[
		-- Vampiric Touch
		elseif GMR.IsCheckedSpell("VampiricTouch")
		and GMR.GetManaFilter("VampiricTouch")
		and GMR.GetMana("player") >= GMR.GetManaFilter("VampiricTouch")
		and GMR.GetHealthFilter("VampiricTouch")
		and GMR.GetHealth("target") >= GMR.GetHealthFilter("VampiricTouch")
		and GMR.GetDebuffExpiration("target", t.VampiricTouch, true) <= 3
		and GMR.IsCastable(t.VampiricTouch, "target")
		and not GMR.IsMoving() then
			GMR.Cast(t.VampiricTouch, "target")
	--]]
		-- Holy Fire
		elseif GMR.IsCheckedSpell("HolyFire")
		and GMR.GetManaFilter("HolyFire")
		and GMR.GetMana("player") >= GMR.GetManaFilter("HolyFire")
		and GMR.IsCastable(t.HolyFire, "target")
		and not GMR.IsMoving() then
			GMR.Cast(t.HolyFire, "target")

		-- Smite I
		elseif GMR.IsCheckedSpell("Smite")
		and GMR.GetManaFilter("Smite")
		and GMR.GetMana("player") >= GMR.GetManaFilter("Smite")
		and GMR.IsCastable(t.Smite, "target")
		and not GMR.IsMoving()
		and not GMR.InCombat("target") then
			GMR.Cast(t.Smite, "target")

	--[[
		-- Shadow Word: Pain
		elseif GMR.IsCheckedSpell("ShadowWordPain")
		and GMR.GetManaFilter("ShadowWordPain")
		and GMR.GetMana("player") >= GMR.GetManaFilter("ShadowWordPain")
		and GMR.GetHealthFilter("ShadowWordPain")
		and GMR.GetHealth("target") >= GMR.GetHealthFilter("ShadowWordPain")
		and GMR.IsCastable(t.ShadowWordPain, "target")
		and GMR.GetDebuffExpiration("target", t.ShadowWordPain, true) <= 3
		and not GMR.IsMoving() then
			GMR.Cast(t.ShadowWordPain, "target")
	--]]
		-- Shadowfiend
		elseif GMR.IsCheckedSpell("Shadowfiend")
		and ((GMR.GetManaFilter("Shadowfiend")
		and GMR.GetMana("player") <= GMR.GetManaFilter("Shadowfiend"))
		or (GMR.GetUnitFilter("Shadowfiend")
		and GMR.GetNumEnemies("player", 6.5) >= GMR.GetUnitFilter("Shadowfiend")))
		and GMR.IsCastable(t.Shadowfiend, "target")
		and not GMR.IsMoving() then
			GMR.Cast(t.Shadowfiend, "target")

		-- Mind Blast
		elseif GMR.IsCheckedSpell("MindBlast")
		and GMR.UnitHealthMax("target") > 10
		and GMR.GetManaFilter("MindBlast")
		and GMR.GetMana("player") >= GMR.GetManaFilter("MindBlast")
		and GMR.IsCastable(t.MindBlast, "target")
		and not GMR.IsMoving() then
			GMR.Cast(t.MindBlast, "target")

				 -- Shadow Word: Death
		elseif GMR.IsCheckedSpell("ShadowWordDeath")
		and GMR.UnitHealthMax("target") > 10
		and GMR.GetManaFilter("ShadowWordDeath")
		and GMR.GetMana("player") >= GMR.GetManaFilter("ShadowWordDeath")
		and GMR.IsCastable(t.ShadowWordDeath, "target")
		and GMR.GetHealth("target") < 20
		and not GMR.IsMoving() then
			GMR.Cast(t.ShadowWordDeath, "target")
	
		-- Mind Sear
		elseif GMR.IsCheckedSpell("MindSear")
		and GMR.UnitHealthMax("target") > 10
		and GMR.GetManaFilter("MindSear")
		and GMR.GetMana("player") >= GMR.GetManaFilter("MindSear")
		and GMR.GetUnitFilter("MindSear")
		and GMR.GetNumEnemies("target", 6.5) >= GMR.GetUnitFilter("MindSear")
		and GMR.IsCastable(t.MindSear, "target")
		and not GMR.IsMoving() then
			GMR.Cast(t.MindSear, "target")
	
		-- Mind Flay
		elseif GMR.IsCheckedSpell("MindFlay")
		and GMR.UnitHealthMax("target") > 10
		and GMR.GetManaFilter("MindFlay")
		and GMR.GetMana("player") >= GMR.GetManaFilter("MindFlay")
		and GMR.IsCastable(t.MindFlay, "target")
		and not GMR.IsMoving() then
			GMR.Cast(t.MindFlay, "target")

		-- Smite II
		elseif GMR.IsCheckedSpell("Smite")
		and GMR.GetManaFilter("Smite")
		and GMR.GetMana("player") >= GMR.GetManaFilter("Smite")
		and GMR.IsCastable(t.Smite, "target")
		and not GMR.IsMoving() then
			GMR.Cast(t.Smite, "target")

		-- Wand
		elseif GMR.IsCheckedSpell("Wand")
		and GMR.IsWandUsable()
		and GMR.IsCastable(t.Wand, "target")
		and not GMR.IsMoving() then
			if not GMR.IsWanding()
			and not GMR.GetDelay("CombatRotation") then
				GMR.Cast(t.Wand, "target");
				GMR.SetDelay("CombatRotation", 0.5)
			end
		end
	end
end
