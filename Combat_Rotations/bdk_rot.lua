C_Timer.NewTicker(0.1, function()
	local class = GMR.GetClass("player")
 if  class == "DEATHKNIGHT" then
 	--spell IDs
	local plagueStrike = GetSpellInfo(45462)
	local bloodPlagueSpell = GetSpellInfo(55078) -- plagues strike's debuff
	local icyTouch = GetSpellInfo(45477)
	local frostFeverSpell = GetSpellInfo(55095) -- icy touch's debuff
	local pestilence = GetSpellInfo(50842)
	local deathStrike = GetSpellInfo(49998)
	local bloodStrike = GetSpellInfo(45902)
	local deathCoil = GetSpellInfo(49893)
	local hornOfWinter = GetSpellInfo(57330)
	local IBF = GetSpellInfo(48792)
	local deathPact = GetSpellInfo(48743)
	local raiseDead = GetSpellInfo(46584)
	local CE = GetSpellInfo(51328)
	local BBBuff = GetSpellInfo(64859)
	local deathGrip = GetSpellInfo(49576)
	local AMS = GetSpellInfo(48707)
	local taunt = GetSpellInfo(56222)
	local bloodTap = GetSpellInfo(45529)
	local ERW = GetSpellInfo(47568)
	local gnaw = GetSpellInfo(47481)
	local mindFreeze = GetSpellInfo(47528)
	
  if not mounted and GMR.IsExecuting() and not GMR.HasBuff("player", 32240) then
	
	if GMR.IsCastable(bloodTap) and GMR.InCombat("player") then
		C_Timer.After(3, function() 
			if GMR.InCombat("player") then 
				GMR.CastSpellByName(bloodTap)
			end
		end)
	end	
	
	if GMR.IsCastable(mindFreeze) and GMR.IsInterruptable("target") and GMR.UnitCastingTime("target", 2.5) then
	  GMR.CastSpellByName(mindFreeze)
	end
	
	if GMR.IsCastable(gnaw) and GMR.IsInterruptable("target") and GMR.UnitCastingTime("target", 2.5) and not GMR.IsCastable(mindFreeze) then
	  GMR.CastSpellByName(gnaw)
	end
	
	if GMR.IsCastable(ERW) and GMR.GetHealth("player") <= 45 and not GMR.IsCastable(deathStrike) then
		GMR.CastSpellByName(ERW)
	end
	
	if (GMR.IsCastable(hornOfWinter) and GMR.InCombat("player")) or (GMR.IsCastable(hornOfWinter) and not GMR.HasBuff("player", hornOfWinter)) then
		GMR.CastSpellByName(hornOfWinter)
	end
	
	if GMR.IsCastable(raiseDead) and not GMR.PlayerHasPet() == true 
	and (GetZoneText() == "The Botanica" and not GMR.IsPlayerPosition(142.30613708496, 299.81820678711, -3.8390989303589, 50)) then
		GMR.CastSpellByName(raiseDead)
	end
	
	if GetZoneText() == "The Botanica" and GMR.IsPlayerPosition(142.30613708496, 299.81820678711, -3.8390989303589, 50) and GMR.PlayerHasPet() == true then
		GMR.RunMacroText("/petdismiss")
	end
	
	if GMR.GetHealth("player") < 60 and GMR.IsCastable(IBF) then
		GMR.CastSpellByName(IBF)
	end
	
	if GMR.IsCastable(deathPact) and GMR.PlayerHasPet() == true and GMR.GetHealth("player") < 50 then
		GMR.CastSpellByName(deathPact)	
	end
	
	if GMR.IsCastable(AMS) and GMR.GetHealth("player") < 70 then
		GMR.CastSpellByName(AMS)
	end
	
	if GMR.IsCastable(deathGrip) and GMR.GetDistance("player", "target", ">", 11) and not GMR.UnitIsPlayer("target") then
		GMR.CastSpellByName(deathGrip)
	end
	
	if GMR.IsCastable(taunt) and GMR.GetDistance("player", "target", ">", 11) and not GMR.UnitIsPlayer("target") and not GMR.InCombat("target") then
		GMR.CastSpellByName(taunt)
	end
	
	if GMR.InLoS("target") and UnitExists("target") and GMR.GetHealth("target") > 0 and not GMR.UnitIsPlayer("target") then
		mobcount = GMR.GetNumAttackingEnemies("player", 10)
		local targetBloodPlagueAuraDuration = (select(6, AuraUtil.FindAuraByName(bloodPlagueSpell, "target", "HARMFUL|PLAYER")) or 0) - GetTime()
		local targetFrostFeverAuraDuration = (select(6, AuraUtil.FindAuraByName(frostFeverSpell, "target", "HARMFUL|PLAYER")) or 0) - GetTime()
	
		--icy touch
		if GMR.IsCastable(icyTouch, "target") and not GMR.HasDebuff("target", frostFeverSpell) then
			GMR.CastSpellByName(icyTouch)
		end
	
		-- plague strike
		if GMR.IsCastable(plagueStrike, "target") and not GMR.HasDebuff("target", bloodPlagueSpell) then
			GMR.CastSpellByName(plagueStrike)
			GMR.PetAttack()
		end
	
		--death strike
		if GMR.IsCastable(deathStrike) and (targetBloodPlagueAuraDuration > 5 or targetFrostFeverAuraDuration > 5) then
			GMR.CastSpellByName(deathStrike)
			GMR.PetAttack()
		end
	
		--pestilence
		local enemiesToTransferDebuff = 0
		local enemiesWithDebuff = 0
		for i = 1, #GMR.Tables.Attackables do
			local attackable = GMR.Tables.Attackables[i][1]
			if GMR.ObjectExists(attackable) and GMR.GetDistance("player", attackable, "<", 10) then
				local attackableBloodPlagueAuraDuration = (select(6, AuraUtil.FindAuraByName(bloodPlagueSpell, attackable, "HARMFUL|PLAYER")) or 0) - GetTime()
				local attackableFrostFeverAuraDuration = (select(6, AuraUtil.FindAuraByName(frostFeverSpell, attackable, "HARMFUL|PLAYER")) or 0) - GetTime()
				if attackableBloodPlagueAuraDuration <= 5 or attackableFrostFeverAuraDuration <= 5 then
					enemiesToTransferDebuff = enemiesToTransferDebuff + 1
				elseif attackableBloodPlagueAuraDuration > 5 and attackableFrostFeverAuraDuration > 5 then
					enemiesWithDebuff = enemiesWithDebuff + 1
				end
			end
		end
	
		local needToCastPestilenceSpell = false
		if targetBloodPlagueAuraDuration > 5 and targetFrostFeverAuraDuration > 5 and enemiesToTransferDebuff > 0 then
			needToCastPestilenceSpell = true
		end
	
		if (needToCastPestilenceSpell and GMR.IsCastable(pestilence)) or 
		(GMR.HasDebuff("target", frostFeverSpell) and GMR.HasDebuff("target", bloodPlagueSpell) and not GMR.HasBuff("player", BBBuff)) then
			GMR.CastSpellByName(pestilence)
			GMR.PetAttack()
		end
		
		if GMR.IsCastable(CE) and mobcount >= 3 then
			if GMR.PlayerHasPet() == true and GMR.GetHealth("player") > 50 then
				GMR.CastSpellByName(CE, "pet")
			end
			
			if not GMR.PlayerHasPet() == true and not GMR.IsCastable(raiseDead) then
				GMR.CastSpellByName(CE)
			end
		elseif GMR.IsCastable(deathCoil) and ((GMR.UnitPower("player") >= 60 and mobcount >= 3) or mobcount < 3) then
			GMR.CastSpellByName(deathCoil)
		end
	end
  end
  end
 end)