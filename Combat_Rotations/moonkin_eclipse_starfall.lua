C_Timer.NewTicker(0.1, function()
	if UnitClass("player") == "Monk" then 

		-- Spells
     tigerpalm = GetSpellInfo(100780)
     blackoutkick = GetSpellInfo(100784)
     risingsunkick = GetSpellInfo(107428)
     fistsoffury = GetSpellInfo(113656)
     spinningcranekick = GetSpellInfo(101546)
     strikeoftheWindlord = GetSpellInfo(392983)
     touchofDeath = GetSpellInfo(322109)
	 vivifyHEAL = GetSpellInfo(116670)
	 chiWave = GetSpellInfo(115098)
	 stormEarthFireCD = GetSpellInfo(137639)
	 invokeAXuenCD = GetSpellInfo(123904)
	 fortifyingBrewDefensive = GetSpellInfo(115203)
	 

	 local palmBuff = GetSpellInfo(129914)
	 local risingSunKickBuff = GetSpellInfo(202090)
	 local spinningCraneKickBuff = GetSpellInfo(325202)






		if not mounted and GMR.IsExecuting() and GMR.InLoS("target") and UnitExists("target") then


			if GMR.InCombat("player") then
				-- Storm Earth and Fire - Cooldowns
				if GMR.GetNumAttackingEnemies("player", 15) >= 2 then
					if GMR.GetHealth("player") < 60 then
						if GMR.GetSpellCooldown(116670) == 0 then
							if GMR.UnitPower("player") >= 30 then
								if GetSpellInfo(vivifyHEAL) and IsUsableSpell(vivifyHEAL) and GMR.IsCastable(vivifyHEAL) then
									GMR.Cast(vivifyHEAL)
								end
							end
						end
					else
						-- fortifying brrew
						if GMR.GetSpellCooldown(115203) == 0 then
							if GetSpellInfo(fortifyingBrewDefensive) and IsUsableSpell(fortifyingBrewDefensive) and GMR.IsCastable(fortifyingBrewDefensive) and GMR.GetHealth("target") > 20 then
								GMR.Cast(fortifyingBrewDefensive)
							end
						-- storm earth fire
						elseif GMR.GetSpellCooldown(137639) == 0 then
							if GetSpellInfo(stormEarthFireCD) and IsUsableSpell(stormEarthFireCD) and GMR.IsCastable(stormEarthFireCD, "target") and GMR.GetHealth("target") > 20 then
								GMR.Cast(stormEarthFireCD)
							end
						elseif GMR.GetSpellCooldown(123904) == 0 then
							if GetSpellInfo(invokeAXuenCD) and IsUsableSpell(invokeAXuenCD) and GMR.IsCastable(invokeAXuenCD, "target") and GMR.GetHealth("target") > 20 then
								GMR.Cast(invokeAXuenCD)
							end
						elseif GMR.GetSpellCooldown(115098) == 0 then
							if GetSpellInfo(chiWave) and IsUsableSpell(chiWave) and GMR.IsCastable(chiWave, "target") and GMR.GetHealth("target") > 20 then
								GMR.Cast(chiWave)
							end
							-- spinning crane kick
						elseif GMR.HasBuff("player", spinningCraneKickBuff) then
							if GMR.GetSpellCooldown(101546) == 0 then
								if GetSpellInfo(spinningcranekick) and IsUsableSpell(spinningcranekick) and GMR.IsCastable(spinningcranekick, "target") and GMR.GetHealth("target") > 10 then
									GMR.Cast(spinningcranekick)
								end
							end
						else
							-- fists of fury
							if GetSpellInfo(fistsoffury) and IsUsableSpell(fistsoffury) and GMR.IsCastable(fistsoffury, "target") and GetSpellCooldown(fistsoffury) == 0 and GMR.GetHealth("target") > 15 
							and GMR.GetDistance("player", "target", "<", 19) then
								GMR.Cast(fistsoffury)
							end
						end
					end
				else
					-- single target rotatio
					if GMR.HasBuff("player", risingSunKickBuff) then
						-- rising Sun Kick
						if GetSpellInfo(risingsunkick) and IsUsableSpell(risingsunkick) and GMR.IsCastable(risingsunkick, "target") and GetSpellCooldown(risingsunkick) == 0 and GMR.GetHealth("target") > 15 
						and GMR.GetDistance("player", "target", "<", 19) then
							GMR.Cast(risingsunkick)
						end
					elseif GMR.HasBuff("player", spinningCraneKickBuff) then
						if GetSpellInfo(spinningcranekick) and IsUsableSpell(spinningcranekick) and GMR.IsCastable(spinningcranekick) and GMR.GetHealth("target") > 10 then
							GMR.Cast(spinningcranekick)
						end
					else
						-- strikeoftheWindlord
						if GetSpellInfo(strikeoftheWindlord) and IsUsableSpell(strikeoftheWindlord) and GMR.IsCastable(strikeoftheWindlord, "target") and GetSpellCooldown(strikeoftheWindlord) == 0 and GMR.GetHealth("target") > 15 
						and GMR.GetDistance("player", "target", "<", 19) then
							GMR.Cast(strikeoftheWindlord)
						end
						-- chi wave
						if GMR.GetSpellCooldown(115098) == 0 then
							if GetSpellInfo(chiWave) and IsUsableSpell(chiWave) and GMR.IsCastable(chiWave, "target") and GMR.GetHealth("target") > 20 then
								GMR.Cast(chiWave)
							end
						end
						-- tigerpalm
						if GetSpellInfo(tigerpalm) and IsUsableSpell(tigerpalm) and GMR.IsCastable(tigerpalm, "target") and GetSpellCooldown(tigerpalm) == 0 and GMR.GetHealth("target") > 15 
						and GMR.GetDistance("player", "target", "<", 19) then
							GMR.Cast(tigerpalm)
						end
						-- blackout kick
						if GetSpellInfo(blackoutkick) and IsUsableSpell(blackoutkick) and GMR.IsCastable(blackoutkick, "target") and GetSpellCooldown(blackoutkick) == 0 and GMR.GetHealth("target") > 15 
						and GMR.GetDistance("player", "target", "<", 19) then
							GMR.Cast(typhoon)
						end
					end
				end
			end
		end
	end
end)