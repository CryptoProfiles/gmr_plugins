if UnitClass("player") == "Rogue" then
  sliceAndDiceMinimumComboPoints = 2
  sliceAndDiceHealthTargetAbove = 50
  killingSpreeHealthTargetAbove = 80
  killingSpreePlayerHealthBellow = 15

  eviscerateModifer = 0.2  -- A higher value means it will cast Eviscerate earlier, if you assign negative value it will cast Eviscerate later.

  ruptureTargetHealthAbove = 55
  ruptureMinimumComboPoints = 4

  stealthDistance = 16  -- yards
  sprintDistance = 25  -- yards

  thistleTeaMinimumPlayerEnergy = 40
  thistleTeaMinimumPlayerHealth = 30
  thistleTeaMinimumTargetHealth = 60

  sinisterStrike = GetSpellInfo(46558)
  sliceAndDice = GetSpellInfo(30470)
  eviscerate = GetSpellInfo(41177)
  rupture = GetSpellInfo(15583)
  cheapShot = GetSpellInfo(1833)
  killingSpree = GetSpellInfo(51690)
  stealth = GetSpellInfo(1787)
  sprint = GetSpellInfo(61922)
  thistleTea = "Thistle Tea"

  C_Timer.NewTicker(0.1, function()
    if GetSpellInfo(sprint) and IsUsableSpell(sprint) and GetSpellCooldown(sprint) == 0 and
    UnitExists("target") and GMR.GetHealth("target") > 0 and not mounted and 
    GMR.GetDistance("player", "target", ">", sprintDistance) then
      GMR.Cast(sprint)
    end
    if not mounted and GMR.IsExecuting() and GMR.InLoS("target") and UnitExists("target") then
      if UnitAffectingCombat("player") then
        -- Use Thistle Tea
        if IsUsableItem(thistleTea) and UnitPower("player") <= thistleTeaMinimumPlayerEnergy and
        (GMR.GetHealth("player") <= thistleTeaMinimumPlayerHealth and 
        GMR.GetHealth("player") >= thistleTeaMinimumTargetHealth) then
          GMR.Use(thistleTea)
          -- print("Use Thistle Tea!")
      end
        -- Killing Spree
        if GetSpellInfo(killingSpree) and IsUsableSpell(killingSpree) and 
        GetSpellCooldown(killingSpree) == 0 and GMR.GetDistance("player", "target", "<", 10) and
        (GMR.GetHealth("target") >= killingSpreeHealthTargetAbove or GMR.GetHealth("player") <= killingSpreePlayerHealthBellow) and not
        AuraUtil.FindAuraByName("Blade Flurry", "player", "HELPFUL") then
          GMR.Cast(killingSpree)
          -- print("Killing Spree!")
        end
        -- Slice and Dice
        if GetSpellInfo(sliceAndDice) and IsUsableSpell(sliceAndDice)
        and GMR.GetHealth("target") >= sliceAndDiceHealthTargetAbove and not 
        AuraUtil.FindAuraByName(sliceAndDice, "player", "HELPFUL") and GetComboPoints("player", "target") >= sliceAndDiceMinimumComboPoints then
          GMR.Cast(sliceAndDice)
          -- print("Slice and Dice!")
        end
        -- Rupture
        if GetSpellInfo(rupture) and IsUsableSpell(rupture) and
        GetComboPoints("player", "target") == ruptureMinimumComboPoints and GMR.GetHealth("target") > ruptureTargetHealthAbove then
            GMR.Cast(rupture)
            -- print("Rupture!")
        elseif GetSpellInfo(eviscerate) and IsUsableSpell(eviscerate) and
        GetComboPoints("player", "target") >= GMR.GetHealth("target") / 10 - eviscerateModifer then
          GMR.Cast(eviscerate)
          -- print("Eviscerate!")
        elseif GetSpellInfo(sinisterStrike) and IsUsableSpell(sinisterStrike) and GetComboPoints("player", "target") <= 5 then
          GMR.Cast(sinisterStrike)
          -- print("Sinister Strike!")
        end
      elseif not mounted and GetSpellInfo(stealth) and IsUsableSpell(stealth) and GetSpellCooldown(stealth) == 0 and 
      UnitCanAttack("player", "target") and GMR.GetDistance("player", "target", "<", stealthDistance) and not
      AuraUtil.FindAuraByName(stealth, "player", "HELPFUL") and GMR.GetHealth("target") > 0 then
        GMR.Cast(stealth)
      elseif GetSpellInfo(cheapShot) and IsUsableSpell(cheapShot) and
      AuraUtil.FindAuraByName(stealth, "player", "HELPFUL") and GMR.GetDistance("player", "target", "<", 6) then
        GMR.Cast(cheapShot)
      end
    end
  end)
end