C_Timer.NewTicker(0.1, function()
	local class = GMR.GetClass("player")
	if class == "DRUID" and GMR.IsCastable(48564) then 
	local mangle = GetSpellInfo(48564)
	local swipe = GetSpellInfo(48562)
	local maul = GetSpellInfo(48480)
	local SI = GetSpellInfo(61336)
	local barkskin = GetSpellInfo(22812)
	local FR = GetSpellInfo(22842)
	local enrage = GetSpellInfo(5229)
	local HT = GetSpellInfo(5185)
	local RJ = GetSpellInfo(774)
	local regrowth = GetSpellInfo(8936)
	local mark = GetSpellInfo(1126)
	local thorns = GetSpellInfo(467)
	local bear = GetSpellInfo(5487)
	local direBear = GetSpellInfo(9634)
	local lacerate = GetSpellInfo(48568)
	local FF = GetSpellInfo(16857)
	local innervate = GetSpellInfo(29166)
	local FCB = GetSpellInfo(16979)
	local prowl2 = GetSpellInfo(5215)
	local lifebloom = GetSpellInfo(33763)
	local bash = GetSpellInfo(8983)
	local demo = GetSpellInfo(48560)
	local catForm2 = GetSpellInfo(768)
	local lacerateDuration = (select(6, AuraUtil.FindAuraByName(lacerate, "target", "HARMFUL|PLAYER")) or 0) - GetTime()
	local dash = GetSpellInfo(33357)
	
	if GMR.IsExecuting() and GMR.HasBuff("player", catForm2) and GMR.IsCastable(dash) and not GMR.InCombat("player") then
		GMR.CastSpellByName(dash)
	end
	
	if not mounted and not GMR.IsDrinking("player") and GMR.IsExecuting() and not GMR.HasBuff("player", prowl2) then
	
		if GMR.GetHealth("player") > 95 and not GMR.InCombat("player") then
			--mark of the wild
			if GMR.IsCastable(mark) and not GMR.HasBuff("player", mark) then
				GMR.CastSpellByName(mark)
			end
		
			--thorns
			if GMR.IsCastable(thorns) and not GMR.HasBuff("player", thorns) then
				GMR.CastSpellByName(thorns)
			end
		
			--innervate
			if GMR.IsCastable(innervate) and GMR.GetMana("player") <= 40 then
				GMR.CastSpellByName(innervate)
			end
		end
		
		if GMR.IsCastable(catForm2) and not GMR.HasBuff("player", catForm2) and (GMR.GetHealth("player") >= 95 or (((GMR.HasBuff("player", regrowth) or GMR.HasBuff("player", rejuvenation)) and GMR.GetHealth("player") > 85))) 
		and not GMR.InCombat("player") and not UnitExists("target") then
			GMR.CastSpellByName(catForm2)
		end
		
		
		
		if GMR.GetHealth("player") < 95 and not GMR.InCombat("player") then 
			--rejuvenation
			if GMR.IsCastable(RJ) and GMR.GetHealth("player") < 90 and not GMR.HasBuff("player", RJ) then
				GMR.CastSpellByName(RJ)
			end
			
			--regrowth
			if GMR.IsCastable(regrowth) and GMR.GetHealth("player") < 85 and not GMR.HasBuff("player", regrowth) then
				GMR.CastSpellByName(regrowth)
			end
			
			--healing touch
			if GMR.IsCastable(HT) and (GMR.GetHealth("player") < 50 or ((GMR.HasBuff("player", regrowth) or GMR.HasBuff("player", rejuvenation)) and GMR.GetHealth("player") < 75)) then
				GMR.CastSpellByName(HT)
			end
		end
		
		if GMR.InCombat("player") and not GMR.HasBuff("player", direBear) then 
			GMR.CastSpellByName(direBear)
		end
		
		if GMR.InCombat("player") and GMR.HasBuff("player", direBear) and GMR.GetHealth("player") <= 40 then 
			if GMR.IsCastable(SI) and GMR.IsCastable(FR) and not GMR.HasBuff("player", SI) and not GMR.HasBuff("player", FR) then
				GMR.CastSpellByName(SI)
				GMR.CastSpellByName(FR)
			end
				
		end
		
		if GMR.InCombat("player") and GMR.GetHealth("player") <= 60 and not GMR.HasBuff("player", barkskin) and GMR.IsCastable(barkskin) then
			GMR.CastSpellByName(barkskin)		
		end
		
		if GMR.IsCastable(bash, "target") and GMR.InLoS("target") and UnitExists("target") and GMR.IsInterruptable("target") and GMR.UnitCastingTime("target", 2.5) then
			GMR.CastSpellByName(bash)
		end
		
		if UnitExists("target") and GMR.GetHealth("target") > 0 and not GMR.UnitIsPlayer("target") and not GMR.InCombat("player") and GMR.UnitIsAttackable("target") then
			if GMR.GetDistance("player", "target", ">", 25) and GMR.GetDistance("player", "target", "<", 45) then
				if not GMR.HasBuff("player", RJ) and GMR.IsCastable(RJ) then 
					GMR.CastSpellByName(RJ)
				end
			elseif not GMR.HasBuff("player", direBear) and GMR.GetDistance("player", "target", "<", 25) then
				GMR.CastSpellByName(direBear)
			end
		end
		
		if GMR.InLoS("target") and UnitExists("target") and GMR.GetHealth("target") > 0 and not GMR.UnitIsPlayer("target") and GMR.HasBuff("player", direBear) then
			mobcount = GMR.GetNumAttackingEnemies("player", 10)
			if GMR.IsCastable(FCB, "target") and GMR.UnitIsAttackable("target") and GMR.GetDistance("player", "target", ">", 11) then
				GMR.CastSpellByName(FCB)
			end
		 
			if GMR.IsCastable(enrage) then
				GMR.CastSpellByName(enrage)
			end
			
			if mobcount > 1 then
				if GMR.IsCastable(demo) and not GMR.HasDebuff("target", demo) and GMR.UnitIsAttackable("target") and UnitPower("player") >= 50 then
					GMR.CastSpellByName(demo)
				end
				if GMR.IsCastable(swipe, "target") and GMR.UnitIsAttackable("target") then
					GMR.CastSpellByName(swipe)
				end
			
				if GMR.IsCastable(maul, "target") and GMR.UnitIsAttackable("target") and UnitPower("player") >= 30 then
					GMR.CastSpellByName(maul)
				end
			else
				if GMR.IsCastable(lacerate, "target") and GMR.UnitIsAttackable("target") and (not GMR.HasDebuff("target", lacerate) or lacerateDuration <= 5 )then
					GMR.CastSpellByName(lacerate)
				end
		
				if GMR.IsCastable(mangle, "target") and GMR.UnitIsAttackable("target") then
					GMR.CastSpellByName(mangle)
				end
		
				if GMR.IsCastable(maul, "target") and GMR.UnitIsAttackable("target") and UnitPower("player") >= 30 then
					GMR.CastSpellByName(maul)
				end
		
				if GMR.IsCastable(ff, "target") and GMR.UnitIsAttackable("target") then
					GMR.CastSpellByName(ff)
				end
		
			end
		end
	end
end
end)
